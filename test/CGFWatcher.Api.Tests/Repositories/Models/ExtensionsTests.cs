﻿using System.Linq;
using FluentAssertions;
using Xunit;

namespace CGFWatcher.Api.Tests.Repositories.Models
{
    public class ExtensionsTests
    {
        [Theory]
        [InlineData(1, new[] { 1, 2, 3, 4, 5 })]
        [InlineData(2, new[] { 6, 7, 8, 9, 10 })]
        [InlineData(6, new[] { 26, 27 })]
        public void ToPagedList(int page, int[] expected)
        {
            // Arrange
            var query = Enumerable.Range(1, 27).AsQueryable();
            var filter = new PaginationFilter { Page = page, PerPage = 5 };

            // Act
            var result = query.ToPagedList(filter);

            // Assert
            result.Should().NotBeNull().And.BeOfType<PagedList<int>>();
            result.TotalCount.Should().Be(27);
            result.Should().BeEquivalentTo(expected);
        }
    }
}