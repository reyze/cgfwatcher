﻿using System.Collections.Generic;
using System.Linq;
using CGFWatcher.Database;
using CGFWatcher.Tests;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace CGFWatcher.Api.Tests.Repositories
{
    public class PlayerRepositoryTests
    {
        #region GetPlayer

        [Fact]
        public void GetPlayer_Projection()
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new Player
                {
                    ID = 2,
                    FullName = "Paul",
                    Handicap = 10.5m,
                    Memberships = new List<Membership>
                    {
                        new Membership { RegistrationNumber = "1111", GolfClubID = 100, IsHome = true },
                        new Membership { RegistrationNumber = "2222", GolfClubID = 200, IsDeleted = true, IsBlocked = true }
                    }
                });

                context.SaveChanges();
            }

            // Act
            PlayerData result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayer(2);
            }

            // Assert
            result.Should().BeEquivalentTo(new PlayerData
            {
                ID = 2,
                FullName = "Paul",
                Handicap = 10.5m,
                Memberships = new[]
                {
                    new PlayerData.MembershipData { RegistrationNumber = "1111", GolfClub = "Augusta", IsHome = true },
                    new PlayerData.MembershipData { RegistrationNumber = "2222", GolfClub = "Black Bridge", IsDeleted = true, IsBlocked = true }
                }
            });
        }

        [Fact]
        public void GetPlayer_Memberships_OrderByDescending_IsHome_ThenBy_IsDeleted_ThenBy_RegistrationNumber()
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.Add(new Player
                {
                    ID = 2,
                    Memberships = new List<Membership>
                    {
                        new Membership { RegistrationNumber = "2222", GolfClubID = 100, IsDeleted = true },
                        new Membership { RegistrationNumber = "5555", GolfClubID = 100 },
                        new Membership { RegistrationNumber = "1111", GolfClubID = 100 },
                        new Membership { RegistrationNumber = "4444", GolfClubID = 100, IsHome = true },
                        new Membership { RegistrationNumber = "3333", GolfClubID = 100 }
                    }
                });

                context.SaveChanges();
            }

            // Act
            PlayerData result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayer(2);
            }

            // Assert
            result.Memberships.Select(x => x.RegistrationNumber).Should()
                .ContainInOrder(new[] { "4444", "1111", "3333", "5555", "2222" });
        }

        [Fact]
        public void GetPlayer_NonExisting()
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.Add(new Player { ID = 2, FullName = "Paul", Memberships = new List<Membership>() });
                context.SaveChanges();
            }

            // Act
            PlayerData result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayer(5);
            }

            // Assert
            result.Should().BeNull();
        }

        #endregion

        #region GetPlayers

        [Fact]
        public void GetPlayers_Projection()
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player
                    {
                        ID = 1,
                        FullName = "Paul",
                        Handicap = 10.5m,
                        Memberships = new List<Membership>
                        {
                            new Membership { RegistrationNumber = "1111", GolfClubID = 100, IsHome = true },
                            new Membership { RegistrationNumber = "2222", GolfClubID = 200, IsDeleted = true }
                        }
                    },
                    new Player
                    {
                        ID = 2,
                        FullName = "Peter",
                        Handicap = 36,
                        Memberships = new List<Membership>
                        {
                            new Membership { RegistrationNumber = "3333", GolfClubID = 300, IsBlocked = true }
                        }
                    }
                });

                context.SaveChanges();
            }

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(new PlayerFilter { Page = 1, PerPage = 20 });
            }

            // Assert
            result.Should().BeEquivalentTo(new[]
            {
                new PlayerData
                {
                    ID = 1,
                    FullName = "Paul",
                    Handicap = 10.5m,
                    Memberships = new[]
                    {
                        new PlayerData.MembershipData { RegistrationNumber = "1111", GolfClub = "Augusta", IsHome = true },
                        new PlayerData.MembershipData { RegistrationNumber = "2222", GolfClub = "Black Bridge", IsDeleted = true }
                    }
                },
                new PlayerData
                {
                    ID = 2,
                    FullName = "Peter",
                    Handicap = 36,
                    Memberships = new[]
                    {
                        new PlayerData.MembershipData { RegistrationNumber = "3333", GolfClub = "Albatross", IsBlocked = true }
                    }
                }
            });
        }

        [Fact]
        public void GetPlayers_OmitPlayersWithNoMembership()
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player
                    {
                        ID = 1,
                        FullName = "Paul",
                        Handicap = 10.5m
                    }
                });

                context.SaveChanges();
            }

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(new PlayerFilter { Page = 1, PerPage = 20 });
            }

            // Assert
            result.Should().BeEmpty();
        }

        [Theory]
        [InlineData("1111", new[] { 1 })]
        [InlineData("11", new[] { 1 })]
        [InlineData("3333", new[] { 2 })]
        [InlineData("4444", new int[0])]
        public void GetPlayers_Filter_RegistrationNumber(string filterValue, int[] expectedIDs)
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            var filter = new PlayerFilter
            {
                RegistrationNumberOrName = filterValue,
                Page = 1,
                PerPage = 20
            };

            PrepareDataForFilterTests(options);

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(filter);
            }

            // Assert
            result.Select(x => x.ID).Should().BeEquivalentTo(expectedIDs);
        }

        [Theory]
        [InlineData("Donald Trump", new[] { 1 })]
        [InlineData("Bond", new[] { 2 })]
        [InlineData("Foo", new int[0])]
        public void GetPlayers_Filter_FullName(string filterValue, int[] expectedIDs)
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            var filter = new PlayerFilter
            {
                RegistrationNumberOrName = filterValue,
                Page = 1,
                PerPage = 20
            };

            PrepareDataForFilterTests(options);

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(filter);
            }

            // Assert
            result.Select(x => x.ID).Should().BeEquivalentTo(expectedIDs);
        }

        [Theory]
        [InlineData(5, new[] { 1, 2 })]
        [InlineData(10, new[] { 1, 2 })]
        [InlineData(15, new[] { 2 })]
        [InlineData(20, new int[0])]
        public void GetPlayers_Filter_HandicapFrom(decimal filterValue, int[] expectedIDs)
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            var filter = new PlayerFilter
            {
                HandicapFrom = filterValue,
                Page = 1,
                PerPage = 20
            };

            PrepareDataForFilterTests(options);

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(filter);
            }

            // Assert
            result.Select(x => x.ID).Should().BeEquivalentTo(expectedIDs);
        }

        [Theory]
        [InlineData(5, new int[0])]
        [InlineData(10, new[] { 1 })]
        [InlineData(15, new[] { 1, 2 })]
        [InlineData(20, new[] { 1, 2 })]
        public void GetPlayers_Filter_HandicapTo(decimal filterValue, int[] expectedIDs)
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            var filter = new PlayerFilter
            {
                HandicapTo = filterValue,
                Page = 1,
                PerPage = 20
            };

            PrepareDataForFilterTests(options);

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(filter);
            }

            // Assert
            result.Select(x => x.ID).Should().BeEquivalentTo(expectedIDs);
        }

        [Theory]
        [InlineData(1, new[] { 1, 2 })]
        [InlineData(2, new[] { 1 })]
        [InlineData(3, new int[0])]
        public void GetPlayers_Filter_MembershipsCountFrom(int filterValue, int[] expectedIDs)
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            var filter = new PlayerFilter
            {
                MembershipsCountFrom = filterValue,
                Page = 1,
                PerPage = 20
            };

            PrepareDataForFilterTests(options);

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(filter);
            }

            // Assert
            result.Select(x => x.ID).Should().BeEquivalentTo(expectedIDs);
        }

        [Theory]
        [InlineData(2, new[] { 1, 2 })]
        [InlineData(1, new[] { 2 })]
        [InlineData(0, new int[0])]        
        public void GetPlayers_Filter_MembershipsCountTo(int filterValue, int[] expectedIDs)
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            var filter = new PlayerFilter
            {
                MembershipsCountTo = filterValue,
                Page = 1,
                PerPage = 20
            };

            PrepareDataForFilterTests(options);

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(filter);
            }

            // Assert
            result.Select(x => x.ID).Should().BeEquivalentTo(expectedIDs);
        }

        [Theory]
        [InlineData("ABC", new[] { 1, 2 })]
        [InlineData("123", new[] { 1 })]
        [InlineData("XYZ", new int[0])]
        public void GetPlayers_Filter_GolfClubPrefix(string filterValue, int[] expectedIDs)
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            var filter = new PlayerFilter
            {
                GolfClubPrefix = filterValue,
                Page = 1,
                PerPage = 20
            };

            PrepareDataForFilterTests(options);

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(filter);
            }

            // Assert
            result.Select(x => x.ID).Should().BeEquivalentTo(expectedIDs);
        }

        [Theory]
        [InlineData(true, new[] { 3 })]
        [InlineData(false, new[] { 1, 2, 4 })]
        public void GetPlayers_Filter_IsHome(bool filterValue, int[] expectedIDs)
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            var filter = new PlayerFilter
            {
                IsHome = filterValue,
                Page = 1,
                PerPage = 20
            };

            PrepareDataForMembershipAttributesFilterTests(options);

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(filter);
            }

            // Assert
            result.Select(x => x.ID).Should().BeEquivalentTo(expectedIDs);
        }

        [Theory]
        [InlineData(true, 1)]
        [InlineData(false, 2)]
        public void GetPlayers_Filter_IsHome_CombinedWith_GolfClubPrefix(bool filterValue, int expectedID)
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            var filter = new PlayerFilter
            {
                GolfClubPrefix = "123", // GolfClubID = 200
                IsHome = filterValue,
                Page = 1,
                PerPage = 20
            };

            PrepareDataForMembershipAttributesFilterTests(options);

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(filter);
            }

            // Assert
            result.Should().OnlyContain(x => x.ID == expectedID);
        }

        [Theory]
        [InlineData(true, new[] { 3 })]
        [InlineData(false, new[] { 1, 2, 4 })]
        public void GetPlayers_Filter_IsBlocked(bool filterValue, int[] expectedIDs)
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            var filter = new PlayerFilter
            {
                IsBlocked = filterValue,
                Page = 1,
                PerPage = 20
            };

            PrepareDataForMembershipAttributesFilterTests(options);

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(filter);
            }

            // Assert
            result.Select(x => x.ID).Should().BeEquivalentTo(expectedIDs);
        }

        [Theory]
        [InlineData(true, 1)]
        [InlineData(false, 2)]
        public void GetPlayers_Filter_IsBlocked_CombinedWith_GolfClubPrefix(bool filterValue, int expectedID)
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            var filter = new PlayerFilter
            {
                GolfClubPrefix = "123", // GolfClubID = 200
                IsBlocked = filterValue,
                Page = 1,
                PerPage = 20
            };

            PrepareDataForMembershipAttributesFilterTests(options);

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(filter);
            }

            // Assert
            result.Should().OnlyContain(x => x.ID == expectedID);
        }

        [Theory]
        [InlineData(true, new[] { 3 })]
        [InlineData(false, new[] { 1, 2, 4 })]
        public void GetPlayers_Filter_IsDeleted(bool filterValue, int[] expectedIDs)
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            var filter = new PlayerFilter
            {
                IsDeleted = filterValue,
                Page = 1,
                PerPage = 20
            };

            PrepareDataForMembershipAttributesFilterTests(options);

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(filter);
            }

            // Assert
            result.Select(x => x.ID).Should().BeEquivalentTo(expectedIDs);
        }

        [Theory]
        [InlineData(true, 1)]
        [InlineData(false, 2)]
        public void GetPlayers_Filter_IsDeleted_CombinedWith_GolfClubPrefix(bool filterValue, int expectedID)
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            var filter = new PlayerFilter
            {
                GolfClubPrefix = "123", // GolfClubID = 200
                IsDeleted = filterValue,
                Page = 1,
                PerPage = 20
            };

            PrepareDataForMembershipAttributesFilterTests(options);

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(filter);
            }

            // Assert
            result.Should().OnlyContain(x => x.ID == expectedID);
        }

        [Fact]
        public void GetPlayers_OrderBy_FullName()
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player { FullName = "Paul", Memberships = new List<Membership> { new Membership { GolfClubID = 100 } } },
                    new Player { FullName = "Alfa", Memberships = new List<Membership> { new Membership { GolfClubID = 100 } } },
                    new Player { FullName = "Zeta", Memberships = new List<Membership> { new Membership { GolfClubID = 100 } } },
                    new Player { FullName = "Beta", Memberships = new List<Membership> { new Membership { GolfClubID = 100 } } }
                });

                context.SaveChanges();
            }

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(new PlayerFilter { Page = 1, PerPage = 20 });
            }

            // Assert
            result.Should().BeInAscendingOrder(x => x.FullName);
        }

        [Fact]
        public void GetPlayers_Memberships_OrderByDescending_IsHome_ThenBy_IsDeleted_ThenBy_RegistrationNumber()
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player
                    {
                        Memberships = new List<Membership>
                        {
                            new Membership { RegistrationNumber = "2222", GolfClubID = 100, IsDeleted = true },
                            new Membership { RegistrationNumber = "5555", GolfClubID = 100 },
                            new Membership { RegistrationNumber = "1111", GolfClubID = 100 },
                            new Membership { RegistrationNumber = "4444", GolfClubID = 100, IsHome = true },
                            new Membership { RegistrationNumber = "3333", GolfClubID = 100 }
                        }
                    }
                });

                context.SaveChanges();
            }

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(new PlayerFilter { Page = 1, PerPage = 20 });
            }

            // Assert
            result[0].Memberships.Select(x => x.RegistrationNumber).Should()
                .ContainInOrder(new[] { "4444", "1111", "3333", "5555", "2222" });
        }

        [Theory]
        [InlineData(1, new[] { 1, 2, 3 })]
        [InlineData(2, new[] { 4, 5, 6 })]
        [InlineData(3, new[] { 7, 8 })]
        public void GetPlayers_Pagination(int page, int[] expectedIDs)
        {
            var options = GetContextOptionsWithClubs();

            // Arrange
            var filter = new PlayerFilter
            {
                Page = page,
                PerPage = 3
            };

            using (var context = new WatcherContext(options))
            {
                foreach(var id in Enumerable.Range(1, 8))
                {
                    context.Players.Add(new Player
                    {
                        ID = id,
                        Memberships = new List<Membership> { new Membership { GolfClubID = 100 } }
                    });
                }

                context.SaveChanges();
            }

            // Act
            IPagedList<PlayerData> result;

            using (var context = new WatcherContext(options))
            {
                var repository = new PlayerRepository(context);

                result = repository.GetPlayers(filter);
            }

            // Assert
            result.Select(x => x.ID).Should().BeEquivalentTo(expectedIDs);
        }

        private void PrepareDataForFilterTests(DbContextOptions<WatcherContext> options)
        {
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player
                    {
                        ID = 1,
                        FullName = "Donald Trump",
                        Handicap = 10,
                        Memberships = new List<Membership>
                        {
                            new Membership { RegistrationNumber = "1111", GolfClubID = 100, IsHome = true },
                            new Membership { RegistrationNumber = "2222", GolfClubID = 200, IsDeleted = true }
                        }
                    },
                    new Player
                    {
                        ID = 2,
                        FullName = "James Bond",
                        Handicap = 15,
                        Memberships = new List<Membership>
                        {
                            new Membership { RegistrationNumber = "3333", GolfClubID = 100, IsBlocked = true }
                        }
                    }
                });

                context.SaveChanges();
            }
        }

        private void PrepareDataForMembershipAttributesFilterTests(DbContextOptions<WatcherContext> options)
        {
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player
                    {
                        ID = 1,
                        Memberships = new List<Membership>
                        {
                            new Membership { RegistrationNumber = "1111", GolfClubID = 100 },
                            new Membership { RegistrationNumber = "2222", GolfClubID = 200, IsHome = true, IsBlocked = true, IsDeleted = true }
                        }
                    },
                    new Player
                    {
                        ID = 2,
                        Memberships = new List<Membership>
                        {
                            new Membership { RegistrationNumber = "3333", GolfClubID = 100, IsHome = true, IsBlocked = true, IsDeleted = true },
                            new Membership { RegistrationNumber = "4444", GolfClubID = 200 }
                        }
                    },
                    new Player
                    {
                        ID = 3,
                        Memberships = new List<Membership>
                        {
                            new Membership { RegistrationNumber = "5555", GolfClubID = 100, IsHome = true, IsBlocked = true, IsDeleted = true }
                        }
                    },
                    new Player
                    {
                        ID = 4,
                        Memberships = new List<Membership>
                        {
                            new Membership { RegistrationNumber = "6666", GolfClubID = 100 }
                        }
                    }
                });

                context.SaveChanges();
            }
        }

        #endregion

        private DbContextOptions<WatcherContext> GetContextOptionsWithClubs()
        {
            var options = TestHelpers.GetContextOptions();

            using (var context = new WatcherContext(options))
            {
                context.GolfClubs.AddRange(new[]
                {
                    new GolfClub { ID = 200, Prefix = "123", Name = "Black Bridge" },
                    new GolfClub { ID = 100, Prefix = "ABC", Name = "Augusta" },
                    new GolfClub { ID = 300, Prefix = "XYZ", Name = "Albatross" }
                });

                context.SaveChanges();
            }

            return options;
        }
    }
}