﻿using System;
using System.Collections.Generic;
using System.Linq;
using CGFWatcher.Database;
using CGFWatcher.Services;
using CGFWatcher.Tests;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace CGFWatcher.Api.Tests.Repositories
{
    public class HandicapChangeRepositoryTests
    {
        private DateTime _today;
        private DbContextOptions<WatcherContext> _options;

        public HandicapChangeRepositoryTests()
        {
            _options = TestHelpers.GetContextOptions();

            using (var context = new WatcherContext(_options))
            {
                context.HandicapChanges.AddRange(new[]
                {
                    // standard player
                    new HandicapChange { PlayerID = 1, Changed = new DateTime(2016, 8, 20), NewHandicap = 28 },
                    new HandicapChange { PlayerID = 1, Changed = new DateTime(2016, 3, 20), NewHandicap = 17 },
                    new HandicapChange { PlayerID = 1, Changed = new DateTime(2017, 2, 20, 14, 0, 0), NewHandicap = 25 },
                    new HandicapChange { PlayerID = 1, Changed = new DateTime(2015, 12, 20), NewHandicap = 24 },
                    // player with date time edge cases
                    new HandicapChange { PlayerID = 2, Changed = new DateTime(2016, 12, 31, 14, 0, 0), NewHandicap = 28 },
                    new HandicapChange { PlayerID = 2, Changed = new DateTime(2016, 1, 1, 14, 0, 0), NewHandicap = 24 },
                    new HandicapChange { PlayerID = 2, Changed = new DateTime(2017, 1, 1, 14, 0, 0), NewHandicap = 28 },
                    new HandicapChange { PlayerID = 2, Changed = new DateTime(2017, 2, 20), NewHandicap = 25 },
                    new HandicapChange { PlayerID = 2, Changed = new DateTime(2015, 12, 31, 14, 0, 0), NewHandicap = 24 }
                });

                context.SaveChanges();
            }
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void Filter_PlayerID(int playerID)
        {
            // Arrange
            _today = new DateTime(2017, 10, 20);

            var filter = new HandicapChangeFilter { PlayerID = playerID };

            // Act
            IList<HandicapChange> result;

            using (var context = new WatcherContext(_options))
            {
                result = GetRepository(context).GetHandicapChanges(filter);
            }

            // Assert
            result.Should().OnlyContain(x => x.PlayerID == playerID);
        }

        [Theory]
        [InlineData(null)]
        [InlineData(2016)]
        public void OrderBy_Changed(int? year)
        {
            // Arrange
            _today = new DateTime(2017, 10, 20);
            
            var filter = new HandicapChangeFilter { PlayerID = 1, Year = year };

            // Act
            IList<HandicapChange> result;

            using (var context = new WatcherContext(_options))
            {
                result = GetRepository(context).GetHandicapChanges(filter);
            }

            // Assert
            result.Should().BeInAscendingOrder(x => x.Changed);
        }

        [Theory]
        [InlineData(2017, 2, 20, 4)]
        [InlineData(2017, 3, 20, 5)]
        [InlineData(2018, 3, 20, 5)]
        public void ItemStandardization_Without_YearFilter(int year, int month, int day, int expectedCount)
        {
            // Arrange
            _today = new DateTime(year, month, day);
            
            var filter = new HandicapChangeFilter { PlayerID = 1 };

            // Act
            IList<HandicapChange> result;

            using (var context = new WatcherContext(_options))
            {
                result = GetRepository(context).GetHandicapChanges(filter);
            }

            // Assert
            result.Should().HaveCount(expectedCount);

            // last item
            result.Where(x => x.Changed >= _today).Should().ContainSingle(x => x.Changed.Date == _today
                && x.PlayerID == 1
                && x.NewHandicap == 25);
        }

        [Theory]
        [InlineData(2, 20, 1, 2)]
        [InlineData(3, 20, 1, 3)]
        [InlineData(3, 20, 2, 3)]
        public void ItemStandardization_With_YearFilter_CurrentYear(int month, int day, int playerID, int expectedCount)
        {
            // Arrange
            _today = new DateTime(2017, month, day);
            
            var filter = new HandicapChangeFilter { PlayerID = playerID, Year = 2017 };

            // Act
            IList<HandicapChange> result;

            using (var context = new WatcherContext(_options))
            {
                result = GetRepository(context).GetHandicapChanges(filter);
            }

            // Assert
            result.Should().HaveCount(expectedCount);

            // first item
            result.Should().ContainSingle(x => x.Changed.Date == new DateTime(2017, 1, 1)
                && x.PlayerID == playerID
                && x.NewHandicap == 28);

            // last item
            result.Where(x => x.Changed >= _today).Should().ContainSingle(x => x.Changed.Date == _today
                && x.PlayerID == playerID
                && x.NewHandicap == 25);
        }

        [Theory]
        [InlineData(1, 4)]
        [InlineData(2, 2)]
        public void ItemStandardization_With_YearFilter_PreviousYear(int playerID, int expectedCount)
        {
            // Arrange
            _today = new DateTime(2017, 10, 20);
            
            var filter = new HandicapChangeFilter { PlayerID = playerID, Year = 2016 };

            // Act
            IList<HandicapChange> result;

            using (var context = new WatcherContext(_options))
            {
                result = GetRepository(context).GetHandicapChanges(filter);
            }

            // Assert
            result.Should().HaveCount(expectedCount);

            // first item
            result.Should().ContainSingle(x => x.Changed.Date == new DateTime(2016, 1, 1)
                && x.PlayerID == playerID
                && x.NewHandicap == 24);

            // last item
            result.Should().ContainSingle(x => x.Changed.Date == new DateTime(2016, 12, 31)
                && x.PlayerID == playerID
                && x.NewHandicap == 28);
        }

        [Theory]
        [InlineData(1, 2)]
        [InlineData(2, 1)]
        public void ItemStandardization_With_YearFilter_FirstRecordedYear(int playerID, int expectedCount)
        {
            // Arrange
            _today = new DateTime(2017, 10, 20);
            
            var filter = new HandicapChangeFilter { PlayerID = playerID, Year = 2015 };

            // Act
            IList<HandicapChange> result;

            using (var context = new WatcherContext(_options))
            {
                result = GetRepository(context).GetHandicapChanges(filter);
            }

            // Assert
            result.Should().HaveCount(expectedCount);

            // last item
            result.Should().ContainSingle(x => x.Changed.Date == new DateTime(2015, 12, 31)
                && x.PlayerID == playerID
                && x.NewHandicap == 24);
        }

        [Fact]
        public void ItemStandardization_With_YearFilter_EmptyYear()
        {
            // Arrange
            _today = new DateTime(2019, 10, 20);
            
            var filter = new HandicapChangeFilter { PlayerID = 1, Year = 2018 };

            // Act
            IList<HandicapChange> result;

            using (var context = new WatcherContext(_options))
            {
                result = GetRepository(context).GetHandicapChanges(filter);
            }

            // Assert
            result.Should().HaveCount(2);

            // first item
            result.Should().ContainSingle(x => x.Changed.Date == new DateTime(2018, 1, 1)
                && x.PlayerID == 1
                && x.NewHandicap == 25);

            // last item
            result.Should().ContainSingle(x => x.Changed.Date == new DateTime(2018, 12, 31)
                && x.PlayerID == 1
                && x.NewHandicap == 25);
        }

        [Fact]
        public void ItemStandardization_With_YearFilter_EmptyCurrentYear()
        {
            // Arrange
            _today = new DateTime(2018, 10, 20);
            
            var filter = new HandicapChangeFilter { PlayerID = 1, Year = 2018 };

            // Act
            IList<HandicapChange> result;

            using (var context = new WatcherContext(_options))
            {
                result = GetRepository(context).GetHandicapChanges(filter);
            }

            // Assert
            result.Should().HaveCount(2);

            // first item
            result.Should().ContainSingle(x => x.Changed.Date == new DateTime(2018, 1, 1)
                && x.PlayerID == 1
                && x.NewHandicap == 25);

            // last item
            result.Should().ContainSingle(x => x.Changed.Date == new DateTime(2018, 10, 20)
                && x.PlayerID == 1
                && x.NewHandicap == 25);
        }
        
        [Theory]
        [InlineData(null)]
        [InlineData(2016)]
        public void ItemStandardization_NoData(int? year)
        {
            // Arrange
            _today = new DateTime(2017, 10, 20);
            
            var filter = new HandicapChangeFilter { PlayerID = 3, Year = year };

            // Act
            IList<HandicapChange> result;

            using (var context = new WatcherContext(_options))
            {
                result = GetRepository(context).GetHandicapChanges(filter);
            }

            // Assert
            result.Should().BeEmpty();
        }
        
        private HandicapChangeRepository GetRepository(WatcherContext context)
        {
            var clockProvider = Mock.Of<IClockProvider>(x => x.Today == _today);

            return new HandicapChangeRepository(context, clockProvider);
        }
    }
}