﻿using System;
using System.Collections.Generic;
using CGFWatcher.Database;
using CGFWatcher.Tests;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace CGFWatcher.Api.Tests.Repositories
{
    public class PlayerSnapshotRepositoryTests
    {
        private DbContextOptions<WatcherContext> _options;

        public PlayerSnapshotRepositoryTests()
        {
            _options = TestHelpers.GetContextOptions();

            using (var context = new WatcherContext(_options))
            {
                context.PlayerSnapshots.AddRange(new[]
                {
                    new PlayerSnapshot { Created = new DateTime(2017, 12, 5, 14, 0, 0) },
                    new PlayerSnapshot { Created = new DateTime(2017, 12, 3) },
                    new PlayerSnapshot { Created = new DateTime(2017, 12, 1) },
                    new PlayerSnapshot { Created = new DateTime(2017, 12, 4, 23, 59, 59) },
                    new PlayerSnapshot { Created = new DateTime(2017, 12, 2) }
                });

                context.SaveChanges();
            }
        }

        [Theory]
        [InlineData(1, 5, 5)]
        [InlineData(3, 5, 3)]
        [InlineData(1, 4, 4)]
        [InlineData(4, 8, 2)]
        public void Filter_DateFrom_And_DateTo(int from, int to, int expectedCount)
        {
            IList<PlayerSnapshot> result;

            // Arrange
            var filter = new PlayerSnapshotFilter
            {
                DateFrom = new DateTime(2017, 12, from),
                DateTo = new DateTime(2017, 12, to)
            };

            // Act
            using (var context = new WatcherContext(_options))
            {
                var repository = new PlayerSnapshotRepository(context);

                result = repository.GetPlayerSnapshots(filter);
            }

            // Assert
            result.Should().HaveCount(expectedCount);
        }

        [Fact]
        public void OrderBy_Created()
        {
            IList<PlayerSnapshot> result;
            
            // Arrange
            var filter = new PlayerSnapshotFilter
            {
                DateFrom = new DateTime(2017, 1, 1),
                DateTo = new DateTime(2017, 12, 31)
            };

            // Act
            using (var context = new WatcherContext(_options))
            {
                var repository = new PlayerSnapshotRepository(context);

                result = repository.GetPlayerSnapshots(filter);
            }

            // Assert
            result.Should().HaveCount(5).And.BeInDescendingOrder(x => x.Created);
        }
    }
}