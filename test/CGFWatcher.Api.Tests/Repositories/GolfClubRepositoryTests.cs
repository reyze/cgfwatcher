﻿using System.Collections.Generic;
using CGFWatcher.Database;
using CGFWatcher.Tests;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace CGFWatcher.Api.Tests.Repositories
{
    public class GolfClubRepositoryTests
    {
        private DbContextOptions<WatcherContext> _options;

        public GolfClubRepositoryTests()
        {
            _options = TestHelpers.GetContextOptions();

            using (var context = new WatcherContext(_options))
            {
                context.GolfClubs.AddRange(new[]
                {
                    new GolfClub { ID = 1, Name = "Rome", Prefix = "B" },
                    new GolfClub { ID = 2, Name = "Prague", Prefix = "A" },
                    new GolfClub { ID = 3, Name = "Atlanta", Prefix = "E" },
                    new GolfClub { ID = 4, Name = "Zurich", Prefix = "D" },
                    new GolfClub { ID = 5, Name = "Berlin", Prefix = "C" }
                });

                context.SaveChanges();
            }
        }

        [Fact]
        public void OrderBy_Prefix()
        {
            IList<GolfClub> result;
            
            // Arrange
            
            // Act
            using (var context = new WatcherContext(_options))
            {
                var repository = new GolfClubRepository(context);

                result = repository.GetGolfClubs();
            }

            // Assert
            result.Should().HaveCount(5).And.BeInAscendingOrder(x => x.Prefix);
        }
    }
}