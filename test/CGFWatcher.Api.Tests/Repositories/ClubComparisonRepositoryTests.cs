﻿using System;
using CGFWatcher.Database;
using CGFWatcher.Tests;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace CGFWatcher.Api.Tests.Repositories
{
    public class ClubComparisonRepositoryTests
    {
        private DbContextOptions<WatcherContext> _options;

        public ClubComparisonRepositoryTests()
        {
            _options = TestHelpers.GetContextOptions();

            using (var context = new WatcherContext(_options))
            {
                context.GolfClubs.AddRange(new[]
                {
                    new GolfClub { ID = 200, Prefix = "123", Name = "Test" },
                    new GolfClub { ID = 100, Prefix = "ABC", Name = "Foo" },
                    new GolfClub { ID = 300, Prefix = "XYZ", Name = "Bar" }
                });

                context.MembershipSnapshots.AddRange(new[]
                {
                    new MembershipSnapshot { ID = 1, GolfClubID = 300, Created = new DateTime(2016, 9, 5, 12, 30, 0) },
                    new MembershipSnapshot { ID = 2, GolfClubID = 100, Created = new DateTime(2016, 9, 10, 12, 30, 0) },
                    new MembershipSnapshot { ID = 3, GolfClubID = 300, Created = new DateTime(2016, 9, 10, 12, 30, 0) },
                    new MembershipSnapshot { ID = 4, GolfClubID = 100, Created = new DateTime(2016, 9, 15, 12, 30, 0) },
                    new MembershipSnapshot { ID = 5, GolfClubID = 200, Created = new DateTime(2016, 9, 20, 12, 30, 0) },
                    new MembershipSnapshot { ID = 6, GolfClubID = 200, Created = new DateTime(2016, 9, 25, 12, 30, 0) }
                });

                context.SaveChanges();
            }
        }

        [Theory]
        [InlineData(30, "123", "ABC", 6, 4)]
        [InlineData(30, "ABC", "123", 4, 6)]
        [InlineData(22, "ABC", "123", 4, 5)]
        [InlineData(12, "ABC", "XYZ", 2, 3)]
        public void Result_LatestSnapshots(int day, string prefix1, string prefix2, int resultID1, int resultID2)
        {
            ClubComparisonData result;

            // Arrange
            var filter = new ClubComparisonFilter
            {
                Date = new DateTime(2016, 9, day),
                GolfClubPrefix1 = prefix1,
                GolfClubPrefix2 = prefix2
            };

            // Act
            using (var context = new WatcherContext(_options))
            {
                var repository = new ClubComparisonRepository(context);

                result = repository.GetClubComparison(filter);
            }

            // Assert
            result.Should().NotBeNull();
            result.GolfClubData1.ID.Should().Be(resultID1);
            result.GolfClubData2.ID.Should().Be(resultID2);
        }

        [Theory]
        [InlineData(15, "XYZ", "ABC", 3, 4)]
        [InlineData(14, "XYZ", "ABC", 3, 2)]
        [InlineData(10, "XYZ", "ABC", 3, 2)]
        public void Result_IncludeSnapshotRecordedOnDate(int day, string prefix1, string prefix2, int resultID1, int resultID2)
        {
            ClubComparisonData result;

            // Arrange
            var filter = new ClubComparisonFilter
            {
                Date = new DateTime(2016, 9, day),
                GolfClubPrefix1 = prefix1,
                GolfClubPrefix2 = prefix2
            };

            // Act
            using (var context = new WatcherContext(_options))
            {
                var repository = new ClubComparisonRepository(context);

                result = repository.GetClubComparison(filter);
            }

            // Assert
            result.Should().NotBeNull();
            result.GolfClubData1.ID.Should().Be(resultID1);
            result.GolfClubData2.ID.Should().Be(resultID2);
        }

        [Fact]
        public void Result_NonExisting()
        {
            ClubComparisonData result;

            // Arrange
            var filter = new ClubComparisonFilter
            {
                Date = new DateTime(2016, 9, 1),
                GolfClubPrefix1 = "ABC",
                GolfClubPrefix2 = "Foo Bar"
            };

            // Act
            using (var context = new WatcherContext(_options))
            {
                var repository = new ClubComparisonRepository(context);

                result = repository.GetClubComparison(filter);
            }

            // Assert
            result.Should().NotBeNull();
            result.GolfClubData1.Should().BeNull();
            result.GolfClubData2.Should().BeNull();
        }

        [Fact]
        public void Result_IncludeGolfClubEntity()
        {
            ClubComparisonData result;

            // Arrange
            var filter = new ClubComparisonFilter
            {
                Date = new DateTime(2016, 10, 1),
                GolfClubPrefix1 = "ABC",
                GolfClubPrefix2 = "XYZ"
            };

            // Act
            using (var context = new WatcherContext(_options))
            {
                var repository = new ClubComparisonRepository(context);

                result = repository.GetClubComparison(filter);
            }

            // Assert
            result.GolfClubData1.GolfClub.Should().NotBeNull()
                .And.Match<GolfClub>(x => x.Name == "Foo");

            result.GolfClubData2.GolfClub.Should().NotBeNull()
                .And.Match<GolfClub>(x => x.Name == "Bar");
        }
    }
}