﻿using System;
using System.Collections.Generic;
using System.Linq;
using CGFWatcher.Database;
using CGFWatcher.Tests;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace CGFWatcher.Api.Tests.Repositories
{
    public class MembershipSnapshotRepositoryTests
    {
        private DbContextOptions<WatcherContext> _options;

        public MembershipSnapshotRepositoryTests()
        {
            _options = TestHelpers.GetContextOptions();

            using (var context = new WatcherContext(_options))
            {
                context.GolfClubs.AddRange(new[]
                {
                    new GolfClub { ID = 200, Prefix = "123" },
                    new GolfClub { ID = 100, Prefix = "ABC" },
                    new GolfClub { ID = 300, Prefix = "XYZ" }
                });

                context.MembershipSnapshots.AddRange(new[]
                {
                    new MembershipSnapshot { ID = 1, GolfClubID = 200, Created = new DateTime(2017, 12, 4) },
                    new MembershipSnapshot { ID = 2, GolfClubID = 100, Created = new DateTime(2017, 12, 4, 15, 0, 0) },
                    new MembershipSnapshot { ID = 3, GolfClubID = 200, Created = new DateTime(2017, 12, 3, 16, 30, 0) },
                    new MembershipSnapshot { ID = 4, GolfClubID = 300, Created = new DateTime(2017, 12, 3) },
                    new MembershipSnapshot { ID = 5, GolfClubID = 100, Created = new DateTime(2017, 12, 2) },
                    new MembershipSnapshot { ID = 6, GolfClubID = 100, Created = new DateTime(2017, 12, 3, 23, 59, 59) }
                });

                context.SaveChanges();
            }
        }

        [Theory]
        [InlineData(2, 4, "ABC", new[] { 2, 5, 6 })]
        [InlineData(3, 4, "ABC", new[] { 2, 6 })]
        [InlineData(2, 3, "ABC", new[] { 5, 6 })]
        [InlineData(4, 8, "ABC", new[] { 2 })]
        [InlineData(5, 8, "ABC", new int[0])]
        [InlineData(2, 4, "123", new[] { 1, 3 })]
        [InlineData(2, 4, "XYZ", new[] { 4 })]
        public void Filter_DateFrom_DateTo_And_GolfClubPrefix(int from, int to, string prefix, int[] expectedIDs)
        {
            IList<MembershipSnapshot> result;

            // Arrange
            var filter = new MembershipSnapshotFilter
            {
                DateFrom = new DateTime(2017, 12, from),
                DateTo = new DateTime(2017, 12, to),
                GolfClubPrefix = prefix
            };

            // Act
            using (var context = new WatcherContext(_options))
            {
                var repository = new MembershipSnapshotRepository(context);

                result = repository.GetMembershipSnapshots(filter);
            }

            // Assert
            result.Select(x => x.ID).Should().BeEquivalentTo(expectedIDs);
        }

        [Theory]
        [InlineData("ABC")]
        [InlineData("123")]
        public void OrderBy_Created(string prefix)
        {
            IList<MembershipSnapshot> result;
            
            // Arrange
            var filter = new MembershipSnapshotFilter
            {
                DateFrom = new DateTime(2017, 1, 1),
                DateTo = new DateTime(2017, 12, 31),
                GolfClubPrefix = prefix
            };

            // Act
            using (var context = new WatcherContext(_options))
            {
                var repository = new MembershipSnapshotRepository(context);

                result = repository.GetMembershipSnapshots(filter);
            }

            // Assert
            result.Should().BeInDescendingOrder(x => x.Created);
        }
    }
}