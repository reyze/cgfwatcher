﻿using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Moq;
using Xunit;

namespace CGFWatcher.Api.Tests.Controllers
{
    public class ExtensionsTests
    {
        [Fact]
        public void AddPaginationHeaders()
        {
            // Arrange
            var list = new PagedList<int>(new int[0], 123);
            var headers = new HeaderDictionary();
            var response = Mock.Of<HttpResponse>(x => x.Headers == headers);

            // Act
            response.AddPaginationHeaders(list);

            // Assert
            headers.Should().Contain("X-Total-Count", "123");
        }
    }
}