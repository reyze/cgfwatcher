﻿using CGFWatcher.Services;
using FluentAssertions;
using Xunit;

namespace CGFWatcher.Tests
{
    public class ImportParserTests
    {
        [Fact]
        public void BasicParsing()
        {
            // Arrange
            var parser = new ImportParser();

            var html = GetHtmlForParsing(
                "<tr><td>Pavel</td> <td>1234567</td> <td>Black Bridge</td> <td>Ano</td> <td>15,6</td> <td>Bar</td> <td>Ne</td></tr>" +
                "<tr><td>Pavel</td> <td>1111111</td> <td>Albatross</td> <td>Ne</td> <td>15,6</td> <td>Bar</td> <td>Ne</td></tr>" +
                "<tr><td>Petra</td> <td>2222222</td> <td>Black Bridge</td> <td>Ne</td> <td>20,0</td> <td>Bar</td> <td>Ano</td></tr>"
            );

            // Act
            var result = parser.ParsePlayersHtml(html);

            // Assert
            result.Should().BeEquivalentTo(new[]
            {
                new ImportData
                {
                    FullName = "Pavel",
                    RegistrationNumber = "1234567",
                    Club = "Black Bridge",
                    Handicap = 15.6m,
                    IsHome = true,
                    IsBlocked = false
                },
                new ImportData
                {
                    FullName = "Pavel",
                    RegistrationNumber = "1111111",
                    Club = "Albatross",
                    Handicap = 15.6m,
                    IsHome = false,
                    IsBlocked = false
                },
                new ImportData
                {
                    FullName = "Petra",
                    RegistrationNumber = "2222222",
                    Club = "Black Bridge",
                    Handicap = 20,
                    IsHome = false,
                    IsBlocked = true
                }
            });
        }

        [Fact]
        public void ThrowAwayDuplicates()
        {
            // Arrange
            var parser = new ImportParser();

            var html = GetHtmlForParsing(
                "<tr><td>Pavel</td> <td>1234567</td> <td>Black Bridge</td> <td>Ano</td> <td>54</td> <td>Bar</td> <td>Ne</td></tr>" +
                "<tr><td>Pavel</td> <td>1234567</td> <td>Black Bridge</td> <td>Ne</td> <td>54</td> <td>Bar</td> <td>Ne</td></tr>" +
                "<tr><td>Petra</td> <td>2222222</td> <td>Black Bridge</td> <td>Ne</td> <td>20,0</td> <td>Bar</td> <td>Ano</td></tr>"
            );

            // Act
            var result = parser.ParsePlayersHtml(html);

            // Assert
            result.Should().BeEquivalentTo(new[]
            {
                new ImportData
                {
                    FullName = "Pavel",
                    RegistrationNumber = "1234567",
                    Club = "Black Bridge",
                    Handicap = 54,
                    IsHome = true,
                    IsBlocked = false
                },
                new ImportData
                {
                    FullName = "Petra",
                    RegistrationNumber = "2222222",
                    Club = "Black Bridge",
                    Handicap = 20,
                    IsHome = false,
                    IsBlocked = true
                }
            });
        }

        [Fact]
        public void HandicapBelowZero()
        {
            // Arrange
            var parser = new ImportParser();

            var html = GetHtmlForParsing(
                "<tr><td>Pavel</td> <td>1234567</td> <td>Black Bridge</td> <td>Ano</td> <td>+2,6</td> <td>Bar</td> <td>Ne</td></tr>"
            );

            // Act
            var result = parser.ParsePlayersHtml(html);

            // Assert
            result.Should().BeEquivalentTo(new[]
            {
                new ImportData
                {
                    FullName = "Pavel",
                    RegistrationNumber = "1234567",
                    Club = "Black Bridge",
                    Handicap = -2.6m,
                    IsHome = true,
                    IsBlocked = false
                }
            });
        }

        [Fact]
        public void EncodedHtml()
        {
            // Arrange
            var parser = new ImportParser();

            var html = GetHtmlForParsing(
                "<tr><td>Nov&#225;k</td> <td>1234567</td> <td>Black Bridge</td> <td>Ano</td> <td>2</td> <td>Bar</td> <td>Ne</td></tr>"
            );

            // Act
            var result = parser.ParsePlayersHtml(html);

            // Assert
            result.Should().ContainSingle(x => x.FullName == "Novák");
        }

        [Fact]
        public void HtmlTags()
        {
            // Arrange
            var parser = new ImportParser();

            var html = GetHtmlForParsing(
                "<tr><td>Pavel</td> <td>1234567</td> <td><a href=\"#\">Black Bridge</a></td> <td>Ano</td> <td>2</td> <td>Bar</td> <td>Ne</td></tr>"
            );

            // Act
            var result = parser.ParsePlayersHtml(html);

            // Assert
            result.Should().ContainSingle(x => x.Club == "Black Bridge");
        }

        [Fact]
        public void TextTrimming()
        {
            // Arrange
            var parser = new ImportParser();

            var html = GetHtmlForParsing(
                "<tr><td>Pavel  </td> <td>   1234567</td> <td>  Black Bridge  </td> <td>Ano</td> <td>2</td> <td>Bar</td> <td>Ne</td></tr>"
            );

            // Act
            var result = parser.ParsePlayersHtml(html);

            // Assert
            result.Should().BeEquivalentTo(new[]
            {
                new ImportData
                {
                    FullName = "Pavel",
                    RegistrationNumber = "1234567",
                    Club = "Black Bridge",
                    Handicap = 2,
                    IsHome = true,
                    IsBlocked = false
                }
            });
        }

        [Theory]
        [InlineData("<div></div>")]
        [InlineData("<div><table></table></div>")]
        [InlineData("<div><table class=\"FooBar\"></table></div>")]
        public void NoData(string html)
        {
            // Arrange
            var parser = new ImportParser();

            // Act
            var result = parser.ParsePlayersHtml(html);

            // Assert
            result.Should().BeEmpty();
        }

        private string GetHtmlForParsing(string rowsHtml)
        {
            return "<div>"
                 + "<table class=\"GridView\">"
                 + "<tr><th>Name</th> <th>Number</th> <th>Club</th> <th>Home</th> <th>Handicap</th> <th>Foo</th> <th>Blocked</th></tr>"
                 + rowsHtml
                 + "</table>"
                 + "<div>";
        }
    }
}