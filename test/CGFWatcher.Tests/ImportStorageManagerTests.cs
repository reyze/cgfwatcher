﻿using System;
using System.Collections.Generic;
using CGFWatcher.Database;
using CGFWatcher.Services;
using FluentAssertions;
using Moq;
using Xunit;

namespace CGFWatcher.Tests
{
    public class ImportStorageManagerTests
    {
        private DateTime _now = new DateTime(2015, 12, 24, 12, 31, 58);
        private ImportParameters _parameters = new ImportParameters { HandicapFrom = "5", HandicapTo = "10" };

        [Fact]
        public void NewRegistrationNumber()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            var data = new List<ImportData>
            {
                new ImportData { RegistrationNumber = "1234567", FullName = "Pavel", Club = "Black Bridge", Handicap = 6 },
                new ImportData { RegistrationNumber = "1111111", FullName = "Pavel", Club = "Albatross", Handicap = 6, IsBlocked = true },
                new ImportData { RegistrationNumber = "7654321", FullName = "Petra", Club = "Black Bridge", Handicap = 9, IsHome = true }
            };

            // Act
            using (var context = new WatcherContext(options))
            {
                GetManager().StoreImportData(data, _parameters, context);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.ImportRows.Should().BeEquivalentTo(new[]
                {
                    new ImportRow { RegistrationNumber = "1234567", FullName = "Pavel", Club = "Black Bridge", Handicap = 6, Changed = _now },
                    new ImportRow { RegistrationNumber = "1111111", FullName = "Pavel", Club = "Albatross", Handicap = 6, IsBlocked = true, Changed = _now },
                    new ImportRow { RegistrationNumber = "7654321", FullName = "Petra", Club = "Black Bridge", Handicap = 9, IsHome = true, Changed = _now }
                });
            }
        }

        [Fact]
        public void ExistingRegistrationNumber()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "1234567", FullName = "Pavel", Club = "Black Bridge", Handicap = 3, IsBlocked = true },
                    new ImportRow { RegistrationNumber = "7654321", FullName = "Pavla", Club = "Albatross", Handicap = 9, IsHome = true },
                });

                context.SaveChanges();
            }

            var data = new List<ImportData>
            {
                new ImportData { RegistrationNumber = "1234567", FullName = "Pavel", Club = "Black Bridge", Handicap = 6, IsHome = true, IsBlocked = false },
                new ImportData { RegistrationNumber = "7654321", FullName = "Petra", Club = "Black Bridge", Handicap = 9.2m, IsHome = false }
            };

            // Act
            using (var context = new WatcherContext(options))
            {
                GetManager().StoreImportData(data, _parameters, context);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.ImportRows.Should().HaveCount(2)
                    .And.Contain(x => x.RegistrationNumber == "1234567"
                        && x.FullName == "Pavel"
                        && x.Club == "Black Bridge"
                        && x.Handicap == 6
                        && x.IsHome == true
                        && x.IsBlocked == false
                        && x.Changed == _now)
                    .And.Contain(x => x.RegistrationNumber == "7654321"
                        && x.FullName == "Petra"
                        && x.Club == "Black Bridge"
                        && x.Handicap == 9.2m
                        && x.IsHome == false
                        && x.Changed == _now);
            }
        }

        [Fact]
        public void DeletedRegistrationNumber()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "1234567", FullName = "Pavel", Club = "Black Bridge", Handicap = 6 },
                    new ImportRow { RegistrationNumber = "7654321", FullName = "Petra", Club = "Albatross", Handicap = 9 },
                    new ImportRow { RegistrationNumber = "5555555", FullName = "Tomáš", Club = "Black Bridge", Handicap = 20 }
                });

                context.SaveChanges();
            }

            var data = new List<ImportData>
            {
                new ImportData { RegistrationNumber = "1234567", FullName = "Pavel", Club = "Black Bridge", Handicap = 6 }
            };

            // Act
            using (var context = new WatcherContext(options))
            {
                GetManager().StoreImportData(data, _parameters, context);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.ImportRows.Should().HaveCount(3)
                    .And.Contain(x => x.RegistrationNumber == "7654321"
                        && x.IsDeleted == true
                        && x.Changed == _now)
                    .And.Contain(x => x.RegistrationNumber == "5555555"
                        && x.IsDeleted == false
                        && x.Changed == default(DateTime));
            }
        }

        [Fact]
        public void DeletedRegistrationNumber_HandicapBelowZero()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "1234567", FullName = "Pavel", Club = "Black Bridge", Handicap = -2 },
                    new ImportRow { RegistrationNumber = "7654321", FullName = "Petra", Club = "Albatross", Handicap = -1.7m },
                    new ImportRow { RegistrationNumber = "5555555", FullName = "Tomáš", Club = "Black Bridge", Handicap = 20 }
                });

                context.SaveChanges();
            }

            var data = new List<ImportData>
            {
                new ImportData { RegistrationNumber = "1234567", FullName = "Pavel", Club = "Black Bridge", Handicap = -2.3m }
            };

            _parameters = new ImportParameters { HandicapFrom = "+2,5", HandicapTo = "+1,5" };

            // Act
            using (var context = new WatcherContext(options))
            {
                GetManager().StoreImportData(data, _parameters, context);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.ImportRows.Should().HaveCount(3)
                    .And.Contain(x => x.RegistrationNumber == "7654321"
                        && x.IsDeleted == true
                        && x.Changed == _now)
                    .And.Contain(x => x.RegistrationNumber == "5555555"
                        && x.IsDeleted == false
                        && x.Changed == default(DateTime));
            }
        }

        [Fact]
        public void RestoredRegistrationNumber()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "1234567", FullName = "Petr", Club = "Black Bridge GC", Handicap = 4, IsDeleted = true }
                });

                context.SaveChanges();
            }
            var data = new List<ImportData>
            {
                new ImportData { RegistrationNumber = "1234567", FullName = "Pavel", Club = "Black Bridge", Handicap = 6.5m, IsHome = true },
                new ImportData { RegistrationNumber = "7654321", FullName = "Petra", Club = "Black Bridge", Handicap = 9.2m }
            };

            // Act
            using (var context = new WatcherContext(options))
            {
                GetManager().StoreImportData(data, _parameters, context);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.ImportRows.Should().HaveCount(2)
                    .And.Contain(x => x.RegistrationNumber == "1234567"
                        && x.FullName == "Pavel"
                        && x.Club == "Black Bridge"
                        && x.Handicap == 6.5m
                        && x.IsHome == true
                        && x.IsDeleted == false
                        && x.Changed == _now);
            }
        }

        [Fact]
        public void Logging_RowsCount()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            var data = new List<ImportData>
            {
                new ImportData { FullName = "Pavel", RegistrationNumber = "1234567", Club = "Black Bridge", Handicap = 10, IsHome = true },
                new ImportData { FullName = "Pavel", RegistrationNumber = "1111111", Club = "Albatross", Handicap = 10 },
                new ImportData { FullName = "Pavel", RegistrationNumber = "2222222", Club = "Konopiště", Handicap = 10 },
                new ImportData { FullName = "Petra", RegistrationNumber = "7654321", Club = "Konopiště", Handicap = 20, IsBlocked = true }
            };

            // Act
            ImportLog result;

            using (var context = new WatcherContext(options))
            {
                result = GetManager().StoreImportData(data, _parameters, context);
            }

            // Assert
            result.RowsCount.Should().Be(4);
        }

        private ImportStorageManager GetManager()
        {
            var clockProvider = Mock.Of<IClockProvider>(x => x.Now == _now);

            return new ImportStorageManager(clockProvider);
        }
    }
}