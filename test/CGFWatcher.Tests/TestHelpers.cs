﻿using System;
using CGFWatcher.Database;
using Microsoft.EntityFrameworkCore;

namespace CGFWatcher.Tests
{
    public class TestHelpers
    {
        public static DbContextOptions<WatcherContext> GetContextOptions()
        {
            return new DbContextOptionsBuilder<WatcherContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
        }
    }
}
