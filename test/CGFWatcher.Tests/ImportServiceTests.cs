﻿using System;
using System.Collections.Generic;
using CGFWatcher.Database;
using CGFWatcher.Services;
using FluentAssertions;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace CGFWatcher.Tests
{
    public class ImportServiceTests
    {
        private DateTime _now = new DateTime(2015, 12, 24, 12, 31, 58);

        [Fact]
        public void Logging()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            var parameters = new ImportParameters { HandicapFrom = "10", HandicapTo = "20" };
            var storageLog = new ImportLog { RowsCount = 1200 };

            // Act
            ImportLog result;

            using (var context = new WatcherContext(options))
            {
                var service = GetImportService(context, parameters, storageLog);

                result = service.ImportPlayers(parameters).Result;
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                result.Imported.Should().Be(_now);
                result.Parameters.Should().Be(JsonConvert.SerializeObject(parameters));

                context.ImportLogs.Should().HaveCount(1)
                    .And.Contain(x => x.RowsCount == 1200
                        && x.Imported == _now
                        && x.Parameters == JsonConvert.SerializeObject(parameters));
            }
        }

        private ImportService GetImportService(WatcherContext context, ImportParameters parameters, ImportLog storageLog)
        {
            var loader = new Mock<IImportLoader>();

            loader.Setup(x => x.LoadPlayersHtml(parameters)).ReturnsAsync("FooBar");

            var parser = Mock.Of<IImportParser>(x => x.ParsePlayersHtml("FooBar") == new List<ImportData>());
            var storageManager = Mock.Of<IImportStorageManager>(x => x.StoreImportData(It.IsAny<IList<ImportData>>(), parameters, context) == storageLog);
            var clockProvider = Mock.Of<IClockProvider>(x => x.Now == _now);

            return new ImportService(context, loader.Object, parser, storageManager, clockProvider);
        }
    }
}