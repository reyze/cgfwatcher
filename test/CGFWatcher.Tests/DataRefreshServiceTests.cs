﻿using System;
using System.Linq;
using CGFWatcher.Database;
using CGFWatcher.Services;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace CGFWatcher.Tests
{
    public class DataRefreshServiceTests
    {
        private DateTime _now = new DateTime(2015, 12, 24, 12, 31, 58);
        private DataRefreshParameters _parameters = new DataRefreshParameters { HandicapFrom = "5", HandicapTo = "10" };

        [Fact]
        public void ExistingPlayer_UpdateHandicap()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player { ID = 100, FullName = "Pavel", Handicap = 6 },
                    new Player { ID = 200, FullName = "Petra", Handicap = 8 },
                    new Player { ID = 300, FullName = "Tomáš", Handicap = 9 }
                });

                context.Memberships.AddRange(new[]
                {
                    new Membership { PlayerID = 100, GolfClubID = 1, RegistrationNumber = "100-10" },
                    new Membership { PlayerID = 100, GolfClubID = 2, RegistrationNumber = "200-11" },
                    new Membership { PlayerID = 200, GolfClubID = 1, RegistrationNumber = "100-20" },
                    new Membership { PlayerID = 200, GolfClubID = 2, RegistrationNumber = "200-21" },
                    new Membership { PlayerID = 300, GolfClubID = 1, RegistrationNumber = "100-30" }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", FullName = "Pavel", Club = "Black Bridge", Handicap = 5.4m },
                    new ImportRow { RegistrationNumber = "200-11", FullName = "Pavel", Club = "Albatross", Handicap = 5.4m },
                    new ImportRow { RegistrationNumber = "100-20", FullName = "Petra", Club = "Black Bridge", Handicap = 8.2m },
                    new ImportRow { RegistrationNumber = "100-30", FullName = "Tomáš", Club = "Black Bridge", Handicap = 9 }
                });

                context.SaveChanges();
            }

            // Act
            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.Players.Single(x => x.FullName == "Pavel").Handicap.Should().Be(5.4m);
                context.Players.Single(x => x.FullName == "Petra").Handicap.Should().Be(8.2m);

                context.HandicapChanges.Should().HaveCount(2)
                    .And.Contain(x => x.NewHandicap == 5.4m && x.Player.FullName == "Pavel" && x.Changed == _now)
                    .And.Contain(x => x.NewHandicap == 8.2m && x.Player.FullName == "Petra" && x.Changed == _now);
            }
        }

        [Fact]
        public void ExistingPlayer_UpdateMembership()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player { ID = 100, FullName = "Pavel", Handicap = 6 }
                });

                context.Memberships.AddRange(new[]
                {
                    new Membership { PlayerID = 100, GolfClubID = 1, RegistrationNumber = "100-10", IsBlocked = true },
                    new Membership { PlayerID = 100, GolfClubID = 2, RegistrationNumber = "200-11", IsHome = true }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", IsHome = true, FullName = "Pavel", Club = "Black Bridge", Handicap = 6 },
                    new ImportRow { RegistrationNumber = "200-11", IsBlocked = true, FullName = "Pavel", Club = "Albatross", Handicap = 6 }
                });

                context.SaveChanges();
            }

            // Act
            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.Memberships.Should()
                    .Contain(x => x.RegistrationNumber == "100-10"
                        && x.IsHome == true
                        && x.IsBlocked == false
                        && x.Changed == _now)
                    .And.Contain(x => x.RegistrationNumber == "200-11"
                        && x.IsHome == false
                        && x.IsBlocked == true
                        && x.Changed == _now);
            }
        }

        [Fact]
        public void ExistingPlayer_UnchangedMembership()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player { ID = 100, FullName = "Pavel", Handicap = 6 }
                });

                context.Memberships.AddRange(new[]
                {
                    new Membership { PlayerID = 100, GolfClubID = 1, RegistrationNumber = "100-10", IsHome = true },
                    new Membership { PlayerID = 100, GolfClubID = 2, RegistrationNumber = "200-11", IsBlocked = true }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", IsHome = true, FullName = "Pavel", Club = "Black Bridge", Handicap = 6 },
                    new ImportRow { RegistrationNumber = "200-11", IsBlocked = true, FullName = "Pavel", Club = "Albatross", Handicap = 6 }
                });

                context.SaveChanges();
            }
            // Act
            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.Memberships.Should()
                    .Contain(x => x.RegistrationNumber == "100-10" && x.Changed == null)
                    .And.Contain(x => x.RegistrationNumber == "200-11" && x.Changed == null);
            }
        }

        [Fact]
        public void ExistingPlayer_NewMembership()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player { ID = 100, FullName = "Pavel", Handicap = 6 }
                });

                context.Memberships.AddRange(new[]
                {
                    new Membership { PlayerID = 100, GolfClubID = 1, RegistrationNumber = "100-10" }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", FullName = "Pavel", Club = "Black Bridge", Handicap = 6 },
                    new ImportRow { RegistrationNumber = "200-11", IsHome = true, FullName = "Pavel", Club = "Albatross", Handicap = 6 }
                });

                context.SaveChanges();
            }
            // Act
            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.Memberships.Should().HaveCount(2).And.Contain(x => x.PlayerID == 100
                    && x.GolfClubID == 2
                    && x.RegistrationNumber == "200-11"
                    && x.IsHome == true
                    && x.IsBlocked == false
                    && x.Created == _now);
            }
        }

        [Fact]
        public void ExistingPlayer_AbandonedMembership()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player { ID = 100, FullName = "Pavel", Handicap = 6 }
                });

                context.Memberships.AddRange(new[]
                {
                    new Membership { PlayerID = 100, GolfClubID = 1, RegistrationNumber = "100-10", IsHome = true },
                    new Membership { PlayerID = 100, GolfClubID = 2, RegistrationNumber = "200-11" },
                    new Membership { PlayerID = 100, GolfClubID = 3, RegistrationNumber = "300-12" }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", IsHome = true, FullName = "Pavel", Club = "Black Bridge", Handicap = 6 }
                });

                context.SaveChanges();
            }
            // Act
            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.Memberships.Should().HaveCount(3);

                var m1 = context.Memberships.Single(x => x.RegistrationNumber == "200-11");
                var m2 = context.Memberships.Single(x => x.RegistrationNumber == "300-12");

                m1.IsDeleted.Should().BeTrue();
                m1.Changed.Should().Be(_now);

                m2.IsDeleted.Should().BeTrue();
                m2.Changed.Should().Be(_now);
            }
        }

        [Fact]
        public void ExistingPlayer_RestoredMembership()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player { ID = 100, FullName = "Pavel", Handicap = 6 }
                });

                context.Memberships.AddRange(new[]
                {
                    new Membership { PlayerID = 100, GolfClubID = 1, RegistrationNumber = "100-10" },
                    new Membership { PlayerID = 100, GolfClubID = 2, RegistrationNumber = "200-11", IsDeleted = true, IsBlocked = true },
                    new Membership { PlayerID = 100, GolfClubID = 3, RegistrationNumber = "300-21", IsDeleted = true }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", FullName = "Pavel", Club = "Black Bridge", Handicap = 6 },
                    new ImportRow { RegistrationNumber = "200-11", FullName = "Pavel", Club = "Albatross", Handicap = 6 },
                    new ImportRow { RegistrationNumber = "300-21", IsHome = true, FullName = "Pavel", Club = "Konopiště", Handicap = 6 }
                });

                context.SaveChanges();
            }

            // Act
            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.Memberships.Should().HaveCount(3)
                    .And.Contain(x => x.PlayerID == 100
                        && x.RegistrationNumber == "200-11"
                        && x.IsDeleted == false
                        && x.IsBlocked == false
                        && x.Changed == _now)
                    .And.Contain(x => x.PlayerID == 100
                        && x.RegistrationNumber == "300-21"
                        && x.IsDeleted == false
                        && x.IsHome == true
                        && x.Changed == _now);
            }
        }

        [Fact]
        public void ExistingPlayer_MovedToDifferentClub()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player { ID = 100, FullName = "Pavel", Handicap = 6.2m }
                });

                context.Memberships.AddRange(new[]
                {
                    new Membership { PlayerID = 100, GolfClubID = 1, RegistrationNumber = "100-10" }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "200-20", IsHome = true, FullName = "Pavel", Club = "Albatross", Handicap = 6.2m }
                });

                context.SaveChanges();
            }

            // Act
            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.Memberships.Should().HaveCount(2)
                    .And.Contain(x => x.PlayerID == 100
                        && x.RegistrationNumber == "100-10"
                        && x.IsDeleted == true
                        && x.Changed == _now)
                    .And.Contain(x => x.PlayerID == 100
                        && x.RegistrationNumber == "200-20"
                        && x.IsHome == true
                        && x.Created == _now);
            }
        }

        [Fact]
        public void ExistingPlayer_FullNameChanged()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player { ID = 100, FullName = "Pavel", Handicap = 6.2m }
                });

                context.Memberships.AddRange(new[]
                {
                    new Membership { PlayerID = 100, GolfClubID = 1, RegistrationNumber = "100-10" },
                    new Membership { PlayerID = 100, GolfClubID = 2, RegistrationNumber = "200-11" }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", IsHome = true, FullName = "Petr", Club = "Black Bridge", Handicap = 6.4m },
                    new ImportRow { RegistrationNumber = "200-11", FullName = "Petr", Club = "Albatross", Handicap = 6.4m }
                });

                context.SaveChanges();
            }
            // Act
            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.Players.Should().ContainSingle(x => x.FullName == "Petr" && x.Handicap == 6.4m);

                context.Memberships.Should().HaveCount(2)
                    .And.Contain(x => x.PlayerID == 100
                        && x.RegistrationNumber == "100-10"
                        && x.IsHome == true
                        && x.Changed == _now)
                    .And.Contain(x => x.PlayerID == 100
                        && x.RegistrationNumber == "200-11"
                        && x.Changed == null);

                context.HandicapChanges.Should().ContainSingle();
            }
        }

        [Fact]
        public void NewPlayer_AddPlayer()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", FullName = "Pavel", Club = "Black Bridge", Handicap = 6.2m },
                    new ImportRow { RegistrationNumber = "200-11", FullName = "Pavel", Club = "Albatross", Handicap = 6.2m },
                    new ImportRow { RegistrationNumber = "100-20", FullName = "Petra", Club = "Black Bridge", Handicap = 8.4m }
                });

                context.SaveChanges();
            }

            // Act
            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.Players.Should().HaveCount(2)
                    .And.Contain(x => x.FullName == "Pavel" && x.Handicap == 6.2m)
                    .And.Contain(x => x.FullName == "Petra" && x.Handicap == 8.4m);

                context.HandicapChanges.Should().HaveCount(2)
                    .And.Contain(x => x.NewHandicap == 6.2m && x.Player.FullName == "Pavel" && x.Changed == _now)
                    .And.Contain(x => x.NewHandicap == 8.4m && x.Player.FullName == "Petra" && x.Changed == _now);
            }
        }

        [Fact]
        public void NewPlayer_AddMembership()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", IsHome = true, FullName = "Pavel", Club = "Black Bridge", Handicap = 6 },
                    new ImportRow { RegistrationNumber = "200-20", FullName = "Pavel", Club = "Albatross", Handicap = 6 },
                    new ImportRow { RegistrationNumber = "100-11", IsBlocked = true, FullName = "Petra", Club = "Black Bridge", Handicap = 8 }
                });

                context.SaveChanges();
            }
            // Act
            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.Memberships.Include(x => x.Player).Should().HaveCount(3)
                    .And.Contain(x => x.Player.FullName == "Pavel"
                        && x.GolfClubID == 1
                        && x.RegistrationNumber == "100-10"
                        && x.IsHome == true
                        && x.IsBlocked == false
                        && x.Created == _now)
                    .And.Contain(x => x.Player.FullName == "Pavel"
                        && x.GolfClubID == 2
                        && x.RegistrationNumber == "200-20"
                        && x.IsHome == false
                        && x.IsBlocked == false
                        && x.Created == _now)
                    .And.Contain(x => x.Player.FullName == "Petra"
                        && x.GolfClubID == 1
                        && x.RegistrationNumber == "100-11"
                        && x.IsHome == false
                        && x.IsBlocked == true
                        && x.Created == _now);
            }
        }

        [Fact]
        public void PlayersWithTheSameName_NewPlayers()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", FullName = "Pavel", Club = "Black Bridge", Handicap = 6.4m },
                    new ImportRow { RegistrationNumber = "200-20", FullName = "Pavel", Club = "Albatross", Handicap = 5.1m },
                    new ImportRow { RegistrationNumber = "100-30", FullName = "Pavel", Club = "Black Bridge", Handicap = 8.9m }
                });

                context.SaveChanges();
            }

            // Act
            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.Players.Should().HaveCount(3)
                    .And.Contain(x => x.FullName == "Pavel" && x.Handicap == 6.4m)
                    .And.Contain(x => x.FullName == "Pavel" && x.Handicap == 5.1m)
                    .And.Contain(x => x.FullName == "Pavel" && x.Handicap == 8.9m);

                context.Memberships.Should().HaveCount(3);
                context.HandicapChanges.Should().HaveCount(3);
            }
        }

        [Fact]
        public void PlayersWithTheSameName_WithFormerlyEqualHandicaps()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player { ID = 100, FullName = "Pavel", Handicap = 6 }
                });

                context.Memberships.AddRange(new[]
                {
                    new Membership { PlayerID = 100, GolfClubID = 1, RegistrationNumber = "100-10" },
                    new Membership { PlayerID = 100, GolfClubID = 2, RegistrationNumber = "200-20", IsBlocked = true },
                    new Membership { PlayerID = 100, GolfClubID = 3, RegistrationNumber = "300-30" }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", FullName = "Pavel", Club = "Black Bridge", Handicap = 5.8m },
                    new ImportRow { RegistrationNumber = "200-20", FullName = "Pavel", Club = "Albatross", Handicap = 6.2m, IsHome = true },
                    new ImportRow { RegistrationNumber = "300-21", FullName = "Pavel", Club = "Konopiště", Handicap = 6.2m },
                    new ImportRow { RegistrationNumber = "300-30", FullName = "Pavel", Club = "Konopiště", Handicap = 12m }
                });

                context.SaveChanges();
            }

            // Act
            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.Players.Should().HaveCount(2)
                    .And.Contain(x => x.FullName == "Pavel" && x.Handicap == 5.8m)
                    .And.Contain(x => x.FullName == "Pavel" && x.Handicap == 6.2m);

                context.Memberships.Should().HaveCount(3)
                    .And.Contain(x => x.RegistrationNumber == "100-10" && x.Player.Handicap == 5.8m && x.Changed == null)
                    .And.Contain(x => x.RegistrationNumber == "200-20" && x.Player.Handicap == 6.2m && x.Changed == _now && x.IsHome == true && x.IsBlocked == false)
                    .And.Contain(x => x.RegistrationNumber == "300-21" && x.Player.Handicap == 6.2m && x.Created == _now);

                context.HandicapChanges.Should().HaveCount(2);
            }
        }

        [Fact]
        public void PlayersWithTheSameName_ReachedEqualHandicaps()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player { ID = 100, FullName = "Pavel", Handicap = 6 },
                    new Player { ID = 101, FullName = "Pavel", Handicap = 7 }
                });

                context.Memberships.AddRange(new[]
                {
                    new Membership { PlayerID = 100, GolfClubID = 1, RegistrationNumber = "100-10" },
                    new Membership { PlayerID = 100, GolfClubID = 2, RegistrationNumber = "200-11" },
                    new Membership { PlayerID = 101, GolfClubID = 3, RegistrationNumber = "300-20" }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", FullName = "Pavel", Club = "Black Bridge", Handicap = 6.5m },
                    new ImportRow { RegistrationNumber = "200-11", FullName = "Pavel", Club = "Albatross", Handicap = 6.5m },
                    new ImportRow { RegistrationNumber = "300-20", FullName = "Pavel", Club = "Konopiště", Handicap = 6.5m }
                });

                context.SaveChanges();
            }

            // Act
            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.Players.Include(x => x.Memberships).Should().HaveCount(2)
                    .And.Contain(x => x.FullName == "Pavel" && x.Handicap == 6.5m && x.Memberships.Count() == 3)
                    .And.Contain(x => x.FullName == "Pavel" && !x.Memberships.Any());

                context.Memberships.Should().HaveCount(3)
                    .And.Contain(x => x.RegistrationNumber == "100-10" && x.Player.Handicap == 6.5m)
                    .And.Contain(x => x.RegistrationNumber == "200-11" && x.Player.Handicap == 6.5m)
                    .And.Contain(x => x.RegistrationNumber == "300-20" && x.Player.Handicap == 6.5m);

                context.HandicapChanges.Should().HaveCount(1);
            }
        }

        [Fact]
        public void ApplyParameters_HandicapRange()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player { ID = 100, FullName = "Pavel", Handicap = 6 },
                    new Player { ID = 101, FullName = "Petra", Handicap = 8 }
                });

                context.Memberships.AddRange(new[]
                {
                    new Membership { PlayerID = 100, GolfClubID = 1, RegistrationNumber = "100-10" },
                    new Membership { PlayerID = 101, GolfClubID = 2, RegistrationNumber = "200-20" }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", FullName = "Pavel", Club = "Black Bridge", Handicap = 4.5m },
                    new ImportRow { RegistrationNumber = "200-20", FullName = "Petra", Club = "Albatross", Handicap = 7.5m },
                    new ImportRow { RegistrationNumber = "300-30", FullName = "Tomáš", Club = "Konopiště", Handicap = 9 },
                    new ImportRow { RegistrationNumber = "100-40", FullName = "Jirka", Club = "Black Bridge", Handicap = 12 },
                    new ImportRow { RegistrationNumber = "100-50", FullName = "Vojta", Club = "Black Bridge", Handicap = -2 }
                });

                context.SaveChanges();
            }

            // Act
            DataRefreshLog result;

            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                result = service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.Players.Should().HaveCount(3)
                .And.Contain(x => x.FullName == "Pavel" && x.Handicap == 6)
                .And.Contain(x => x.FullName == "Petra" && x.Handicap == 7.5m);

                context.Memberships.Should().HaveCount(3);
                context.HandicapChanges.Should().HaveCount(2);

                result.RowsCount.Should().Be(2);
            }
        }

        [Fact]
        public void ApplyParameters_ItemsRange()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                _parameters.ItemsFrom = 2;
                _parameters.ItemsTo = 4;

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-30", FullName = "Tomáš", Club = "Black Bridge", Handicap = 7 },
                    new ImportRow { RegistrationNumber = "200-31", FullName = "Tomáš", Club = "Albatross", Handicap = 7 },
                    new ImportRow { RegistrationNumber = "300-32", FullName = "Tomáš", Club = "Konopiště", Handicap = 7 },

                    new ImportRow { RegistrationNumber = "100-20", FullName = "Petra", Club = "Black Bridge", Handicap = 6 },

                    new ImportRow { RegistrationNumber = "100-40", FullName = "Jirka", Club = "Black Bridge", Handicap = 8 },

                    new ImportRow { RegistrationNumber = "100-50", FullName = "Vojta", Club = "Black Bridge", Handicap = 9 },
                    new ImportRow { RegistrationNumber = "200-51", FullName = "Vojta", Club = "Albatross", Handicap = 9 },
                    new ImportRow { RegistrationNumber = "300-52", FullName = "Vojta", Club = "Konopiště", Handicap = 9 },

                    new ImportRow { RegistrationNumber = "100-10", FullName = "Pavel", Club = "Black Bridge", Handicap = 5 },
                    new ImportRow { RegistrationNumber = "200-11", FullName = "Pavel", Club = "Albatross", Handicap = 5 }
                });

                context.SaveChanges();
            }

            // Act
            DataRefreshLog result;

            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                result = service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.Players.Should().HaveCount(3);
                context.Players.Select(x => x.FullName).Should().BeEquivalentTo(new[]
                {
                    "Petra",
                    "Tomáš",
                    "Jirka"
                });

                context.Memberships.Should().HaveCount(5);
                context.Memberships.Select(x => x.RegistrationNumber).Should().BeEquivalentTo(new[]
                {
                    "100-20",
                    "100-30",
                    "200-31",
                    "300-32",
                    "100-40"
                });

                context.HandicapChanges.Should().HaveCount(3);

                result.RowsCount.Should().Be(5);
            }
        }

        [Fact]
        public void DeletedMembershipsCleanup()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player { ID = 100, FullName = "Pavel", Handicap = 6 },
                    new Player { ID = 101, FullName = "Petra", Handicap = 12 }
                });

                context.Memberships.AddRange(new[]
                {
                    new Membership { PlayerID = 100, GolfClubID = 1, RegistrationNumber = "100-10" },
                    new Membership { PlayerID = 101, GolfClubID = 2, RegistrationNumber = "200-20" }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", FullName = "Pavel", Club = "Black Bridge", Handicap = 7, IsDeleted = true },
                    new ImportRow { RegistrationNumber = "200-20", FullName = "Petra", Club = "Albatross", Handicap = 13, IsDeleted = true }
                });

                context.SaveChanges();
            }

            // Act
            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.Players.Should().HaveCount(2)
                    .And.Contain(x => x.FullName == "Pavel" && x.Handicap == 6)
                    .And.Contain(x => x.FullName == "Petra" && x.Handicap == 12);

                context.Memberships.Should().HaveCount(2)
                    .And.Contain(x => x.RegistrationNumber == "100-10" && x.IsDeleted == true && x.Changed == _now)
                    .And.Contain(x => x.RegistrationNumber == "200-20" && x.IsDeleted == false && x.Changed == null);
            }
        }

        [Fact]
        public void NewGolfClub()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.GolfClubs.AddRange(new[]
                {
                    new GolfClub { ID = 10, Prefix = "100", Name = "Black Bridge" }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-01", FullName = "Pavel", Club = "Black Bridge", Handicap = 6 },
                    new ImportRow { RegistrationNumber = "123-01", FullName = "Pavel", Club = "Albatross", Handicap = 6 },
                    new ImportRow { RegistrationNumber = "765-01", FullName = "Petra", Club = "Konopiště", Handicap = 8 },
                    new ImportRow { RegistrationNumber = "123-02", FullName = "Pavel", Club = "Albatross", Handicap = 8 }
                });

                context.SaveChanges();
            }

            // Act
            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context, prepareGolfClubs: false);

                service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.GolfClubs.Should().HaveCount(3)
                    .And.Contain(x => x.Name == "Albatross" && x.Prefix == "123")
                    .And.Contain(x => x.Name == "Konopiště" && x.Prefix == "765");
            }
        }

        [Fact]
        public void RenamedGolfClub()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.GolfClubs.AddRange(new[]
                {
                    new GolfClub { ID = 10, Name = "Black Bridge Resort", Prefix = "100" }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-01", FullName = "Pavel", Club = "Black Bridge Club", Handicap = 6 }
                });

                context.SaveChanges();
            }

            // Act
            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context, prepareGolfClubs: false);

                service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.GolfClubs.Should().BeEquivalentTo(new[]
                {
                    new GolfClub { ID = 10, Name = "Black Bridge Club", Prefix = "100" }
                });
            }
        }

        [Fact]
        public void Logging()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange

            // Act
            DataRefreshLog result;

            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                result = service.RefreshPlayersData(_parameters);
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                result.Refreshed.Should().Be(_now);
                result.Parameters.Should().Be(JsonConvert.SerializeObject(_parameters));

                context.DataRefreshLogs.Should().ContainSingle();

                context.DataRefreshLogs.First().Should().BeEquivalentTo(result);
            }
        }

        [Fact]
        public void Logging_RowsCount()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-00", FullName = "Pavel", Club = "Black Bridge", Handicap = 6 },
                    new ImportRow { RegistrationNumber = "200-00", FullName = "Pavel", Club = "Albatross", Handicap = 6 },
                    new ImportRow { RegistrationNumber = "200-01", FullName = "Petra", Club = "Albatross", Handicap = 8, IsDeleted = true },
                    new ImportRow { RegistrationNumber = "100-01", FullName = "Petra", Club = "Black Bridge", Handicap = 8 },
                    new ImportRow { RegistrationNumber = "300-00", FullName = "Tomáš", Club = "Konopiště", Handicap = 9 },
                    new ImportRow { RegistrationNumber = "100-02", FullName = "Jirka", Club = "Black Bridge", Handicap = 12 },
                    new ImportRow { RegistrationNumber = "100-03", FullName = "Vojta", Club = "Black Bridge", Handicap = -2 },
                });

                context.SaveChanges();
            }

            // Act
            DataRefreshLog result;

            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                result = service.RefreshPlayersData(_parameters);
            }

            // Assert
            result.RowsCount.Should().Be(5);
        }
        
        [Fact]
        public void Logging_NewPlayersCount()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player { ID = 101, FullName = "Petra" }
                });

                context.Memberships.AddRange(new[]
                {
                    new Membership { PlayerID = 101, GolfClubID = 2, RegistrationNumber = "200-20" }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", FullName = "Pavel", Club = "Black Bridge", Handicap = 6 },
                    new ImportRow { RegistrationNumber = "200-20", FullName = "Petra", Club = "Albatross", Handicap = 8 },
                    new ImportRow { RegistrationNumber = "300-30", FullName = "Tomáš", Club = "Konopiště", Handicap = 9 }
                });

                context.SaveChanges();
            }

            // Act
            DataRefreshLog result;

            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                result = service.RefreshPlayersData(_parameters);
            }

            // Assert
            result.NewPlayersCount.Should().Be(2);
        }

        [Fact]
        public void Logging_NewMembershipsCount()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player { ID = 101, FullName = "Petra", Handicap = 8 },
                    new Player { ID = 102, FullName = "Tomáš", Handicap = 9 }
                });

                context.Memberships.AddRange(new[]
                {
                    new Membership { PlayerID = 101, GolfClubID = 1, RegistrationNumber = "100-20" },
                    new Membership { PlayerID = 102, GolfClubID = 3, RegistrationNumber = "300-30" }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", FullName = "Pavel", Club = "Black Bridge", Handicap = 6 },
                    new ImportRow { RegistrationNumber = "200-10", FullName = "Pavel", Club = "Albatross", Handicap = 6 },
                    new ImportRow { RegistrationNumber = "200-20", FullName = "Petra", Club = "Albatross", Handicap = 8 },
                    new ImportRow { RegistrationNumber = "100-20", FullName = "Petra", Club = "Black Bridge", Handicap = 8 },
                    new ImportRow { RegistrationNumber = "300-30", FullName = "Tomáš", Club = "Konopiště", Handicap = 9 }
                });

                context.SaveChanges();
            }

            // Act
            DataRefreshLog result;

            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                result = service.RefreshPlayersData(_parameters);
            }

            // Assert
            result.NewMembershipsCount.Should().Be(3);
        }

        [Fact]
        public void Logging_HandicapChangesCount()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                context.Players.AddRange(new[]
                {
                    new Player { ID = 101, FullName = "Petra", Handicap = 8.1m },
                    new Player { ID = 102, FullName = "Lenka", Handicap = 5.3m },
                    new Player { ID = 103, FullName = "Tomáš", Handicap = 9.8m },
                    new Player { ID = 104, FullName = "Jirka", Handicap = 7 }
                });

                context.Memberships.AddRange(new[]
                {
                    new Membership { PlayerID = 101, GolfClubID = 2, RegistrationNumber = "200-20" },
                    new Membership { PlayerID = 102, GolfClubID = 3, RegistrationNumber = "300-40" },
                    new Membership { PlayerID = 103, GolfClubID = 2, RegistrationNumber = "200-30" },
                    new Membership { PlayerID = 103, GolfClubID = 1, RegistrationNumber = "100-30" }
                });

                context.ImportRows.AddRange(new[]
                {
                    new ImportRow { RegistrationNumber = "100-10", FullName = "Pavel", Club = "Black Bridge", Handicap = 6 },
                    new ImportRow { RegistrationNumber = "200-20", FullName = "Petra", Club = "Albatross", Handicap = 8 },
                    new ImportRow { RegistrationNumber = "200-30", FullName = "Tomáš", Club = "Albatross", Handicap = 9.2m },
                    new ImportRow { RegistrationNumber = "100-30", FullName = "Tomáš", Club = "Black Bridge", Handicap = 9.2m },
                    new ImportRow { RegistrationNumber = "300-40", FullName = "Lenka", Club = "Konopiště", Handicap = 5.3m }
                });

                context.SaveChanges();
            }

            // Act
            DataRefreshLog result;

            using (var context = new WatcherContext(options))
            {
                var service = GetDataRefreshService(context);

                result = service.RefreshPlayersData(_parameters);
            }

            // Assert
            result.HandicapChangesCount.Should().Be(2);
        }

        private DataRefreshService GetDataRefreshService(WatcherContext context, bool prepareGolfClubs = true)
        {
            if (prepareGolfClubs)
            {
                context.GolfClubs.AddRange(new[]
                {
                    new GolfClub { ID = 1, Prefix = "100", Name = "Black Bridge" },
                    new GolfClub { ID = 2, Prefix = "200", Name = "Albatross" },
                    new GolfClub { ID = 3, Prefix = "300", Name = "Konopiště" }
                });

                context.SaveChanges();
            }

            var clockProvider = Mock.Of<IClockProvider>(x => x.Now == _now);

            return new DataRefreshService(context, clockProvider);
        }
    }
}