﻿using CGFWatcher.Services;
using FluentAssertions;
using Xunit;

namespace CGFWatcher.Tests
{
    public class ExtensionsTests
    {
        [Fact]
        public void ParseHandicap()
        {
            // Arrange

            // Act

            // Assert
            "7".ParseHandicap().Should().Be(7m);
            "5,3".ParseHandicap().Should().Be(5.3m);
            "0".ParseHandicap().Should().Be(decimal.Zero);
            "+1".ParseHandicap().Should().Be(-1m);
            "+2,3".ParseHandicap().Should().Be(-2.3m);
        }

        [Fact]
        public void FormatHandicap()
        {
            // Arrange

            // Act

            // Assert
            7m.FormatHandicap().Should().Be("7,0");
            5.3m.FormatHandicap().Should().Be("5,3");
            decimal.Zero.FormatHandicap().Should().Be("0,0");
            (-1m).FormatHandicap().Should().Be("+1,0");
            (-2.3m).FormatHandicap().Should().Be("+2,3");
        }
    }
}