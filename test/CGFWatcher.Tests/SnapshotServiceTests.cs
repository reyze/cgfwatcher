﻿using System;
using CGFWatcher.Database;
using CGFWatcher.Services;
using FluentAssertions;
using Moq;
using Xunit;

namespace CGFWatcher.Tests
{
    public class SnapshotServiceTests
    {
        private DateTime _now = new DateTime(2015, 12, 24, 12, 31, 58);

        [Fact]
        public void PlayerSnapshot()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                PrepareDatabase(context);
            }

            // Act
            using (var context = new WatcherContext(options))
            {
                GetService(context).CreateSnapshots();
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.PlayerSnapshots.Should().HaveCount(1)
                    .And.Contain(x => x.Created == _now
                        && x.Handicap_0_10 == 3
                        && x.Handicap_10_20 == 4
                        && x.Handicap_20_30 == 3
                        && x.Handicap_30_36 == 5
                        && x.Handicap_37_53 == 2
                        && x.Handicap_54 == 1
                        && x.Handicap_lt_0 == 2);
            }
        }

        [Fact]
        public void MembershipSnapshot()
        {
            var options = TestHelpers.GetContextOptions();

            // Arrange
            using (var context = new WatcherContext(options))
            {
                PrepareDatabase(context);
            }

            // Act
            using (var context = new WatcherContext(options))
            {
                GetService(context).CreateSnapshots();
            }

            // Assert
            using (var context = new WatcherContext(options))
            {
                context.MembershipSnapshots.Should().HaveCount(3)
                    .And.Contain(x => x.GolfClubID == 1
                        && x.Created == _now
                        && x.Total == 18
                        && x.TotalHome == 6
                        && x.TotalBlocked == 3
                        && x.Handicap_0_10 == 3
                        && x.Handicap_10_20 == 4
                        && x.Handicap_20_30 == 3
                        && x.Handicap_30_36 == 5
                        && x.Handicap_37_53 == 2
                        && x.Handicap_54 == 1
                        && x.Handicap_lt_0 == 0)
                    .And.Contain(x => x.GolfClubID == 2
                        && x.Created == _now
                        && x.Total == 8
                        && x.TotalHome == 2
                        && x.TotalBlocked == 0
                        && x.Handicap_0_10 == 1
                        && x.Handicap_10_20 == 2
                        && x.Handicap_20_30 == 0
                        && x.Handicap_30_36 == 1
                        && x.Handicap_37_53 == 1
                        && x.Handicap_54 == 1
                        && x.Handicap_lt_0 == 2)
                    .And.Contain(x => x.GolfClubID == 3
                        && x.Created == _now
                        && x.Total == 9
                        && x.TotalHome == 4
                        && x.TotalBlocked == 3
                        && x.Handicap_0_10 == 3
                        && x.Handicap_10_20 == 2
                        && x.Handicap_20_30 == 0
                        && x.Handicap_30_36 == 1
                        && x.Handicap_37_53 == 2
                        && x.Handicap_54 == 1
                        && x.Handicap_lt_0 == 0);
            }
        }

        private SnapshotService GetService(WatcherContext context)
        {
            var clockProvider = Mock.Of<IClockProvider>(x => x.Now == _now);

            return new SnapshotService(context, clockProvider);
        }

        private void PrepareDatabase(WatcherContext context)
        {
            context.Players.AddRange(new[]
            {
                // Handicap_0_10
                new Player { ID = 1, Handicap = 0 },
                new Player { ID = 2, Handicap = 5 },
                new Player { ID = 3, Handicap = 9.9m },

                // Handicap_10_20
                new Player { ID = 4, Handicap = 10 },
                new Player { ID = 5, Handicap = 12 },
                new Player { ID = 6, Handicap = 14 }, // no active membership
                new Player { ID = 7, Handicap = 15 },
                new Player { ID = 8, Handicap = 19.9m },

                // Handicap_20_30
                new Player { ID = 9, Handicap = 20 },
                new Player { ID = 10, Handicap = 25 },
                new Player { ID = 11, Handicap = 29.9m },

                // Handicap_30_36
                new Player { ID = 12, Handicap = 30 },
                new Player { ID = 13, Handicap = 31 },
                new Player { ID = 14, Handicap = 32 }, // no active membership
                new Player { ID = 15, Handicap = 33 },
                new Player { ID = 16, Handicap = 35 },
                new Player { ID = 17, Handicap = 36 },

                // Handicap_37_53
                new Player { ID = 18, Handicap = 37 },
                new Player { ID = 19, Handicap = 53 },

                // Handicap_54
                new Player { ID = 20, Handicap = 54 },

                // Handicap_lt_0
                new Player { ID = 21, Handicap = -8 },
                new Player { ID = 22, Handicap = -0.1m }
            });

            context.Memberships.AddRange(new[]
            {
                // club 1
                new Membership { GolfClubID = 1, PlayerID = 1 },
                new Membership { GolfClubID = 1, PlayerID = 2, IsHome = true },
                new Membership { GolfClubID = 1, PlayerID = 3, IsHome = true },
                new Membership { GolfClubID = 1, PlayerID = 4 },
                new Membership { GolfClubID = 1, PlayerID = 5 },
                new Membership { GolfClubID = 1, PlayerID = 7, IsHome = true, IsBlocked = true },
                new Membership { GolfClubID = 1, PlayerID = 8, IsBlocked = true },
                new Membership { GolfClubID = 1, PlayerID = 9 },
                new Membership { GolfClubID = 1, PlayerID = 10 },
                new Membership { GolfClubID = 1, PlayerID = 11, IsHome = true },
                new Membership { GolfClubID = 1, PlayerID = 12 },
                new Membership { GolfClubID = 1, PlayerID = 13 },
                new Membership { GolfClubID = 1, PlayerID = 14, IsDeleted = true },
                new Membership { GolfClubID = 1, PlayerID = 15 },
                new Membership { GolfClubID = 1, PlayerID = 16 },
                new Membership { GolfClubID = 1, PlayerID = 17, IsHome = true, IsBlocked = true },
                new Membership { GolfClubID = 1, PlayerID = 18 },
                new Membership { GolfClubID = 1, PlayerID = 19, IsHome = true },
                new Membership { GolfClubID = 1, PlayerID = 20 },

                // club 2
                new Membership { GolfClubID = 2, PlayerID = 2 },
                new Membership { GolfClubID = 2, PlayerID = 5, IsHome = true },
                new Membership { GolfClubID = 2, PlayerID = 8 },
                new Membership { GolfClubID = 2, PlayerID = 13, IsDeleted = true },
                new Membership { GolfClubID = 2, PlayerID = 16 },
                new Membership { GolfClubID = 2, PlayerID = 18, IsHome = true },
                new Membership { GolfClubID = 2, PlayerID = 20 },
                new Membership { GolfClubID = 2, PlayerID = 21 },
                new Membership { GolfClubID = 2, PlayerID = 22 },

                // club 3
                new Membership { GolfClubID = 3, PlayerID = 1, IsHome = true },
                new Membership { GolfClubID = 3, PlayerID = 2, IsHome = true },
                new Membership { GolfClubID = 3, PlayerID = 3, IsBlocked = true },
                new Membership { GolfClubID = 3, PlayerID = 4, IsBlocked = true },
                new Membership { GolfClubID = 3, PlayerID = 5 },
                new Membership { GolfClubID = 3, PlayerID = 17 },
                new Membership { GolfClubID = 3, PlayerID = 18 },
                new Membership { GolfClubID = 3, PlayerID = 19, IsHome = true },
                new Membership { GolfClubID = 3, PlayerID = 20, IsHome = true, IsBlocked = true },
            });

            context.SaveChanges();
        }
    }
}