﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using CGFWatcher.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CGFWatcher.Scheduler
{
    public class Program
    {
        private static IServiceProvider _serviceProvider;

        public static void Main(string[] args)
        {
            ConfigureServices();

            Console.WriteLine("Sequence started");

            var stopwatch = Stopwatch.StartNew();

            RunImportService();
            RunDataRefreshService();
            RunSnapshotService();

            stopwatch.Stop();

            Console.WriteLine("Sequence finished in " + stopwatch.Elapsed);
            Console.ReadKey();
        }

        private static void RunImportService()
        {
            Console.WriteLine("Running ImportService...");

            var queue = new[]
            {
                new ImportParameters { HandicapFrom = "+8", HandicapTo = "+0" },
                new ImportParameters { HandicapFrom = "0,1", HandicapTo = "5" },
                new ImportParameters { HandicapFrom = "5,1", HandicapTo = "10" },
                new ImportParameters { HandicapFrom = "10,1", HandicapTo = "12,5" },
                new ImportParameters { HandicapFrom = "12,6", HandicapTo = "15" },
                new ImportParameters { HandicapFrom = "15,1", HandicapTo = "17,5" },
                new ImportParameters { HandicapFrom = "17,6", HandicapTo = "20" },
                new ImportParameters { HandicapFrom = "20,1", HandicapTo = "23,3" },
                new ImportParameters { HandicapFrom = "23,4", HandicapTo = "26,6" },
                new ImportParameters { HandicapFrom = "26,7", HandicapTo = "30" },
                new ImportParameters { HandicapFrom = "30,1", HandicapTo = "35,9" },
                new ImportParameters { HandicapFrom = "36", HandicapTo = "40" },
                new ImportParameters { HandicapFrom = "41", HandicapTo = "53" },
                new ImportParameters { HandicapFrom = "54", HandicapTo = "54" }
            };

            var importService = _serviceProvider.GetService<IImportService>();

            foreach (var parameters in queue)
            {
                // try import players until we are successful
                while (true)
                {
                    try
                    {
                        var result = importService.ImportPlayers(parameters).GetAwaiter().GetResult();

                        Console.WriteLine("{0} - loading duration {1}s, storage duration {2}s, rows count {3}",
                            result.Parameters,
                            result.LoadingDurationInSeconds,
                            result.StorageDurationInSeconds,
                            result.RowsCount);

                        break;
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Exception thrown during the import process, running same import schema again");

                        Thread.Sleep(20000);
                    }
                }

                Thread.Sleep(10000);
            }
        }

        private static void RunDataRefreshService()
        {
            Console.WriteLine("Running DataRefreshService...");

            var queue = new[]
            {
                new DataRefreshParameters { HandicapFrom = "+8", HandicapTo = "0" },
                new DataRefreshParameters { HandicapFrom = "0,1", HandicapTo = "5" },
                new DataRefreshParameters { HandicapFrom = "5,1", HandicapTo = "10" },
                new DataRefreshParameters { HandicapFrom = "10,1", HandicapTo = "15" },
                new DataRefreshParameters { HandicapFrom = "15,1", HandicapTo = "20" },
                new DataRefreshParameters { HandicapFrom = "20,1", HandicapTo = "25" },
                new DataRefreshParameters { HandicapFrom = "25,1", HandicapTo = "30" },
                new DataRefreshParameters { HandicapFrom = "30,1", HandicapTo = "35" },
                new DataRefreshParameters { HandicapFrom = "35,1", HandicapTo = "40" },
                new DataRefreshParameters { HandicapFrom = "41", HandicapTo = "53" },
                new DataRefreshParameters { HandicapFrom = "54", HandicapTo = "54" }
            };

            var dataRefreshService = _serviceProvider.GetService<IDataRefreshService>();

            foreach (var parameters in queue)
            {
                var result = dataRefreshService.RefreshPlayersData(parameters);

                Console.WriteLine("{0} - duration {1}s, rows count {2}, new players {3}, new memberships {4}, HCP changes {5}",
                    result.Parameters,
                    result.DurationInSeconds,
                    result.RowsCount,
                    result.NewPlayersCount,
                    result.NewMembershipsCount,
                    result.HandicapChangesCount);
            }
        }

        private static void RunSnapshotService()
        {
            Console.WriteLine("Running SnapshotService...");

            _serviceProvider.GetService<ISnapshotService>().CreateSnapshots();
        }

        private static void ConfigureServices()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            var configuration = builder.Build();
            var services = new ServiceCollection();

            services.AddCGFWatcher(configuration.GetConnectionString("WatcherConnection"));

            _serviceProvider = services.BuildServiceProvider();
        }
    }
}
