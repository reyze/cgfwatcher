﻿import { Component, OnInit, Input } from "@angular/core";
import { Language } from "angular-l10n";

import { ClubComparison, MembershipSnapshot } from "../models";
import { LanguageService } from "../../shared";

@Component({
    selector: "cgf-club-comparison-chart-bar",
    templateUrl: "./club-comparison-chart-bar.component.html"
})
export class ClubComparisonChartBarComponent implements OnInit {
    @Language() lang: string;

    @Input() comparison: ClubComparison;

    chartLabels: string[] = [
        "Total", "Home", "Blocked", "HCP < 0", "HCP 0-10", "HCP 10-20", "HCP 20-30", "HCP 30-36", "HCP 37-53", "HCP 54"
    ];

    chartData: any[];

    chartOptions: any = {
        scales: {
            yAxes: [{
                ticks: { callback: (label) => this.formatTickY(label) }
            }]
        },
        tooltips: {
            callbacks: {
                label: (item, data) => this.createTooltipLabel(item, data)
            }
        },
        legend: {
            display: true,
            position: "bottom"
        }
    };

    constructor(private languageService: LanguageService) { }

    ngOnInit() {
        this.languageService.translate("ClubComparison.Chart.Total", text => this.chartLabels[0] = text);
        this.languageService.translate("ClubComparison.Chart.Home", text => this.chartLabels[1] = text);
        this.languageService.translate("ClubComparison.Chart.Blocked", text => this.chartLabels[2] = text);

        this.languageService.subscribeTranslationChange(() => this.handleChartData());

        this.handleChartData();
    }

    private handleChartData() {
        this.chartData = [
            this.getChartData(this.comparison.golfClubData1),
            this.getChartData(this.comparison.golfClubData2)
        ];
    }

    private getChartData(data: MembershipSnapshot) {
        return {
            label: data.golfClub.name,
            data: [
                data.total,
                data.totalHome,
                data.totalBlocked,
                data.handicap_lt_0,
                data.handicap_0_10,
                data.handicap_10_20,
                data.handicap_20_30,
                data.handicap_30_36,
                data.handicap_37_53,
                data.handicap_54
            ]
        };
    }

    private formatTickY(label: number) {
        return label.toLocaleString(this.lang);
    }

    private createTooltipLabel(tooltipItem, data) {
        const label = data.datasets[tooltipItem.datasetIndex].label;
        const value = tooltipItem.yLabel.toLocaleString(this.lang);

        return `${label}: ${value}`;
    }
}
