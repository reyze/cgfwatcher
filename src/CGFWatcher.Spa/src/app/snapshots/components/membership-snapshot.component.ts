﻿import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Language } from "angular-l10n";

import { MembershipSnapshotFilter } from "../services";
import { isEmptyObject } from "../../shared";

@Component({
    selector: "cgf-snapshot-membership",
    templateUrl: "./membership-snapshot.component.html"
})
export class MembershipSnapshotComponent implements OnInit {
    @Language() lang: string;

    filter: MembershipSnapshotFilter;
    submittedFilter: MembershipSnapshotFilter;

    constructor(private router: Router, private route: ActivatedRoute) { }

    ngOnInit() {
        this.route.params.subscribe(
            (filter: MembershipSnapshotFilter) => this.handleRouteParams(filter)
        );
    }

    onFilterSubmit(filter: MembershipSnapshotFilter) {
        this.router.navigate(["/membership-snapshots", filter]);
        this.submittedFilter = filter;
    }

    private handleRouteParams(filter: MembershipSnapshotFilter) {
        filter = Object.assign(new MembershipSnapshotFilter(), filter);

        if (isEmptyObject(filter)) {
            this.setInitialFilterValues();
            this.submittedFilter = null;
        } else {
            this.filter = filter;
            this.submittedFilter = filter;
        }
    }

    private setInitialFilterValues() {
        const today = new Date();
        const lastYear = new Date(today.valueOf() - (365 * 24 * 60 * 60 * 1000));

        this.filter = {
            golfClubPrefix: null,
            dateFrom: lastYear,
            dateTo: today
        };
    }
}
