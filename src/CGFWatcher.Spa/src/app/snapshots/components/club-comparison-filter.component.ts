﻿import { Component, OnInit, EventEmitter, Input, Output } from "@angular/core";
import { Language } from "angular-l10n";

import { ClubComparisonFilter } from "../services";

import { GolfClubService, GolfClub } from "../../shared";

@Component({
    selector: "cgf-club-comparison-filter",
    templateUrl: "./club-comparison-filter.component.html"
})
export class ClubComparisonFilterComponent implements OnInit {
    @Language() lang: string;

    @Input() filter: ClubComparisonFilter;
    @Output() onFilterSubmit = new EventEmitter<ClubComparisonFilter>();

    golfClubs: GolfClub[];

    constructor(private golfClubService: GolfClubService) { }

    ngOnInit() {
        this.getGolfClubs();
    }

    submitFilter() {
        this.onFilterSubmit.emit(this.filter);
    }

    private getGolfClubs() {
        this.golfClubService.getGolfClubs().subscribe(
            clubs => this.golfClubs = clubs
        );
    }
}
