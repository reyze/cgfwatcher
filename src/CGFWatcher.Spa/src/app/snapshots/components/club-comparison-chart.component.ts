﻿import { Component, OnChanges, Input } from "@angular/core";
import { Language } from "angular-l10n";

import { ClubComparison } from "../models";
import { ClubComparisonService, ClubComparisonFilter } from "../services";

@Component({
    selector: "cgf-club-comparison-chart",
    templateUrl: "./club-comparison-chart.component.html",
    styleUrls: ["./club-comparison-chart.component.css"]
})
export class ClubComparisonChartComponent implements OnChanges {
    @Language() lang: string;

    @Input() filter: ClubComparisonFilter;

    comparison: ClubComparison;
    errorMessage: string;
    isLoadingInProgress: boolean;
    hasData: boolean;

    constructor(private clubComparisonService: ClubComparisonService) { }

    ngOnChanges(changeRecord) {
        this.getComparison(this.filter);
    }

    private getComparison(filter: ClubComparisonFilter) {
        if (!filter) {
            return;
        }

        this.errorMessage = null;
        this.isLoadingInProgress = true;

        this.clubComparisonService.getClubComparison(filter).subscribe(
            comparison => this.handleComparisonData(comparison),
            (error: Error) => this.errorMessage = error.message
        );
    }

    private handleComparisonData(comparison: ClubComparison) {
        this.comparison = comparison;
        this.isLoadingInProgress = false;
        this.hasData = (comparison.golfClubData1 != null) && (comparison.golfClubData2 != null);
    }
}
