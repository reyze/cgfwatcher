﻿import { Component, OnInit, OnChanges, Input } from "@angular/core";
import { Language } from "angular-l10n";

import { MembershipSnapshot } from "../models";
import { MembershipSnapshotService, MembershipSnapshotFilter } from "../services";
import { LanguageService } from "../../shared";

@Component({
    selector: "cgf-snapshot-membership-chart",
    templateUrl: "./membership-snapshot-chart.component.html",
    styleUrls: ["./membership-snapshot-chart.component.css"]
})
export class MembershipSnapshotChartComponent implements OnInit, OnChanges {
    @Language() lang: string;

    @Input() filter: MembershipSnapshotFilter;

    snapshots: MembershipSnapshot[];
    errorMessage: string;
    isLoadingInProgress: boolean;

    chartData: Array<any> = [
        { label: "Total", fill: false, lineTension: 0, pointHitRadius: 20 },
        { label: "Home", fill: false, lineTension: 0, pointHitRadius: 20 },
        { label: "Blocked", fill: false, lineTension: 0, pointHitRadius: 20, hidden: true },
        { label: "HCP < 0", fill: false, lineTension: 0, pointHitRadius: 20, hidden: true },
        { label: "HCP 0-10", fill: false, lineTension: 0, pointHitRadius: 20, hidden: true },
        { label: "HCP 10-20", fill: false, lineTension: 0, pointHitRadius: 20, hidden: true },
        { label: "HCP 20-30", fill: false, lineTension: 0, pointHitRadius: 20, hidden: true },
        { label: "HCP 30-36", fill: false, lineTension: 0, pointHitRadius: 20, hidden: true },
        { label: "HCP 37-53", fill: false, lineTension: 0, pointHitRadius: 20, hidden: true },
        { label: "HCP 54", fill: false, lineTension: 0, pointHitRadius: 20, hidden: true }
    ];

    chartOptions: any = {
        scales: {
            xAxes: [{
                type: "time",
                time: { unit: "month" },
                ticks: { callback: (label) => this.formatTickX(label) }
            }],
            yAxes: [{
                ticks: { callback: (label) => this.formatTickY(label) }
            }]
        },
        tooltips: {
            callbacks: {
                title: (items, data) => this.createTooltipTitle(items, data),
                label: (item, data) => this.createTooltipLabel(item, data)
            }
        },
        legend: {
            display: true
        }
    };

    constructor(private membershipSnapshotService: MembershipSnapshotService, private languageService: LanguageService) { }

    ngOnInit() {
        this.languageService.subscribeTranslationChange(() => this.getSnapshots(this.filter));
    }

    ngOnChanges(changeRecord) {
        this.getSnapshots(this.filter);
    }

    private getSnapshots(filter: MembershipSnapshotFilter) {
        if (!filter) {
            return;
        }

        this.errorMessage = null;
        this.isLoadingInProgress = true;

        this.membershipSnapshotService.getMembershipSnapshots(filter).subscribe(
            snapshots => this.handleChartData(snapshots, filter),
            (error: Error) => this.errorMessage = error.message
        );
    }

    private handleChartData(snapshots: MembershipSnapshot[], filter: MembershipSnapshotFilter) {
        this.languageService.translate("MembershipSnapshot.Chart.Total", text => this.chartData[0].label = text);
        this.languageService.translate("MembershipSnapshot.Chart.Home", text => this.chartData[1].label = text);
        this.languageService.translate("MembershipSnapshot.Chart.Blocked", text => this.chartData[2].label = text);

        this.mapChartData(snapshots);
        this.normalizeChartXAxis(filter);

        this.snapshots = snapshots;
        this.isLoadingInProgress = false;
    }

    private formatTickX(label: string) {
        return new Date(label).toLocaleDateString(this.lang);
    }

    private formatTickY(label: number) {
        return label.toLocaleString(this.lang);
    }

    private createTooltipTitle(tooltipItems, data) {
        return new Date(tooltipItems[0].xLabel).toLocaleDateString(this.lang);
    }

    private createTooltipLabel(tooltipItem, data) {
        const label = data.datasets[tooltipItem.datasetIndex].label;
        const value = tooltipItem.yLabel.toLocaleString(this.lang);

        return `${label}: ${value}`;
    }

    private mapChartData(snapshots: MembershipSnapshot[]) {
        this.chartData[0].data = snapshots.map(function (item) {
            return { x: item.created, y: item.total };
        });

        this.chartData[1].data = snapshots.map(function (item) {
            return { x: item.created, y: item.totalHome };
        });

        this.chartData[2].data = snapshots.map(function (item) {
            return { x: item.created, y: item.totalBlocked };
        });

        this.chartData[3].data = snapshots.map(function (item) {
            return { x: item.created, y: item.handicap_lt_0 };
        });

        this.chartData[4].data = snapshots.map(function (item) {
            return { x: item.created, y: item.handicap_0_10 };
        });

        this.chartData[5].data = snapshots.map(function (item) {
            return { x: item.created, y: item.handicap_10_20 };
        });

        this.chartData[6].data = snapshots.map(function (item) {
            return { x: item.created, y: item.handicap_20_30 };
        });

        this.chartData[7].data = snapshots.map(function (item) {
            return { x: item.created, y: item.handicap_30_36 };
        });

        this.chartData[8].data = snapshots.map(function (item) {
            return { x: item.created, y: item.handicap_37_53 };
        });

        this.chartData[9].data = snapshots.map(function (item) {
            return { x: item.created, y: item.handicap_54 };
        });
    }

    private normalizeChartXAxis(filter: MembershipSnapshotFilter) {
        const scaleOptions = this.chartOptions.scales.xAxes[0].time;
        const dateFrom = new Date(filter.dateFrom);
        const dateTo = new Date(filter.dateTo);
        const dateDiff = dateTo.valueOf() - dateFrom.valueOf();

        scaleOptions.min = dateFrom;
        scaleOptions.max = dateTo;

        // period is at least 40 days long? display months on x-axis
        if (dateDiff > 40 * 24 * 60 * 60 * 1000) {
            scaleOptions.unit = "month";
        } else {
            scaleOptions.unit = null;
        }
    }
}
