﻿import { Component, OnInit, Input } from "@angular/core";
import { Language } from "angular-l10n";

import { ClubComparison, MembershipSnapshot } from "../models";

@Component({
    selector: "cgf-club-comparison-chart-radar",
    templateUrl: "./club-comparison-chart-radar.component.html"
})
export class ClubComparisonChartRadarComponent implements OnInit {
    @Language() lang: string;

    @Input() comparison: ClubComparison;

    chartLabels: string[] = ["HCP < 0", "HCP 0-10", "HCP 10-20", "HCP 20-30", "HCP 30-36", "HCP 37-53", "HCP 54"];
    chartData: any[];

    chartOptions: any = {
        scale: {
            ticks: { callback: (label) => this.formatTickY(label) }
        },
        tooltips: {
            callbacks: {
                label: (item, data) => this.createTooltipLabel(item, data)
            }
        },
        legend: {
            display: true,
            position: "bottom"
        }
    };

    ngOnInit() {
        this.chartData = [
            this.getChartData(this.comparison.golfClubData1),
            this.getChartData(this.comparison.golfClubData2)
        ];
    }

    private getChartData(data: MembershipSnapshot) {
        return {
            label: data.golfClub.name,
            data: [
                data.handicap_lt_0,
                data.handicap_0_10,
                data.handicap_10_20,
                data.handicap_20_30,
                data.handicap_30_36,
                data.handicap_37_53,
                data.handicap_54
            ]
        };
    }

    private formatTickY(label: number) {
        return label.toLocaleString(this.lang);
    }

    private createTooltipLabel(tooltipItem, data) {
        const label = data.datasets[tooltipItem.datasetIndex].label;
        const value = tooltipItem.yLabel.toLocaleString(this.lang);

        return `${label}: ${value}`;
    }
}
