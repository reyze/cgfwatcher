﻿import { Component, OnInit, EventEmitter, Input, Output } from "@angular/core";
import { Language } from "angular-l10n";

import { MembershipSnapshotFilter } from "../services";

import { GolfClubService, GolfClub } from "../../shared";

@Component({
    selector: "cgf-snapshot-membership-filter",
    templateUrl: "./membership-snapshot-filter.component.html"
})
export class MembershipSnapshotFilterComponent implements OnInit {
    @Language() lang: string;

    @Input() filter: MembershipSnapshotFilter;
    @Output() onFilterSubmit = new EventEmitter<MembershipSnapshotFilter>();

    golfClubs: GolfClub[];

    constructor(private golfClubService: GolfClubService) { }

    ngOnInit() {
        this.getGolfClubs();
    }

    submitFilter() {
        this.onFilterSubmit.emit(this.filter);
    }

    private getGolfClubs() {
        this.golfClubService.getGolfClubs().subscribe(
            clubs => this.golfClubs = clubs
        );
    }
}
