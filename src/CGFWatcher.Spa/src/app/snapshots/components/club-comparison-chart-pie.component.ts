﻿import { Component, OnInit, Input } from "@angular/core";
import { Language } from "angular-l10n";

import { ClubComparison, MembershipSnapshot } from "../models";

@Component({
    selector: "cgf-club-comparison-chart-pie",
    templateUrl: "./club-comparison-chart-pie.component.html"
})
export class ClubComparisonChartPieComponent implements OnInit {
    @Language() lang: string;

    @Input() comparison: ClubComparison;

    chartLabels: string[] = ["HCP < 0", "HCP 0-10", "HCP 10-20", "HCP 20-30", "HCP 30-36", "HCP 37-53", "HCP 54"];
    chartData1: number[];
    chartData2: number[];

    chartOptions: any = {
        legend: {
            display: true,
            position: "bottom"
        },
        tooltips: {
            callbacks: {
                label: (item, data) => this.createPercentageLabel(item, data)
            }
        }
    };

    ngOnInit() {
        this.chartData1 = this.getChartData(this.comparison.golfClubData1);
        this.chartData2 = this.getChartData(this.comparison.golfClubData2);
    }

    golfClubName1() {
        return this.comparison.golfClubData1.golfClub.name;
    }

    golfClubName2() {
        return this.comparison.golfClubData2.golfClub.name;
    }

    private getChartData(data: MembershipSnapshot) {
        return [
            data.handicap_lt_0 / data.total,
            data.handicap_0_10 / data.total,
            data.handicap_10_20 / data.total,
            data.handicap_20_30 / data.total,
            data.handicap_30_36 / data.total,
            data.handicap_37_53 / data.total,
            data.handicap_54 / data.total
        ];
    }

    private createPercentageLabel(tooltipItem, data) {
        const label = data.labels[tooltipItem.index];
        const dataset = data.datasets[tooltipItem.datasetIndex];
        const currentValue = dataset.data[tooltipItem.index];

        const precentage = currentValue.toLocaleString(this.lang, {
            style: "percent",
            minimumFractionDigits: 1,
            maximumFractionDigits: 1
        });

        return `${label}: ${precentage}`;
    }
}
