﻿import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Language } from "angular-l10n";

import { ClubComparisonFilter } from "../services";
import { isEmptyObject } from "../../shared";

@Component({
    selector: "cgf-club-comparison",
    templateUrl: "./club-comparison.component.html"
})
export class ClubComparisonComponent implements OnInit {
    @Language() lang: string;

    filter: ClubComparisonFilter;
    submittedFilter: ClubComparisonFilter;

    constructor(private router: Router, private route: ActivatedRoute) { }

    ngOnInit() {
        this.route.params.subscribe(
            (filter: ClubComparisonFilter) => this.handleRouteParams(filter)
        );
    }

    onFilterSubmit(filter: ClubComparisonFilter) {
        this.router.navigate(["/club-comparison", filter]);
        this.submittedFilter = filter;
    }

    private handleRouteParams(filter: ClubComparisonFilter) {
        filter = Object.assign(new ClubComparisonFilter(), filter);

        if (isEmptyObject(filter)) {
            this.setInitialFilterValues();
            this.submittedFilter = null;
        } else {
            this.filter = filter;
            this.submittedFilter = filter;
        }
    }

    private setInitialFilterValues() {
        const today = new Date();

        this.filter = {
            date: today,
            golfClubPrefix1: null,
            golfClubPrefix2: null
        };
    }
}
