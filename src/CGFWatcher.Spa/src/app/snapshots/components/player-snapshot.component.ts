﻿import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Language } from "angular-l10n";

import { PlayerSnapshotFilter } from "../services";
import { isEmptyObject } from "../../shared";

@Component({
    selector: "cgf-snapshot-player",
    templateUrl: "./player-snapshot.component.html"
})
export class PlayerSnapshotComponent implements OnInit {
    @Language() lang: string;

    filter: PlayerSnapshotFilter;
    submittedFilter: PlayerSnapshotFilter;

    constructor(private router: Router, private route: ActivatedRoute) { }

    ngOnInit() {
        this.route.params.subscribe(
            (filter: PlayerSnapshotFilter) => this.handleRouteParams(filter)
        );
    }

    onFilterSubmit(filter: PlayerSnapshotFilter) {
        this.router.navigate(["/player-snapshots", filter]);
        this.submittedFilter = filter;
    }

    private handleRouteParams(filter: PlayerSnapshotFilter) {
        filter = Object.assign(new PlayerSnapshotFilter(), filter);

        if (isEmptyObject(filter)) {
            this.setInitialFilterValues();
            this.submittedFilter = null;
        } else {
            this.filter = filter;
            this.submittedFilter = filter;
        }
    }

    private setInitialFilterValues() {
        const today = new Date();
        const lastYear = new Date(today.valueOf() - (365 * 24 * 60 * 60 * 1000));

        this.filter = {
            dateFrom: lastYear,
            dateTo: today
        };
    }
}
