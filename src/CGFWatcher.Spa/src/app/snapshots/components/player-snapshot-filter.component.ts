﻿import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Language } from "angular-l10n";

import { PlayerSnapshotFilter } from "../services";

@Component({
    selector: "cgf-snapshot-player-filter",
    templateUrl: "./player-snapshot-filter.component.html"
})
export class PlayerSnapshotFilterComponent {
    @Language() lang: string;

    @Input() filter: PlayerSnapshotFilter;
    @Output() onFilterSubmit = new EventEmitter<PlayerSnapshotFilter>();

    submitFilter() {
        this.onFilterSubmit.emit(this.filter);
    }
}
