import { NgModule } from "@angular/core";

import { SharedModule } from "../shared";

import {
    PlayerSnapshotComponent, PlayerSnapshotFilterComponent, PlayerSnapshotChartComponent,
    MembershipSnapshotComponent, MembershipSnapshotFilterComponent, MembershipSnapshotChartComponent,
    ClubComparisonComponent, ClubComparisonFilterComponent, ClubComparisonChartComponent,
    ClubComparisonChartBarComponent, ClubComparisonChartRadarComponent, ClubComparisonChartPieComponent
} from "./components";

import { snapshotsRouting } from "./snapshots.routing";

@NgModule({
    declarations: [
        PlayerSnapshotComponent,
        PlayerSnapshotFilterComponent,
        PlayerSnapshotChartComponent,
        MembershipSnapshotComponent,
        MembershipSnapshotFilterComponent,
        MembershipSnapshotChartComponent,
        ClubComparisonComponent,
        ClubComparisonFilterComponent,
        ClubComparisonChartComponent,
        ClubComparisonChartBarComponent,
        ClubComparisonChartRadarComponent,
        ClubComparisonChartPieComponent
    ],
    imports: [SharedModule, snapshotsRouting]
})
export class SnapshotsModule { }
