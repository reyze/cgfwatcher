﻿import { TestBed, inject } from "@angular/core/testing";
import { HttpClient } from "@angular/common/http";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";

import { MembershipSnapshotService } from "./membership-snapshot.service";
import { MembershipSnapshotFilter } from "./membership-snapshot.filter";
import { MembershipSnapshot } from "../models";
import { ConfigurationService } from "../../shared";

describe("MembershipSnapshotService", () => {
    let configuration: ConfigurationService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule]
        });

        configuration = <ConfigurationService>{
            apiUrl(path: string): string {
                return "http://example.com".concat(path);
            }
        };
    });

    afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
        httpMock.verify();
    }));

    describe("getMembershipSnapshots()", () => {
        it("should call api to get a list of membership snapshots", inject([
            HttpClient, HttpTestingController
        ], (
            http: HttpClient,
            httpMock: HttpTestingController
        ) => {
            const service = new MembershipSnapshotService(http, configuration);

            const filter: MembershipSnapshotFilter = {
                golfClubPrefix: "321",
                dateFrom: <any>"2016-12-20",
                dateTo: <any>"2016-12-30"
            };

            service.getMembershipSnapshots(filter).subscribe((snapshots: MembershipSnapshot[]) => {
                expect(snapshots.length).toBe(2);
                expect(snapshots[0]).toEqual({
                    id: 1, created: new Date(2016, 12, 22), total: 863, totalHome: 654, totalBlocked: 29,
                    handicap_lt_0: 5, handicap_0_10: 8, handicap_10_20: 120, handicap_20_30: 150,
                    handicap_30_36: 234, handicap_37_53: 80, handicap_54: 210, golfClub: null
                });
            });

            const request = httpMock.expectOne("http://example.com/api/membershipsnapshots"
                + "?golfClubPrefix=321&dateFrom=2016-12-20&dateTo=2016-12-30");

            request.flush([
                {
                    id: 1, created: new Date(2016, 12, 22), total: 863, totalHome: 654, totalBlocked: 29,
                    handicap_lt_0: 5, handicap_0_10: 8, handicap_10_20: 120, handicap_20_30: 150,
                    handicap_30_36: 234, handicap_37_53: 80, handicap_54: 210, golfClub: null
                },
                {
                    id: 2, created: new Date(2016, 12, 24), total: 102, totalHome: 87, totalBlocked: 2,
                    handicap_lt_0: 0, handicap_0_10: 3, handicap_10_20: 5, handicap_20_30: 25,
                    handicap_30_36: 32, handicap_37_53: 28, handicap_54: 18, golfClub: null
                }
            ]);
        }));
    });
});
