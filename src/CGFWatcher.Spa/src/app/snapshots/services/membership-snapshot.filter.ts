﻿export class MembershipSnapshotFilter {
    golfClubPrefix: string;
    dateFrom: Date;
    dateTo: Date;
}
