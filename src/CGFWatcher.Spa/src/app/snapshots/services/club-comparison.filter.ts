﻿export class ClubComparisonFilter {
    date: Date;
    golfClubPrefix1: string;
    golfClubPrefix2: string;
}
