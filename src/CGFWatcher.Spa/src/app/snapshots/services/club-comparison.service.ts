import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";

import { ClubComparison, ClubComparisonFilter } from "../";
import { ConfigurationService } from "../../shared";

@Injectable({
    providedIn: "root"
})
export class ClubComparisonService {
    private urlPath = "/api/clubcomparison";

    constructor(private http: HttpClient, private configuration: ConfigurationService) { }

    getClubComparison(filter: ClubComparisonFilter): Observable<ClubComparison> {
        const url = this.configuration.apiUrl(this.urlPath);

        const params = new HttpParams()
            .set("date", filter.date.toString())
            .set("golfClubPrefix1", filter.golfClubPrefix1)
            .set("golfClubPrefix2", filter.golfClubPrefix2);

        return this.http.get<ClubComparison>(url, {
            params: params
        });
    }
}
