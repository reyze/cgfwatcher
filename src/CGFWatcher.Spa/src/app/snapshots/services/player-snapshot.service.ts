import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";

import { PlayerSnapshot, PlayerSnapshotFilter } from "../";
import { ConfigurationService } from "../../shared";

@Injectable({
    providedIn: "root"
})
export class PlayerSnapshotService {
    private urlPath = "/api/playersnapshots";

    constructor(private http: HttpClient, private configuration: ConfigurationService) { }

    getPlayerSnapshots(filter: PlayerSnapshotFilter): Observable<PlayerSnapshot[]> {
        const url = this.configuration.apiUrl(this.urlPath);

        const params = new HttpParams()
            .set("dateFrom", filter.dateFrom.toString())
            .set("dateTo", filter.dateTo.toString());

        return this.http.get<PlayerSnapshot[]>(url, {
            params: params
        });
    }
}
