import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";

import { MembershipSnapshot, MembershipSnapshotFilter } from "../";
import { ConfigurationService } from "../../shared";

@Injectable({
    providedIn: "root"
})
export class MembershipSnapshotService {
    private urlPath = "/api/membershipsnapshots";

    constructor(private http: HttpClient, private configuration: ConfigurationService) { }

    getMembershipSnapshots(filter: MembershipSnapshotFilter): Observable<MembershipSnapshot[]> {
        const url = this.configuration.apiUrl(this.urlPath);

        const params = new HttpParams()
            .set("golfClubPrefix", filter.golfClubPrefix)
            .set("dateFrom", filter.dateFrom.toString())
            .set("dateTo", filter.dateTo.toString());

        return this.http.get<MembershipSnapshot[]>(url, {
            params: params
        });
    }
}
