﻿import { TestBed, inject } from "@angular/core/testing";
import { HttpClient } from "@angular/common/http";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";

import { ClubComparisonService } from "./club-comparison.service";
import { ClubComparisonFilter } from "./club-comparison.filter";
import { ClubComparison } from "../models";
import { ConfigurationService } from "../../shared";

describe("ClubComparisonService", () => {
    let configuration: ConfigurationService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule]
        });

        configuration = <ConfigurationService>{
            apiUrl(path: string): string {
                return "http://example.com".concat(path);
            }
        };
    });

    afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
        httpMock.verify();
    }));

    describe("getClubComparison()", () => {
        it("should call api to get the club comparison", inject([
            HttpClient, HttpTestingController
        ], (
            http: HttpClient,
            httpMock: HttpTestingController
        ) => {
            const service = new ClubComparisonService(http, configuration);

            const filter: ClubComparisonFilter = {
                date: <any>"2016-12-20",
                golfClubPrefix1: "321",
                golfClubPrefix2: "ABC"
            };

            service.getClubComparison(filter).subscribe((comparison: ClubComparison) => {
                expect(comparison).toBeDefined();
                expect(comparison.golfClubData1).toEqual({
                    id: 1, created: new Date(2016, 12, 22), total: 863, totalHome: 654, totalBlocked: 29,
                    handicap_lt_0: 5, handicap_0_10: 8, handicap_10_20: 120, handicap_20_30: 150,
                    handicap_30_36: 234, handicap_37_53: 80, handicap_54: 210, golfClub: null
                });
                expect(comparison.golfClubData2).toEqual({
                    id: 2, created: new Date(2016, 12, 24), total: 102, totalHome: 87, totalBlocked: 2,
                    handicap_lt_0: 0, handicap_0_10: 3, handicap_10_20: 5, handicap_20_30: 25,
                    handicap_30_36: 32, handicap_37_53: 28, handicap_54: 18, golfClub: null
                });
            });

            const request = httpMock.expectOne("http://example.com/api/clubcomparison"
                + "?date=2016-12-20&golfClubPrefix1=321&golfClubPrefix2=ABC");

            request.flush({
                golfClubData1: {
                    id: 1, created: new Date(2016, 12, 22), total: 863, totalHome: 654, totalBlocked: 29,
                    handicap_lt_0: 5, handicap_0_10: 8, handicap_10_20: 120, handicap_20_30: 150,
                    handicap_30_36: 234, handicap_37_53: 80, handicap_54: 210, golfClub: null
                },
                golfClubData2: {
                    id: 2, created: new Date(2016, 12, 24), total: 102, totalHome: 87, totalBlocked: 2,
                    handicap_lt_0: 0, handicap_0_10: 3, handicap_10_20: 5, handicap_20_30: 25,
                    handicap_30_36: 32, handicap_37_53: 28, handicap_54: 18, golfClub: null
                }
            });
        }));
    });
});
