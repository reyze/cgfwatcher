﻿import { GolfClub } from "../../shared";

export class MembershipSnapshot {
    id: number;
    created: Date;

    total: number;
    totalHome: number;
    totalBlocked: number;

    handicap_lt_0: number;
    handicap_0_10: number;
    handicap_10_20: number;
    handicap_20_30: number;
    handicap_30_36: number;
    handicap_37_53: number;
    handicap_54: number;

    golfClub: GolfClub;
}
