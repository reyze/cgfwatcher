﻿import { MembershipSnapshot } from "./membership-snapshot";

export class ClubComparison {
    golfClubData1: MembershipSnapshot;
    golfClubData2: MembershipSnapshot;
}
