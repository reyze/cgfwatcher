﻿import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { PlayerSnapshotComponent, MembershipSnapshotComponent, ClubComparisonComponent } from "./components";

const snapshotsRoutes: Routes = [
    { path: "membership-snapshots", component: MembershipSnapshotComponent },
    { path: "player-snapshots", component: PlayerSnapshotComponent },
    { path: "club-comparison", component: ClubComparisonComponent },
];

export const snapshotsRouting: ModuleWithProviders = RouterModule.forChild(snapshotsRoutes);
