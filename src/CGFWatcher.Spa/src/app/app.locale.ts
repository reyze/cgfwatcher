import { LOCALE_ID, ModuleWithProviders } from "@angular/core";
import { TranslationModule, LocaleValidationModule, L10nConfig, StorageStrategy, ProviderType } from "angular-l10n";

import { LanguageService } from "./shared";

const l10nConfig: L10nConfig = {
    locale: {
        languages: [
            { code: "en", dir: "ltr" },
            { code: "cs", dir: "ltr" }
        ],
        storage: StorageStrategy.Disabled
    },
    translation: {
        providers: [
            { type: ProviderType.Static, prefix: "./resources/locale-" }
        ],
        caching: true
    }
};

export function localeFactory(service: LanguageService) {
    return service.getLocale();
}

export const localeProviders: any[] = [
    {
        provide: LOCALE_ID,
        useFactory: localeFactory,
        deps: [LanguageService]
    }
];

export const locale: ModuleWithProviders[] = [
    TranslationModule.forRoot(l10nConfig),
    LocaleValidationModule.forRoot()
];
