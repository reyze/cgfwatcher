import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { SharedModule, LanguageService } from "./shared";
import { PlayersModule } from "./players";
import { SnapshotsModule } from "./snapshots";

import { AppComponent } from "./app.component";
import { routing, appRoutingProviders } from "./app.routing";
import { locale, localeProviders } from "./app.locale";

@NgModule({
    declarations: [AppComponent],
    imports: [BrowserModule, SharedModule, PlayersModule, SnapshotsModule, routing, locale],
    providers: [appRoutingProviders, localeProviders],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(private languageService: LanguageService) {
        languageService.initializeLocalizationInfrastructure();
    }
}
