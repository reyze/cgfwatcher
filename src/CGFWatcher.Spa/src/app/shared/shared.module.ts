import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { LocalizationModule, LocaleValidationModule } from "angular-l10n";
import { ChartsModule } from "ng2-charts/ng2-charts";

import { PagerComponent, MenuComponent } from "./components";

import {
    DialogDirective,
    SuccessLabelDirective,
    WarningLabelDirective,
    AlertLabelDirective,
    DateValueAccessorDirective
} from "./directives";

@NgModule({
    declarations: [
        PagerComponent, MenuComponent,
        DialogDirective, SuccessLabelDirective, WarningLabelDirective, AlertLabelDirective,
        DateValueAccessorDirective
    ],
    imports: [CommonModule, HttpClientModule, RouterModule, LocalizationModule],
    exports: [
        CommonModule, HttpClientModule, FormsModule,
        ChartsModule, LocalizationModule, LocaleValidationModule,
        PagerComponent, MenuComponent,
        DialogDirective, SuccessLabelDirective, WarningLabelDirective, AlertLabelDirective,
        DateValueAccessorDirective
    ]
})
export class SharedModule { }
