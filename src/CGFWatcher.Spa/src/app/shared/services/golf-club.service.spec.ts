﻿import { TestBed, inject } from "@angular/core/testing";
import { HttpClient } from "@angular/common/http";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";

import { GolfClubService } from "./golf-club.service";
import { ConfigurationService } from "./configuration.service";
import { GolfClub } from "../models";

describe("GolfClubService", () => {
    let configuration: ConfigurationService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule]
        });

        configuration = <ConfigurationService>{
            apiUrl(path: string): string {
                return "http://example.com".concat(path);
            }
        };
    });

    afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
        httpMock.verify();
    }));

    describe("getGolfClubs()", () => {
        it("should call api to get a list of golf clubs", inject([
            HttpClient, HttpTestingController
        ], (
            http: HttpClient,
            httpMock: HttpTestingController
        ) => {
            const service = new GolfClubService(http, configuration);

            service.getGolfClubs().subscribe((clubs: GolfClub[]) => {
                expect(clubs.length).toBe(2);
                expect(clubs[0]).toEqual({ id: 123, prefix: "ABC", name: "Prague" });
            });

            const request = httpMock.expectOne("http://example.com/api/golfclubs");

            request.flush([
                { id: 123, prefix: "ABC", name: "Prague" },
                { id: 321, prefix: "XYZ", name: "London" }
            ]);
        }));
    });
});
