import { Injectable } from "@angular/core";

import { environment } from "../../../environments";

@Injectable({
    providedIn: "root"
})
export class ConfigurationService {
    get defaultLocale(): string {
        return environment.defaultLocale;
    }

    apiUrl(path: string): string {
        return environment.apiUrl.concat(path);
    }
}
