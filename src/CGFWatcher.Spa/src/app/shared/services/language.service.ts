import { Injectable } from "@angular/core";
import { registerLocaleData } from "@angular/common";
import { L10nLoader, LocaleService, TranslationService } from "angular-l10n";

import { ConfigurationService } from "./configuration.service";

import localeCs from "@angular/common/locales/cs";

@Injectable({
    providedIn: "root"
})
export class LanguageService {
    private currentLocale: string;

    constructor(
        private l10nLoader: L10nLoader,
        private locale: LocaleService,
        private translation: TranslationService,
        private configuration: ConfigurationService
    ) {
        this.currentLocale = localStorage.getItem("locale") || configuration.defaultLocale;
    }

    getLocale(): string {
        return this.currentLocale;
    }

    getLanguage(): string {
        return this.currentLocale.split("-")[0];
    }

    setLocale(locale: string) {
        // to change locale without reloading the page we need to use angular-l10n pipes only
        // (Angular LOCALE_ID is not refreshed in real time)
        this.currentLocale = locale;
        this.locale.setCurrentLanguage(this.getLanguage());

        // app will be loaded with the new locale next time
        localStorage.setItem("locale", locale);
    }

    initializeLocalizationInfrastructure() {
        registerLocaleData(localeCs);

        this.locale.setCurrentLanguage(this.getLanguage());
        this.l10nLoader.load();
    }

    translate(key: string, handler: (text: string) => void) {
        this.translation.translateAsync(key).subscribe(handler);

        this.translation.translationChanged().subscribe(
            () => this.translation.translateAsync(key).subscribe(handler)
        );
    }

    subscribeTranslationChange(handler: () => void) {
        this.translation.translationChanged().subscribe(handler);
    }
}
