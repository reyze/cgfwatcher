import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { ConfigurationService } from "./configuration.service";
import { GolfClub } from "../models";

@Injectable({
    providedIn: "root"
})
export class GolfClubService {
    private urlPath = "/api/golfclubs";

    constructor(private http: HttpClient, private configuration: ConfigurationService) { }

    getGolfClubs(): Observable<GolfClub[]> {
        const url = this.configuration.apiUrl(this.urlPath);

        return this.http.get<GolfClub[]>(url);
    }
}
