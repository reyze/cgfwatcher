﻿import { Component } from "@angular/core";
import { Language } from "angular-l10n";

import { LanguageService } from "../services";

@Component({
    selector: "cgf-menu",
    templateUrl: "./menu.component.html"
})
export class MenuComponent {
    @Language() lang: string;

    availableLanguages: any[] = [
        { locale: "cs-CZ", label: "CZ" },
        { locale: "en-US", label: "EN" }
    ];

    constructor(private languageService: LanguageService) { }

    changeLanguage(locale: string) {
        this.languageService.setLocale(locale);
    }

    isCurrentLocale(locale: string) {
        return (locale === this.languageService.getLocale());
    }
}
