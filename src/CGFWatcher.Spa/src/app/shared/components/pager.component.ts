﻿import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Language } from "angular-l10n";

import { PaginationFilter, PagedList } from "../models";

@Component({
    selector: "cgf-pager",
    templateUrl: "./pager.component.html",
    styleUrls: ["./pager.component.css"]
})
export class PagerComponent<T> {
    private pageNumbersPadding = 2;

    @Language() lang: string;

    @Input() filter: PaginationFilter;
    @Input() list: PagedList<T>;
    @Output() onPageChanged = new EventEmitter<number>();

    itemsFrom() {
        return (this.filter.perPage * (this.filter.page - 1)) + 1;
    }

    itemsTo() {
        const calculatedTo = (this.filter.perPage * this.filter.page);

        if (this.list.totalCount < calculatedTo) {
            return this.list.totalCount;
        }

        return calculatedTo;
    }

    pageCount() {
        return Math.ceil(this.list.totalCount / this.filter.perPage);
    }

    pageNumbers() {
        let from = this.filter.page - this.pageNumbersPadding;
        let to = this.filter.page + this.pageNumbersPadding;
        const pageNumbers: number[] = [];

        if (from < 1) {
            from = 1;
        }

        if (to > this.pageCount()) {
            to = this.pageCount();
        }

        while (from <= to) {
            pageNumbers.push(from);
            from++;
        }

        return pageNumbers;
    }

    showFrontHellip() {
        return (this.filter.page - this.pageNumbersPadding) > 1;
    }

    showBackHellip() {
        return (this.filter.page + this.pageNumbersPadding) < this.pageCount();
    }

    changePage(pageNumber: number) {
        this.onPageChanged.emit(pageNumber);
    }
}
