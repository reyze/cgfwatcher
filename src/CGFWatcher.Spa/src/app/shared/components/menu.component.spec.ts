import { Component } from "@angular/core";
import { TestBed, ComponentFixture, async, inject } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { Router } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";
import { TranslationModule, L10nConfig, StorageStrategy, L10nLoader } from "angular-l10n";

import { MenuComponent } from "./menu.component";
import { LanguageService, ConfigurationService } from "../services";

@Component({ template: "" }) class DummyComponent { }
@Component({ template: "<cgf-menu></cgf-menu><router-outlet></router-outlet>" }) class OutletComponent { }

describe("MenuComponent", () => {
    const l10nConfig: L10nConfig = {
        locale: {
            languages: [{ code: "en", dir: "ltr" }],
            language: "en",
            storage: StorageStrategy.Disabled
        },
        translation: {
            translationData: [{
                languageCode: "en",
                data: {
                    "Menu.MembershipSnapshots": "MEMBERSHIP SNAPSHOTS"
                }
            }]
        }
    };

    let fixture: ComponentFixture<MenuComponent>;
    let component: MenuComponent;
    let service: LanguageService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                TranslationModule.forRoot(l10nConfig),
                HttpClientModule,
                RouterTestingModule.withRoutes([
                    { path: "membership-snapshots", component: DummyComponent }
                ])
            ],
            declarations: [MenuComponent, DummyComponent, OutletComponent],
            providers: [LanguageService, ConfigurationService]
        }).compileComponents();

        fixture = TestBed.createComponent(MenuComponent);
        component = fixture.componentInstance;

        service = fixture.debugElement.injector.get(LanguageService);

        TestBed.get(L10nLoader).load();
    }));

    describe("Template: language selector", () => {
        beforeEach(async(() => {
            fixture.detectChanges();
        }));

        it("should display available languages", () => {
            const els = fixture.debugElement.queryAll(By.css(".button-group > a"));

            expect(els.length).toBe(2);
            expect(els[0].nativeElement.textContent.trim()).toBe("CZ");
            expect(els[1].nativeElement.textContent.trim()).toBe("EN");
        });

        it("should place emphasis to selected language", () => {
            const spy = spyOn(service, "getLocale").and.returnValue("cs-CZ");

            assertHollowClasses("CZ", "EN");

            spy.and.returnValue("en-US");
            fixture.detectChanges();

            assertHollowClasses("EN", "CZ");

            function assertHollowClasses(activeLabel: string, inactiveLabel: string) {
                const active = fixture.debugElement.query(By.css(".button-group > a:not(.hollow)")).nativeElement;
                const inactive = fixture.debugElement.query(By.css(".button-group > a.hollow")).nativeElement;

                expect(active.textContent).toContain(activeLabel);
                expect(inactive.textContent).toContain(inactiveLabel);
            }
        });
    });

    describe("Template: language selector click event handling", () => {
        beforeEach(async(() => {
            fixture.detectChanges();
        }));

        it("should call setLocale method with given parameter", () => {
            spyOn(service, "setLocale");

            const els = fixture.debugElement.queryAll(By.css(".button-group > a"));

            els[0].triggerEventHandler("click", null);
            expect(service.setLocale).toHaveBeenCalledWith("cs-CZ");

            els[1].triggerEventHandler("click", null);
            expect(service.setLocale).toHaveBeenCalledWith("en-US");
        });
    });

    describe("Template: menu links", () => {
        let outletFixture: ComponentFixture<OutletComponent>;

        beforeEach(async(() => {
            outletFixture = TestBed.createComponent(OutletComponent);
            outletFixture.detectChanges();
        }));

        it("should highlight active pag", inject([Router], (router: Router) => {
            router.navigate(["/membership-snapshots"]).then(() => {
                const active = outletFixture.debugElement.query(By.css(".menu > li.active")).nativeElement;

                expect(active.textContent).toBe("MEMBERSHIP SNAPSHOTS");
            });
        }));
    });

    describe("changeLanguage()", () => {
        it("should call setLocale method with given parameter", () => {
            spyOn(service, "setLocale");

            component.changeLanguage("cs-CZ");

            expect(service.setLocale).toHaveBeenCalledWith("cs-CZ");
        });
    });

    describe("isCurrentLocale()", () => {
        it("should return true when current locale equals specified parameter", () => {
            const spy = spyOn(service, "getLocale").and.returnValue("cs-CZ");

            expect(component.isCurrentLocale("en-US")).toBeFalsy();
            expect(component.isCurrentLocale("cs-CZ")).toBeTruthy();

            spy.and.returnValue("en-US");

            expect(component.isCurrentLocale("en-US")).toBeTruthy();
            expect(component.isCurrentLocale("cs-CZ")).toBeFalsy();
        });
    });
});
