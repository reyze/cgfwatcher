import { TestBed, ComponentFixture, async } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { LocalizationModule, L10nConfig, StorageStrategy, L10nLoader } from "angular-l10n";

import { PagerComponent } from "./pager.component";

describe("PagerComponent", () => {
    const l10nConfig: L10nConfig = {
        locale: {
            languages: [{ code: "en", dir: "ltr" }],
            language: "en",
            storage: StorageStrategy.Disabled
        },
        translation: {
            translationData: [{
                languageCode: "en",
                data: {
                    "Pager.NoData": "NO DATA MESSAGE"
                }
            }]
        }
    };

    let fixture: ComponentFixture<PagerComponent<{}>>;
    let component: PagerComponent<{}>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [LocalizationModule.forRoot(l10nConfig), HttpClientModule],
            declarations: [PagerComponent]
        }).compileComponents();

        fixture = TestBed.createComponent(PagerComponent);

        component = fixture.componentInstance;

        TestBed.get(L10nLoader).load();
    }));

    describe("Template: items count section", () => {
        beforeEach(async(() => {
            component.filter = { page: 2, perPage: 20 };
            component.list = { totalCount: 2500, data: [] };

            fixture.detectChanges();
        }));

        it("should display items range", () => {
            const el = fixture.debugElement.queryAll(By.css("p > strong"))[0].nativeElement;

            expect(el.textContent).toContain("21–40");
        });

        it("should display total count", () => {
            const el = fixture.debugElement.queryAll(By.css("p > strong"))[1].nativeElement;

            expect(el.textContent).toContain("2,500");
        });
    });

    describe("Template: pager control section", () => {
        beforeEach(async(() => {
            component.filter = { page: 2, perPage: 20 };
            component.list = { totalCount: 250, data: [] };

            fixture.detectChanges();
        }));

        it("should display page numbers", () => {
            const selector = By.css(".pagination li:not(.pagination-previous):not(.pagination-next):not(.ellipsis)");

            let els = fixture.debugElement.queryAll(selector);

            expect(els.length).toBe(4);
            expect(els[0].nativeElement.textContent.trim()).toBe("1");
            expect(els[1].nativeElement.textContent.trim()).toBe("You're on page 2");
            expect(els[2].nativeElement.textContent.trim()).toBe("3");
            expect(els[3].nativeElement.textContent.trim()).toBe("4");

            component.filter = { page: 8, perPage: 20 };
            fixture.detectChanges();

            els = fixture.debugElement.queryAll(selector);

            expect(els.length).toBe(5);
            expect(els[0].nativeElement.textContent.trim()).toBe("6");
            expect(els[1].nativeElement.textContent.trim()).toBe("7");
            expect(els[2].nativeElement.textContent.trim()).toBe("You're on page 8");
            expect(els[3].nativeElement.textContent.trim()).toBe("9");
            expect(els[4].nativeElement.textContent.trim()).toBe("10");
        });

        it("should decorate anchors tags with aria-labels", () => {
            const els = fixture.debugElement.queryAll(By.css(".pagination a"));

            expect(els.length).toBe(5);
            expect(els[0].attributes["aria-label"]).toBe("Previous page");
            expect(els[1].attributes["aria-label"]).toBe("Page 1");
            expect(els[2].attributes["aria-label"]).toBe("Page 3");
            expect(els[3].attributes["aria-label"]).toBe("Page 4");
            expect(els[4].attributes["aria-label"]).toBe("Next page");
        });

        it("should display previous button if needed", () => {
            const selector = By.css(".pagination-previous");

            expect(fixture.debugElement.query(selector)).toBeDefined();

            component.filter = { page: 1, perPage: 20 };
            fixture.detectChanges();

            expect(fixture.debugElement.query(selector)).toBeNull();
        });

        it("should display front ellipsis if needed", () => {
            const selector = By.css(".pagination li:nth-of-type(2)");

            let el = fixture.debugElement.query(selector).nativeElement;
            expect(el.className).not.toBe("ellipsis");

            component.filter = { page: 5, perPage: 20 };
            fixture.detectChanges();

            el = fixture.debugElement.query(selector).nativeElement;
            expect(el.className).toBe("ellipsis");
        });

        it("should display back ellipsis if needed", () => {
            const selector = By.css(".pagination li:nth-last-of-type(2)");

            let el = fixture.debugElement.query(selector).nativeElement;
            expect(el.className).toBe("ellipsis");

            component.filter = { page: 3, perPage: 50 };
            fixture.detectChanges();

            el = fixture.debugElement.query(selector).nativeElement;
            expect(el.className).not.toBe("ellipsis");
        });

        it("should display next button if needed", () => {
            const selector = By.css(".pagination-next");

            expect(fixture.debugElement.query(selector)).toBeDefined();

            component.filter = { page: 5, perPage: 20 };
            component.list = { totalCount: 100, data: [] };
            fixture.detectChanges();

            expect(fixture.debugElement.query(selector)).toBeNull();

        });
    });

    describe("Template: no data", () => {
        beforeEach(async(() => {
            component.filter = { page: 2, perPage: 20 };
            component.list = { totalCount: 0, data: [] };

            fixture.detectChanges();
        }));

        it("should display no data message", () => {
            const el = fixture.debugElement.queryAll(By.css("p > em"))[0].nativeElement;

            expect(el.textContent).toContain("NO DATA MESSAGE");
        });

        it("should not display items count section", () => {
            const els = fixture.debugElement.queryAll(By.css("p > strong"));

            expect(els.length).toBe(0);
        });

        it("should not display pager control section", () => {
            const els = fixture.debugElement.queryAll(By.css(".pagination"));

            expect(els.length).toBe(0);
        });
    });

    describe("Template: click event handling", () => {
        beforeEach(async(() => {
            component.filter = { page: 8, perPage: 20 };
            component.list = { totalCount: 250, data: [] };

            fixture.detectChanges();
        }));

        it("should raise onPageChanged event when page number clicked", () => {
            let newPage: number;
            const els = fixture.debugElement.queryAll(By.css(".pagination li:not(.pagination-previous):not(.pagination-next) a"));

            component.onPageChanged.subscribe((page: number) => newPage = page);

            expect(els.length).toBe(4);

            els[0].triggerEventHandler("click", null);
            expect(newPage).toBe(6);

            els[1].triggerEventHandler("click", null);
            expect(newPage).toBe(7);

            els[2].triggerEventHandler("click", null);
            expect(newPage).toBe(9);

            els[3].triggerEventHandler("click", null);
            expect(newPage).toBe(10);
        });

        it("should raise onPageChanged event when previous page link clicked", () => {
            const selector = By.css(".pagination li.pagination-previous a");

            let newPage: number;
            const el = fixture.debugElement.query(selector);

            component.onPageChanged.subscribe((page: number) => newPage = page);

            el.triggerEventHandler("click", null);
            expect(newPage).toBe(7);

            component.filter = { page: 4, perPage: 20 };
            fixture.detectChanges();

            el.triggerEventHandler("click", null);
            expect(newPage).toBe(3);
        });

        it("should raise onPageChanged event when next page link clicked", () => {
            const selector = By.css(".pagination li.pagination-next a");

            let newPage: number;
            const el = fixture.debugElement.query(selector);

            component.onPageChanged.subscribe((page: number) => newPage = page);

            el.triggerEventHandler("click", null);
            expect(newPage).toBe(9);

            component.filter = { page: 4, perPage: 20 };
            fixture.detectChanges();

            el.triggerEventHandler("click", null);
            expect(newPage).toBe(5);
        });
    });

    describe("itemsFrom()", () => {
        it("should return index of first displayed item based on filter parameters", () => {
            component.filter = { page: 1, perPage: 20 };
            expect(component.itemsFrom()).toBe(1);

            component.filter = { page: 2, perPage: 20 };
            expect(component.itemsFrom()).toBe(21);

            component.filter = { page: 2, perPage: 30 };
            expect(component.itemsFrom()).toBe(31);

            component.filter = { page: 3, perPage: 30 };
            expect(component.itemsFrom()).toBe(61);
        });
    });

    describe("itemsTo()", () => {
        it("should return index of last displayed item based on filter and list parameters", () => {
            component.filter = { page: 1, perPage: 20 };
            component.list = { totalCount: 200, data: [] };
            expect(component.itemsTo()).toBe(20);

            component.filter = { page: 2, perPage: 20 };
            expect(component.itemsTo()).toBe(40);

            component.filter = { page: 2, perPage: 30 };
            expect(component.itemsTo()).toBe(60);

            component.filter = { page: 3, perPage: 30 };
            expect(component.itemsTo()).toBe(90);

            component.list = { totalCount: 83, data: [] };
            expect(component.itemsTo()).toBe(83);
        });
    });

    describe("pageCount()", () => {
        it("should return calculated page count based on filter and list parameters", () => {
            component.filter = { page: 1, perPage: 20 };
            component.list = { totalCount: 80, data: [] };
            expect(component.pageCount()).toBe(4);

            component.filter = { page: 2, perPage: 20 };
            component.list = { totalCount: 83, data: [] };
            expect(component.pageCount()).toBe(5);

            component.filter = { page: 1, perPage: 30 };
            expect(component.pageCount()).toBe(3);
        });
    });

    describe("pageNumbers()", () => {
        it("should return list of page numbers displayed in pager control", () => {
            component.filter = { page: 5, perPage: 10 };
            component.list = { totalCount: 100, data: [] };

            expect(component.pageNumbers()).toEqual([3, 4, 5, 6, 7]);
        });

        it("should trim list in the beginning if necessary", () => {
            component.list = { totalCount: 100, data: [] };

            component.filter = { page: 2, perPage: 10 };
            expect(component.pageNumbers()).toEqual([1, 2, 3, 4]);

            component.filter = { page: 1, perPage: 10 };
            expect(component.pageNumbers()).toEqual([1, 2, 3]);
        });

        it("should trim list at the end if necessary", () => {
            component.filter = { page: 5, perPage: 10 };

            component.list = { totalCount: 60, data: [] };
            expect(component.pageNumbers()).toEqual([3, 4, 5, 6]);

            component.list = { totalCount: 50, data: [] };
            expect(component.pageNumbers()).toEqual([3, 4, 5]);

        });
    });

    describe("showFrontHellip()", () => {
        it("should return true if there are any hidden page numbers prior to first page number in pager control", () => {
            component.list = { totalCount: 100, data: [] };

            component.filter = { page: 5, perPage: 10 };
            expect(component.showFrontHellip()).toBeTruthy();

            component.filter = { page: 3, perPage: 10 };
            expect(component.showFrontHellip()).toBeFalsy();

            component.filter = { page: 1, perPage: 10 };
            expect(component.showFrontHellip()).toBeFalsy();
        });
    });

    describe("showBackHellip()", () => {
        it("should return true if there are any hidden page numbers after last page number in pager control", () => {
            component.filter = { page: 5, perPage: 10 };

            component.list = { totalCount: 100, data: [] };
            expect(component.showBackHellip()).toBeTruthy();

            component.list = { totalCount: 70, data: [] };
            expect(component.showBackHellip()).toBeFalsy();

            component.list = { totalCount: 50, data: [] };
            expect(component.showBackHellip()).toBeFalsy();
        });
    });
});
