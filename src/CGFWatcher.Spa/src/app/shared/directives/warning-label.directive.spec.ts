﻿import { ElementRef } from "@angular/core";

import { WarningLabelDirective } from "./warning-label.directive";

describe("WarningLabelDirective", () => {
    describe("constructor()", () => {
        it("should update CSS classes of native element", () => {
            const nativeElement = {
                className: "foo bar"
            };

            const elementRef = new ElementRef(nativeElement);
            const directive = new WarningLabelDirective(elementRef);

            expect(nativeElement.className).toBe("warning label");
        });
    });
});
