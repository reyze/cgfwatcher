﻿import { DialogDirective } from "./dialog.directive";

describe("DialogDirective", () => {
    describe("closeDialogOnEscape()", () => {
        it("should emit onDialogClose event when user press Escape key", () => {
            let isEventTriggered: boolean;

            const directive = new DialogDirective();
            const event = { keyCode: 13 };

            directive.onDialogClose.subscribe(() => isEventTriggered = true);

            directive.closeDialogOnEscape(<KeyboardEvent>event);
            expect(isEventTriggered).toBeFalsy();

            event.keyCode = 27;

            directive.closeDialogOnEscape(<KeyboardEvent>event);
            expect(isEventTriggered).toBeTruthy();
        });
    });

    describe("closeDialogOnOverlayClick()", () => {
        it("should emit onDialogClose event when user click outside the dialog element", () => {
            let isEventTriggered: boolean;

            const directive = new DialogDirective();
            const event = { srcElement: { className: "reveal" } };

            directive.onDialogClose.subscribe(() => isEventTriggered = true);

            directive.closeDialogOnOverlayClick(<MouseEvent>event);
            expect(isEventTriggered).toBeFalsy();

            event.srcElement.className = "reveal-overlay";

            directive.closeDialogOnOverlayClick(<MouseEvent>event);
            expect(isEventTriggered).toBeTruthy();
        });
    });
});
