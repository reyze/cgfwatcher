﻿import { Directive, ElementRef } from "@angular/core";

@Directive({
    selector: "[cgfWarningLabel]"
})
export class WarningLabelDirective {
    constructor(el: ElementRef) {
        el.nativeElement.className = "warning label";
    }
}
