﻿import { Directive, ElementRef } from "@angular/core";

@Directive({
    selector: "[cgfSuccessLabel]"
})
export class SuccessLabelDirective {
    constructor(el: ElementRef) {
        el.nativeElement.className = "success label";
    }
}
