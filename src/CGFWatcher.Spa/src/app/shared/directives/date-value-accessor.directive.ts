/* tslint:disable:directive-selector */
/* tslint:disable:no-use-before-declare */

import { Directive, ElementRef, HostListener, Renderer, forwardRef } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";

export const DATEINPUT_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => DateValueAccessorDirective),
    multi: true
};

@Directive({
    selector: "input[type=date][formControlName], input[type=date][formControl], input[type=date][ngModel]",
    providers: [DATEINPUT_VALUE_ACCESSOR]
})
export class DateValueAccessorDirective implements ControlValueAccessor {
    @HostListener("change", ["$event.target.value"]) onChange = (_: any) => { };
    @HostListener("blur", []) onTouched = () => { };

    constructor(private _renderer: Renderer, private _elementRef: ElementRef) { }

    writeValue(value: any): void {
        if (!value) {
            this._renderer.setElementProperty(this._elementRef.nativeElement, "value", null);
            return;
        }

        if (value instanceof Date) {
            value = value.toISOString().split("T")[0];
        }

        this._renderer.setElementProperty(this._elementRef.nativeElement, "value", value);
        this.onChange(value);
    }

    registerOnChange(fn: (_: any) => {}): void { this.onChange = fn; }

    registerOnTouched(fn: () => {}): void { this.onTouched = fn; }

    setDisabledState(isDisabled: boolean): void {
        this._renderer.setElementProperty(this._elementRef.nativeElement, "disabled", isDisabled);
    }
}
