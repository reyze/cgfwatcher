﻿import { Directive, ElementRef } from "@angular/core";

@Directive({
    selector: "[cgfAlertLabel]"
})
export class AlertLabelDirective {
    constructor(el: ElementRef) {
        el.nativeElement.className = "alert label";
    }
}
