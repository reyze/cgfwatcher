﻿/* tslint:disable:no-output-rename */

import { Directive, HostListener, Output, EventEmitter } from "@angular/core";

@Directive({
    selector: "[cgfDialog]"
})
export class DialogDirective {
    @Output("cgfDialog") onDialogClose = new EventEmitter();

    @HostListener("document:keydown", ["$event"])
    closeDialogOnEscape(event: KeyboardEvent) {
        if (event.keyCode === 27) {
            this.onDialogClose.emit();
        }
    }

    @HostListener("document:click", ["$event"])
    closeDialogOnOverlayClick(event: MouseEvent) {
        if (event.srcElement.className === "reveal-overlay") {
            this.onDialogClose.emit();
        }
    }
}
