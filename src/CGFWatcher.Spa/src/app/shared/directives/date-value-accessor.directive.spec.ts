﻿import { Component, ElementRef } from "@angular/core";
import { TestBed, ComponentFixture, async, inject } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";

import { DateValueAccessorDirective } from "./date-value-accessor.directive";

@Component({
    template: `<form><input type="date" name="dateInput" [(ngModel)]="testDate"></form>`
}) class TestComponent {
    testDate: Date;
}

describe("DateValueAccessorDirective", () => {
    let fixture: ComponentFixture<TestComponent>;
    let component: TestComponent;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule],
            declarations: [TestComponent, DateValueAccessorDirective]
        }).compileComponents();

        fixture = TestBed.createComponent(TestComponent);
        component = fixture.componentInstance;
    }));

    describe("Model value defined as Date type", () => {
        beforeEach(async(() => {
            component.testDate = new Date(2017, 11, 25);
            fixture.detectChanges();
        }));

        it("should correctly set input value", () => {
            const input = fixture.debugElement.query(By.css("input"));

            expect(input.nativeElement.value).toBe("2017-12-24");
        });

        it("should update model value to string representation", () => {
            expect(component.testDate).toBe(<any>"2017-12-24");
        });
    });

    describe("Model value defined as string", () => {
        beforeEach(async(() => {
            component.testDate = <any>"2017-11-17";
            fixture.detectChanges();
        }));

        it("should correctly set input value", () => {
            const input = fixture.debugElement.query(By.css("input"));

            expect(input.nativeElement.value).toBe("2017-11-17");
        });
    });

    describe("Model value not specified", () => {
        beforeEach(async(() => {
            fixture.detectChanges();
        }));

        it("should correctly set input value", () => {
            const input = fixture.debugElement.query(By.css("input"));

            expect(input.nativeElement.value).toBe("");
        });
    });

    describe("Reacting to change event", () => {
        beforeEach(async(() => {
            component.testDate = new Date(2017, 11, 25);
            fixture.detectChanges();
        }));

        it("should populate new date in string representation", () => {
            const el = fixture.debugElement.query(By.css("input")).nativeElement;

            el.value = "2017-03-15";
            el.dispatchEvent(new Event("change"));

            expect(component.testDate).toBe(<any>"2017-03-15");
        });
    });
});
