﻿import { ElementRef } from "@angular/core";

import { SuccessLabelDirective } from "./success-label.directive";

describe("SuccessLabelDirective", () => {
    describe("constructor()", () => {
        it("should update CSS classes of native element", () => {
            const nativeElement = {
                className: "foo bar"
            };

            const elementRef = new ElementRef(nativeElement);
            const directive = new SuccessLabelDirective(elementRef);

            expect(nativeElement.className).toBe("success label");
        });
    });
});
