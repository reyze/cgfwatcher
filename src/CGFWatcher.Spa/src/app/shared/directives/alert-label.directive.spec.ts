﻿import { ElementRef } from "@angular/core";

import { AlertLabelDirective } from "./alert-label.directive";

describe("AlertLabelDirective", () => {
    describe("constructor()", () => {
        it("should update CSS classes of native element", () => {
            const nativeElement = {
                className: "foo bar"
            };

            const elementRef = new ElementRef(nativeElement);
            const directive = new AlertLabelDirective(elementRef);

            expect(nativeElement.className).toBe("alert label");
        });
    });
});
