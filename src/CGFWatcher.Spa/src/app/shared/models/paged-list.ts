﻿import { HttpResponse } from "@angular/common/http";

export class PagedList<T> {
    data: T[];
    totalCount: number;

    constructor(response: HttpResponse<T[]>) {
        this.data = response.body;
        this.totalCount = +response.headers.get("X-Total-Count");
    }
}
