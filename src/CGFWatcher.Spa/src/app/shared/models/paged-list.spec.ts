﻿import { HttpResponse, HttpHeaders } from "@angular/common/http";
import { PagedList } from "./paged-list";

describe("PagedList", () => {
    const response = new HttpResponse<number[]>({
        body: [1, 2, 7],
        headers: new HttpHeaders({
            "X-Total-Count": "123"
        })
    });

    const pagedList = new PagedList<number>(response);

    it("constructor should initialize 'data' property", () => expect(pagedList.data).toEqual([1, 2, 7]));

    it("constructor should initialize 'totalCount' property", () => expect(pagedList.totalCount).toBe(123));
});
