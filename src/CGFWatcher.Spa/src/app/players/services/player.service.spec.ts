﻿import { TestBed, inject } from "@angular/core/testing";
import { HttpClient } from "@angular/common/http";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";

import { PlayerService } from "./player.service";
import { PlayerFilter } from "./player.filter";
import { Player } from "../models";
import { PagedList, ConfigurationService } from "../../shared";

describe("PlayerService", () => {
    let configuration: ConfigurationService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule]
        });

        configuration = <ConfigurationService>{
            apiUrl(path: string): string {
                return "http://example.com".concat(path);
            }
        };
    });

    afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
        httpMock.verify();
    }));

    describe("getPlayer()", () => {
        it("should call api to get a player with given id", inject([
            HttpClient, HttpTestingController
        ], (
            http: HttpClient,
            httpMock: HttpTestingController
        ) => {
            const service = new PlayerService(http, configuration);

            service.getPlayer(8).subscribe((player: Player) => {
                expect(player).toEqual({ id: 30, fullName: "John", handicap: 5.7, memberships: [] });
            });

            const request = httpMock.expectOne("http://example.com/api/players/8");

            request.flush({
                id: 30, fullName: "John", handicap: 5.7, memberships: []
            });
        }));
    });

    describe("getPlayers()", () => {
        it("should call api to get a paged list of players", inject([
            HttpClient, HttpTestingController
        ], (
            http: HttpClient,
            httpMock: HttpTestingController
        ) => {
            const service = new PlayerService(http, configuration);

            const filter: PlayerFilter = {
                page: 3,
                perPage: 20
            };

            service.getPlayers(filter).subscribe((players: PagedList<Player>) => {
                expect(players.totalCount).toBe(1234);
                expect(players.data.length).toBe(2);
                expect(players.data[0]).toEqual({ id: 10, fullName: "Paul", handicap: 10.5, memberships: [] });
            });

            const request = httpMock.expectOne("http://example.com/api/players?page=3&perPage=20");

            request.flush([
                { id: 10, fullName: "Paul", handicap: 10.5, memberships: [] },
                { id: 20, fullName: "Peter", handicap: 28.7, memberships: [] }
            ], {
                headers: { "X-Total-Count": "1234" }
            });
        }));

        it("should translate optional filter parameters to query string", inject([
            HttpClient, HttpTestingController
        ], (
            http: HttpClient,
            httpMock: HttpTestingController
        ) => {
            const service = new PlayerService(http, configuration);

            const filter: PlayerFilter = {
                page: 3,
                perPage: 20,
                registrationNumberOrName: "465789",
                handicapFrom: 10,
                handicapTo: 30,
                membershipsCountFrom: 1,
                membershipsCountTo: 5,
                golfClubPrefix: "164",
                isHome: true,
                isBlocked: false,
                isDeleted: true
            };

            service.getPlayers(filter).subscribe((players: PagedList<Player>) => { });

            const request = httpMock.expectOne("http://example.com/api/players"
                + "?page=3&perPage=20&registrationNumberOrName=465789"
                + "&handicapFrom=10&handicapTo=30"
                + "&membershipsCountFrom=1&membershipsCountTo=5"
                + "&golfClubPrefix=164&isHome=true&isBlocked=false&isDeleted=true");

            request.flush([]);
        }));
    });
});
