import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";

import { HandicapChange, HandicapChangeFilter } from "../";
import { ConfigurationService } from "../../shared";

@Injectable({
    providedIn: "root"
})
export class HandicapChangeService {
    private urlPath = "/api/handicapchanges";

    constructor(private http: HttpClient, private configuration: ConfigurationService) { }

    getHandicapChanges(filter: HandicapChangeFilter): Observable<HandicapChange[]> {
        const url = this.configuration.apiUrl(this.urlPath);

        return this.http.get<HandicapChange[]>(url, {
            params: this.translateFilterToParams(filter)
        });
    }

    private translateFilterToParams(filter: HandicapChangeFilter): HttpParams {
        let params = new HttpParams();

        params = params.set("playerId", filter.playerId.toString());

        if (filter.year) {
            params = params.set("year", filter.year.toString());
        }

        return params;
    }
}
