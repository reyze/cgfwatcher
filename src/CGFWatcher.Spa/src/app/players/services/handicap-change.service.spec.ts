﻿import { TestBed, inject } from "@angular/core/testing";
import { HttpClient } from "@angular/common/http";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";

import { HandicapChangeService } from "./handicap-change.service";
import { HandicapChangeFilter } from "./handicap-change.filter";
import { HandicapChange } from "../models";
import { ConfigurationService } from "../../shared";

describe("HandicapChangeService", () => {
    let configuration: ConfigurationService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule]
        });

        configuration = <ConfigurationService>{
            apiUrl(path: string): string {
                return "http://example.com".concat(path);
            }
        };
    });

    afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
        httpMock.verify();
    }));

    describe("getHandicapChanges()", () => {
        it("should call api to get a list of handicap changes", inject([
            HttpClient, HttpTestingController
        ], (
            http: HttpClient,
            httpMock: HttpTestingController
        ) => {
            const service = new HandicapChangeService(http, configuration);

            const filter: HandicapChangeFilter = {
                playerId: 123
            };

            service.getHandicapChanges(filter).subscribe((changes: HandicapChange[]) => {
                expect(changes.length).toBe(2);
                expect(changes[0]).toEqual({ id: 10, playerId: 100, newHandicap: 15.6, changed: new Date(2016, 12, 24) });
            });

            const request = httpMock.expectOne("http://example.com/api/handicapchanges?playerId=123");

            request.flush([
                { id: 10, playerId: 100, newHandicap: 15.6, changed: new Date(2016, 12, 24) },
                { id: 20, playerId: 200, newHandicap: 25.8, changed: new Date(2016, 12, 22) }
            ]);
        }));

        it("should translate optional filter parameters to query string", inject([
            HttpClient, HttpTestingController
        ], (
            http: HttpClient,
            httpMock: HttpTestingController
        ) => {
            const service = new HandicapChangeService(http, configuration);

            const filter: HandicapChangeFilter = {
                playerId: 123,
                year: 2017
            };

            service.getHandicapChanges(filter).subscribe((changes: HandicapChange[]) => { });

            const request = httpMock.expectOne("http://example.com/api/handicapchanges?playerId=123&year=2017");

            request.flush([]);
        }));
    });
});
