﻿import { PaginationFilter } from "../../shared";

export class PlayerFilter extends PaginationFilter {
    registrationNumberOrName?: string;
    handicapFrom?: number;
    handicapTo?: number;
    membershipsCountFrom?: number;
    membershipsCountTo?: number;
    golfClubPrefix?: string;
    isHome?: boolean;
    isBlocked?: boolean;
    isDeleted?: boolean;
}
