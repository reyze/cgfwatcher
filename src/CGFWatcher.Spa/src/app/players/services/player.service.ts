import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { Player, PlayerFilter } from "../";
import { PagedList, ConfigurationService } from "../../shared";

@Injectable({
    providedIn: "root"
})
export class PlayerService {
    private urlPath = "/api/players";

    private get baseUrl(): string {
        return this.configuration.apiUrl(this.urlPath);
    }

    constructor(private http: HttpClient, private configuration: ConfigurationService) { }

    getPlayer(id: number): Observable<Player> {
        const url = `${this.baseUrl}/${id}`;

        return this.http.get<Player>(url);
    }

    getPlayers(filter: PlayerFilter): Observable<PagedList<Player>> {
        const url = this.baseUrl;

        return this.http
            .get<Player[]>(url, {
                observe: "response",
                params: this.translateFilterToParams(filter)
            })
            .pipe(
                map(res => new PagedList<Player>(res))
            );
    }

    private translateFilterToParams(filter: PlayerFilter): HttpParams {
        let params = new HttpParams();

        params = params.set("page", filter.page.toString());
        params = params.set("perPage", filter.perPage.toString());

        if (filter.registrationNumberOrName) {
            params = params.set("registrationNumberOrName", filter.registrationNumberOrName);
        }

        if (filter.handicapFrom) {
            params = params.set("handicapFrom", filter.handicapFrom.toString());
        }

        if (filter.handicapTo) {
            params = params.set("handicapTo", filter.handicapTo.toString());
        }

        if (filter.membershipsCountFrom) {
            params = params.set("membershipsCountFrom", filter.membershipsCountFrom.toString());
        }

        if (filter.membershipsCountTo) {
            params = params.set("membershipsCountTo", filter.membershipsCountTo.toString());
        }

        if (filter.golfClubPrefix) {
            params = params.set("golfClubPrefix", filter.golfClubPrefix);
        }

        if (filter.isHome !== undefined) {
            params = params.set("isHome", filter.isHome.toString());
        }

        if (filter.isBlocked !== undefined) {
            params = params.set("isBlocked", filter.isBlocked.toString());
        }

        if (filter.isDeleted !== undefined) {
            params = params.set("isDeleted", filter.isDeleted.toString());
        }

        return params;
    }
}
