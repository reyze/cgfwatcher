import { NgModule } from "@angular/core";

import { SharedModule } from "../shared";

import {
    PlayerComponent,
    PlayerFilterComponent,
    PlayerListingComponent,
    PlayerDetailComponent,
    PlayerHandicapChartComponent
} from "./components";

import { playersRouting } from "./players.routing";

@NgModule({
    declarations: [
        PlayerComponent,
        PlayerFilterComponent,
        PlayerListingComponent,
        PlayerDetailComponent,
        PlayerHandicapChartComponent
    ],
    imports: [SharedModule, playersRouting]
})
export class PlayersModule { }
