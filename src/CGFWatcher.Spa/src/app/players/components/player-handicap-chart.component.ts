﻿import { Component, OnInit, Input } from "@angular/core";
import { Language } from "angular-l10n";

import { HandicapChange, Player } from "../models";
import { HandicapChangeService, HandicapChangeFilter } from "../services";

@Component({
    selector: "cgf-player-handicap-chart",
    templateUrl: "./player-handicap-chart.component.html",
    styleUrls: ["./player-handicap-chart.component.css"]
})
export class PlayerHandicapChartComponent implements OnInit {
    @Language() lang: string;

    @Input() player: Player;

    handicapChanges: HandicapChange[];
    errorMessage: string;
    isLoadingInProgress: boolean;

    chartData: Array<any> = [{
        label: "HCP",
        fill: false,
        lineTension: 0,
        pointHitRadius: 20
    }];

    chartOptions: any = {
        scales: {
            xAxes: [{
                type: "time",
                time: { unit: "month" },
                ticks: { callback: (label) => this.formatTickX(label) }
            }],
            yAxes: [{
                ticks: { callback: (label) => this.formatTickY(label) }
            }]
        },
        tooltips: {
            callbacks: {
                title: (items, data) => this.createTooltipTitle(items, data),
                label: (item, data) => this.createTooltipLabel(item, data)
            }
        },
        legend: { display: false }
    };

    get reversedHandicapChanges(): HandicapChange[] {
        return this.handicapChanges.slice().reverse();
    }

    constructor(private handicapChangeService: HandicapChangeService) { }

    ngOnInit() {
        this.getHandicapChanges();
    }

    yearsForFilter() {
        const startingYear = 2016;

        const years = [];
        let currentYear = new Date().getFullYear();

        while (currentYear >= startingYear) {
            years.push(currentYear);
            currentYear--;
        }

        return years;
    }

    getHandicapChanges(year?: number) {
        this.errorMessage = null;
        this.isLoadingInProgress = true;

        const filter: HandicapChangeFilter = {
            playerId: this.player.id,
            year: year
        };

        this.handicapChangeService.getHandicapChanges(filter).subscribe(
            changes => this.handleChartData(changes),
            (error: Error) => this.errorMessage = error.message
        );
    }

    private handleChartData(handicapChanges: HandicapChange[]) {
        const chartDataset: any[] = handicapChanges.map(function (item) {
            return {
                x: new Date(item.changed),
                y: item.newHandicap
            };
        });

        this.chartData[0].data = chartDataset;

        this.normalizeChartXAxis(chartDataset);

        this.handicapChanges = handicapChanges;
        this.isLoadingInProgress = false;
    }

    private formatTickX(label: string) {
        return new Date(label).toLocaleDateString(this.lang);
    }

    private formatTickY(label: number) {
        return label.toLocaleString(this.lang, { minimumFractionDigits: 1 });
    }

    private createTooltipTitle(tooltipItems, data) {
        return tooltipItems[0].xLabel.toLocaleDateString(this.lang);
    }

    private createTooltipLabel(tooltipItem, data) {
        const label = data.datasets[tooltipItem.datasetIndex].label;
        const value = tooltipItem.yLabel.toLocaleString(this.lang, { minimumFractionDigits: 1 });

        return `${label}: ${value}`;
    }

    private normalizeChartXAxis(chartDataset: any[]) {
        if (!chartDataset.length) {
            return;
        }

        const scaleOptions = this.chartOptions.scales.xAxes[0].time;
        const minDate: Date = new Date(chartDataset[0].x);

        minDate.setDate(1);

        scaleOptions.min = minDate;
        scaleOptions.max = chartDataset[chartDataset.length - 1].x;
    }
}
