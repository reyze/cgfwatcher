﻿import { Component, OnInit, EventEmitter, Input, Output } from "@angular/core";
import { Language, LocaleValidation } from "angular-l10n";

import { PlayerFilter } from "../services";

import { GolfClubService, GolfClub, LanguageService } from "../../shared";

@Component({
    selector: "cgf-player-filter",
    templateUrl: "./player-filter.component.html"
})
export class PlayerFilterComponent implements OnInit {
    @Language() lang: string;

    @Input() filter: PlayerFilter;
    @Output() onFilterSubmit = new EventEmitter<PlayerFilter>();

    golfClubs: GolfClub[];

    constructor(
        private golfClubService: GolfClubService,
        private languageService: LanguageService,
        private localeValidation: LocaleValidation
    ) { }

    ngOnInit() {
        this.getGolfClubs();
    }

    localizeFilter(property: string) {
        return this.formatLocaleNumber(property);
    }

    submitFilter() {
        this.onFilterSubmit.emit(this.delocalizeFilter());
    }

    private getGolfClubs() {
        this.golfClubService.getGolfClubs().subscribe(
            clubs => this.golfClubs = clubs
        );
    }

    private delocalizeFilter(): PlayerFilter {
        return {
            ...this.filter,
            handicapFrom: this.parseLocaleNumber(this.filter.handicapFrom),
            handicapTo: this.parseLocaleNumber(this.filter.handicapTo)
        };
    }

    private parseLocaleNumber(property: number) {
        if (!property) {
            return property;
        }

        const parsedValue = this.localeValidation.parseNumber(property.toString());

        if (!parsedValue) {
            return property;
        }

        return parsedValue;
    }

    private formatLocaleNumber(property: string) {
        if (!property) {
            return "";
        }

        const parsedValue = this.localeValidation.parseNumber(property);

        // correct locale value, do nothing
        if (parsedValue) {
            return property;
        }

        const floatValue = parseFloat(property);

        // invariant number format (e.q. passed via URL), transform to locale number
        if (!isNaN(floatValue)) {
            return floatValue.toLocaleString(this.languageService.getLanguage(), {
                maximumFractionDigits: 1
            });
        }

        return "";
    }
}
