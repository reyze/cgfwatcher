import { Component, DebugElement } from "@angular/core";
import { TestBed, ComponentFixture, async } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { Router, ActivatedRoute } from "@angular/router";
import { from } from "rxjs";
import { TranslationModule, LocaleValidationModule, StorageStrategy, L10nConfig, L10nLoader } from "angular-l10n";

import { RouterStub, ActivatedRouteStub, RouterOutletStubComponent } from "../../../stubs";

import {
    PlayerComponent, PlayerFilterComponent, PlayerListingComponent, PlayerDetailComponent, PlayerHandicapChartComponent
} from "../components";

import { PlayerService, PlayerFilter, HandicapChangeService } from "../services";
import { Player } from "../models";

import { SharedModule, PagedList, GolfClubService } from "../../shared";

describe("PlayerComponent", () => {
    const l10nConfig: L10nConfig = {
        locale: {
            languages: [{ code: "en", dir: "ltr" }],
            language: "en",
            storage: StorageStrategy.Disabled
        },
        translation: {
            translationData: [{
                languageCode: "en",
                data: {}
            }]
        }
    };

    let fixture: ComponentFixture<PlayerComponent>;
    let component: PlayerComponent;
    let router: Router;
    let route: ActivatedRoute;
    let players: Player[];
    let list: PagedList<Player>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [SharedModule, TranslationModule.forRoot(l10nConfig), LocaleValidationModule.forRoot()],
            declarations: [
                PlayerComponent,
                PlayerFilterComponent,
                PlayerListingComponent,
                PlayerDetailComponent,
                PlayerHandicapChartComponent,
                RouterOutletStubComponent
            ],
            providers: [
                PlayerService, HandicapChangeService,
                { provide: Router, useClass: RouterStub },
                { provide: ActivatedRoute, useClass: ActivatedRouteStub }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(PlayerComponent);
        component = fixture.componentInstance;

        router = fixture.debugElement.injector.get(Router);
        route = fixture.debugElement.injector.get(ActivatedRoute);

        TestBed.get(L10nLoader).load();

        players = [
            { id: 1, fullName: "Donald Trump", handicap: 30.2, memberships: [] },
            { id: 2, fullName: "James Bond", handicap: 6.8, memberships: [] }
        ];

        list = { data: players, totalCount: 2 };
    }));

    function mockServices() {
        const golfClubService = fixture.debugElement.injector.get(GolfClubService);
        spyOn(golfClubService, "getGolfClubs").and.returnValue(from([]));

        const playerService = fixture.debugElement.injector.get(PlayerService);
        spyOn(playerService, "getPlayers").and.returnValue(from([list]));
    }

    describe("Template: filter submitting", () => {
        let form: DebugElement;

        beforeEach(async(() => {
            mockServices();

            form = fixture.debugElement.query(By.css("form"));

            fixture.detectChanges();
        }));

        it("should reload player listing", () => {
            form.triggerEventHandler("submit", null);
            fixture.detectChanges();

            const listing = fixture.debugElement.query(By.css("cgf-player-listing"));
            const rows = listing.queryAll(By.css("tbody tr"));

            expect(listing).not.toBeNull();
            expect(rows.length).toBe(2);
        });

        it("should navigate router using filter parameters", () => {
            const spy = spyOn(router, "navigate");

            form.triggerEventHandler("submit", null);
            fixture.detectChanges();

            expect(router.navigate).toHaveBeenCalled();

            const params = spy.calls.first().args[0];
            expect(params[0]).toBe("/players");
            expect(params[1]).toEqual({});
        });
    });

    describe("Template: player selecting", () => {
        beforeEach(async(() => {
            mockServices();
            fixture.detectChanges();

            component.submittedFilter = new PlayerFilter();
            fixture.detectChanges();
        }));

        it("should navigate router to player detail", () => {
            const spy = spyOn(router, "navigate");
            const link = fixture.debugElement.queryAll(By.css("cgf-player-listing a"))[1];

            link.triggerEventHandler("click", null);
            fixture.detectChanges();

            expect(router.navigate).toHaveBeenCalled();
            expect(spy.calls.first().args[0]).toEqual([2]);
        });
    });

    describe("ngOnInit()", () => {
        it("should subscribe to route params and initialize filters", () => {
            component.ngOnInit();

            (route as any).testParams = {
                registrationNumberOrName: "Paul",
                handicapFrom: 10,
                page: 4
            };

            const expectedFilter = new PlayerFilter();
            expectedFilter.registrationNumberOrName = "Paul";
            expectedFilter.handicapFrom = 10;
            expectedFilter.page = 4;

            expect(component.filter).toEqual(expectedFilter);
            expect(component.submittedFilter).toEqual(expectedFilter);

            (route as any).testParams = {};

            expect(component.filter).toEqual(new PlayerFilter());
            expect(component.submittedFilter).toBeNull();
        });


        it("should set page to 1 if not specified", () => {
            component.ngOnInit();

            (route as any).testParams = {
                handicapFrom: 10
            };

            const expectedFilter = new PlayerFilter();
            expectedFilter.handicapFrom = 10;
            expectedFilter.page = 1;

            expect(component.filter).toEqual(expectedFilter);
            expect(component.submittedFilter).toEqual(expectedFilter);
        });

        it("should cast page to number data type", () => {
            component.ngOnInit();

            (route as any).testParams = {
                handicapFrom: 10,
                page: "4"
            };

            const expectedFilter = new PlayerFilter();
            expectedFilter.handicapFrom = 10;
            expectedFilter.page = 4;

            expect(component.filter).toEqual(expectedFilter);
            expect(component.submittedFilter).toEqual(expectedFilter);
        });
    });

    describe("submitFilter()", () => {
        it("should set submittedFilter property", () => {
            const filter = <PlayerFilter>{
                registrationNumberOrName: "Paul",
                handicapFrom: 10
            };

            component.submitFilter(filter);

            expect(component.submittedFilter).toBe(filter);
        });

        it("should navigate router using filter parameters", () => {
            const spy = spyOn(router, "navigate");

            const filter = <PlayerFilter>{
                registrationNumberOrName: "Paul",
                handicapFrom: 10,
                handicapTo: 20,
                membershipsCountFrom: 1,
                membershipsCountTo: 5,
                golfClubPrefix: "XYZ",
                isHome: true,
                isBlocked: false,
                isDeleted: true
            };

            component.submitFilter(filter);

            expect(router.navigate).toHaveBeenCalled();

            const params = spy.calls.first().args[0];

            expect(params[0]).toBe("/players");
            expect(params[1]).toEqual({
                registrationNumberOrName: "Paul",
                handicapFrom: 10,
                handicapTo: 20,
                membershipsCountFrom: 1,
                membershipsCountTo: 5,
                golfClubPrefix: "XYZ",
                isHome: true,
                isBlocked: false,
                isDeleted: true
            });
        });

        it("should exclude empty filter parameters from routing", () => {
            const spy = spyOn(router, "navigate");

            const filter = <PlayerFilter>{
                handicapFrom: 10,
                membershipsCountTo: 5,
                golfClubPrefix: "XYZ",
                isHome: false
            };

            component.submitFilter(filter);

            const params = spy.calls.first().args[0];

            expect(params[1]).toEqual({
                handicapFrom: 10,
                membershipsCountTo: 5,
                golfClubPrefix: "XYZ",
                isHome: false
            });
        });

        it("should set page to 1 and exclude 'page' parameter from routing", () => {
            const spy = spyOn(router, "navigate");

            const filter = <PlayerFilter>{
                page: 4
            };

            component.submitFilter(filter);

            const params = spy.calls.first().args[0];

            expect(params[1]).toEqual({});
            expect(filter.page).toBe(1);
        });

        it("should exclude 'perPage' parameter from routing", () => {
            const spy = spyOn(router, "navigate");

            const filter = <PlayerFilter>{
                perPage: 20
            };

            component.submitFilter(filter);

            const params = spy.calls.first().args[0];

            expect(params[1]).toEqual({});
        });
    });

    describe("changePage()", () => {
        it("should update page on submittedFilter property", () => {
            component.submittedFilter = <PlayerFilter>{
                registrationNumberOrName: "Paul",
                handicapFrom: 10,
                page: 3
            };

            component.changePage(5);

            expect(component.submittedFilter.page).toBe(5);
        });

        it("should navigate router using filter parameters", () => {
            const spy = spyOn(router, "navigate");

            component.submittedFilter = <PlayerFilter>{
                registrationNumberOrName: "Paul",
                handicapFrom: 10,
                handicapTo: 20,
                membershipsCountFrom: 1,
                membershipsCountTo: 5,
                golfClubPrefix: "XYZ",
                isHome: true,
                isBlocked: false,
                isDeleted: true,
                page: 3
            };

            component.changePage(5);

            expect(router.navigate).toHaveBeenCalled();

            const params = spy.calls.first().args[0];

            expect(params[0]).toBe("/players");
            expect(params[1]).toEqual({
                registrationNumberOrName: "Paul",
                handicapFrom: 10,
                handicapTo: 20,
                membershipsCountFrom: 1,
                membershipsCountTo: 5,
                golfClubPrefix: "XYZ",
                isHome: true,
                isBlocked: false,
                isDeleted: true,
                page: 5
            });
        });

        it("should exclude empty filter parameters from routing", () => {
            const spy = spyOn(router, "navigate");

            component.submittedFilter = <PlayerFilter>{
                handicapFrom: 10,
                membershipsCountTo: 5,
                golfClubPrefix: "XYZ",
                isHome: false,
                page: 3
            };

            component.changePage(5);

            const params = spy.calls.first().args[0];

            expect(params[1]).toEqual({
                handicapFrom: 10,
                membershipsCountTo: 5,
                golfClubPrefix: "XYZ",
                isHome: false,
                page: 5
            });
        });

        it("should exclude 'perPage' and default 'page' parameters from routing", () => {
            const spy = spyOn(router, "navigate");

            component.submittedFilter = {
                page: 4,
                perPage: 20
            };

            component.changePage(1);

            const params = spy.calls.first().args[0];

            expect(params[1]).toEqual({});
        });
    });

    describe("selectPlayer()", () => {
        it("should navigate router to player detail", () => {
            const player: Player = {
                id: 100,
                fullName: "Donald Trump",
                handicap: 30.2,
                memberships: []
            };

            const spy = spyOn(router, "navigate");

            component.selectPlayer(player);

            expect(router.navigate).toHaveBeenCalled();

            const args = spy.calls.first().args;
            expect(args[0]).toEqual([100]);
            expect(args[1].relativeTo).toBe(route);
        });
    });
});
