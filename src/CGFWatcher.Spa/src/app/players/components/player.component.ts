﻿import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Language } from "angular-l10n";

import { PlayerFilter } from "../services";
import { Player } from "../models";
import { isEmptyObject } from "../../shared";

@Component({
    selector: "cgf-player",
    templateUrl: "./player.component.html"
})
export class PlayerComponent implements OnInit {
    @Language() lang: string;

    filter: PlayerFilter;
    submittedFilter: PlayerFilter;

    constructor(private router: Router, private route: ActivatedRoute) { }

    ngOnInit() {
        this.route.params.subscribe(
            (filter: PlayerFilter) => this.handleRouteParams(filter)
        );
    }

    submitFilter(filter: PlayerFilter) {
        filter.page = 1;

        this.submittedFilter = filter;
        this.router.navigate(["/players", this.getRouteParams(filter)]);
    }

    changePage(pageNumber: number) {
        this.submittedFilter.page = pageNumber;

        this.router.navigate(["/players", this.getRouteParams(this.submittedFilter)]);
    }

    selectPlayer(player: Player) {
        this.router.navigate([player.id], { relativeTo: this.route });
    }

    private handleRouteParams(filter: PlayerFilter) {
        filter = Object.assign(new PlayerFilter(), filter);

        if (isEmptyObject(filter)) {
            this.filter = new PlayerFilter();
            this.submittedFilter = null;
        } else {
            filter.page = +(filter.page || 1);

            this.filter = filter;
            this.submittedFilter = filter;
        }
    }

    private getRouteParams(filter: PlayerFilter) {
        const params = {};

        for (const param in filter) {
            if (param === "constructor") { continue; }
            const paramValue = filter[param];
            if (!paramValue && paramValue !== false) { continue; } // exclude empty parameters (but include booleans)
            if (param === "perPage") { continue; } // exclude perPage paramater
            if (param === "page" && paramValue === 1) { continue; } // exclude default page parameter
            params[param] = paramValue;
        }

        return params;
    }
}
