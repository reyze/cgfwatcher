﻿import { Component, OnChanges, EventEmitter, Input, Output } from "@angular/core";
import { Language } from "angular-l10n";

import { Player } from "../models";
import { PlayerService, PlayerFilter } from "../services";
import { PagedList } from "../../shared";

@Component({
    selector: "cgf-player-listing",
    templateUrl: "./player-listing.component.html",
    styleUrls: ["./player-listing.component.css"]
})
export class PlayerListingComponent implements OnChanges {
    @Language() lang: string;

    @Input() filter: PlayerFilter;
    @Output() onPageChanged = new EventEmitter<number>();
    @Output() onPlayerSelected = new EventEmitter<Player>();

    players: PagedList<Player>;
    errorMessage: string;
    isLoadingInProgress: boolean;

    constructor(private playerService: PlayerService) { }

    ngOnChanges(changeRecord) {
        if (this.filter) {
            this.getPlayers(this.filter);
        }
    }

    changePage(pageNumber: number) {
        this.onPageChanged.emit(pageNumber);
    }

    selectPlayer(player: Player) {
        this.onPlayerSelected.emit(player);
    }

    private getPlayers(filter: PlayerFilter) {
        this.errorMessage = null;
        this.isLoadingInProgress = true;

        filter.perPage = 20;

        this.playerService.getPlayers(filter).subscribe(
            players => { this.players = players; this.isLoadingInProgress = false; },
            (error: Error) => this.errorMessage = error.message
        );
    }
}
