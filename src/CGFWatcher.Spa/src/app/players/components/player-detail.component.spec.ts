import { DebugElement } from "@angular/core";
import { TestBed, ComponentFixture, async } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { Router, ActivatedRoute } from "@angular/router";
import { from } from "rxjs";
import { TranslationModule, StorageStrategy, L10nConfig, L10nLoader } from "angular-l10n";

import { RouterStub, ActivatedRouteStub } from "../../../stubs";

import { PlayerDetailComponent, PlayerHandicapChartComponent } from "../components";
import { PlayerService, HandicapChangeService } from "../services";

import { SharedModule } from "../../shared";

describe("PlayerDetailComponent", () => {
    const l10nConfig: L10nConfig = {
        locale: {
            languages: [{ code: "en", dir: "ltr" }],
            language: "en",
            storage: StorageStrategy.Disabled
        },
        translation: {
            translationData: [{
                languageCode: "en",
                data: {
                    "Player.HomeClub": "Home club",
                    "Player.Blocked": "Blocked club",
                    "Player.Deleted": "Deleted club",
                    "Player.Detail.HandicapAbbr": "HCP"
                }
            }]
        }
    };

    let fixture: ComponentFixture<PlayerDetailComponent>;
    let component: PlayerDetailComponent;
    let service: PlayerService;
    let router: Router;
    let route: ActivatedRoute;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [SharedModule, TranslationModule.forRoot(l10nConfig)],
            declarations: [PlayerDetailComponent, PlayerHandicapChartComponent],
            providers: [
                PlayerService, HandicapChangeService,
                { provide: Router, useClass: RouterStub },
                { provide: ActivatedRoute, useClass: ActivatedRouteStub }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(PlayerDetailComponent);
        component = fixture.componentInstance;

        service = fixture.debugElement.injector.get(PlayerService);
        router = fixture.debugElement.injector.get(Router);
        route = fixture.debugElement.injector.get(ActivatedRoute);

        (route as any).testParams = { id: 3 };

        TestBed.get(L10nLoader).load();
    }));

    describe("Template: rendering", () => {
        beforeEach(async(() => {
            component.player = {
                id: 100,
                fullName: "Donald Trump",
                handicap: 32,
                memberships: []
            };
        }));

        it("should render header with player name and handicap", () => {
            fixture.detectChanges();

            const h2 = fixture.debugElement.query(By.css("h2")).nativeElement;
            const lead = fixture.debugElement.query(By.css("h2 + .lead")).nativeElement;

            expect(h2.textContent).toBe("Donald Trump");
            expect(lead.textContent).toContain("HCP");
            expect(lead.textContent).toContain("32.0");
        });

        it("should render memberships list", () => {
            component.player.memberships = [
                { registrationNumber: "111222", golfClub: "Black Bridge", isHome: false, isBlocked: false, isDeleted: false },
                { registrationNumber: "888999", golfClub: "Augusta National", isHome: false, isBlocked: false, isDeleted: false }
            ];

            fixture.detectChanges();

            const rows = fixture.debugElement.queryAll(By.css("tbody > tr"));

            checkRow(rows[0], ["111222", "Black Bridge", "", "", ""]);
            checkRow(rows[1], ["888999", "Augusta National", "", "", ""]);

            function checkRow(row: DebugElement, cellsExpected: string[]) {
                const cellsActual = row.children.map(cell => cell.nativeElement.textContent);

                expect(cellsActual).toEqual(cellsExpected);
            }
        });

        it("should render label for home memberships", () => {
            component.player.memberships = [
                { registrationNumber: "111222", golfClub: "Black Bridge", isHome: true, isBlocked: false, isDeleted: false }
            ];

            fixture.detectChanges();

            const row = fixture.debugElement.query(By.css("tbody > tr"));
            const label = row.children[2].query(By.css("span.label")).nativeElement;

            expect(label.className).toContain("success");
            expect(label.textContent).toBe("Home club");
        });

        it("should render label for blocked memberships", () => {
            component.player.memberships = [
                { registrationNumber: "111222", golfClub: "Black Bridge", isHome: false, isBlocked: true, isDeleted: false }
            ];

            fixture.detectChanges();

            const row = fixture.debugElement.query(By.css("tbody > tr"));
            const label = row.children[3].query(By.css("span.label")).nativeElement;

            expect(label.className).toContain("warning");
            expect(label.textContent).toBe("Blocked club");
        });

        it("should render label for deleted memberships", () => {
            component.player.memberships = [
                { registrationNumber: "111222", golfClub: "Black Bridge", isHome: false, isBlocked: false, isDeleted: true }
            ];

            fixture.detectChanges();

            const row = fixture.debugElement.query(By.css("tbody > tr"));
            const label = row.children[4].query(By.css("span.label")).nativeElement;

            expect(label.className).toContain("alert");
            expect(label.textContent).toBe("Deleted club");
        });
    });

    describe("Template: detail closing", () => {
        beforeEach(async(() => {
            component.player = {
                id: 100,
                fullName: "Donald Trump",
                handicap: 30.2,
                memberships: []
            };
        }));

        it("should navigate router back when close button clicked", () => {
            fixture.detectChanges();

            const el = fixture.debugElement.query(By.css("button"));
            const spy = spyOn(router, "navigate");

            el.triggerEventHandler("click", null);

            expect(router.navigate).toHaveBeenCalled();

            const args = spy.calls.first().args;
            expect(args[0]).toEqual(["../"]);
            expect(args[1].relativeTo).toBe(route);
        });
    });

    describe("ngOnInit()", () => {
        it("should subscribe to route param and load player data", () => {
            const player1 = { id: 3, fullName: "Donald Trump" };
            const player2 = { id: 42, fullName: "Tiger Woods" };

            spyOn(service, "getPlayer").and.returnValues(
                from([player1]),
                from([player2])
            );

            component.ngOnInit();

            expect(service.getPlayer).toHaveBeenCalledWith(3);
            expect(component.player).toEqual(<any>player1);

            (route as any).testParams = { id: 42 };

            expect(service.getPlayer).toHaveBeenCalledWith(42);
            expect(component.player).toEqual(<any>player2);
        });
    });

    describe("closeDetail()", () => {
        it("should navigate router back", () => {
            const spy = spyOn(router, "navigate");

            component.closeDetail();

            expect(router.navigate).toHaveBeenCalled();

            const args = spy.calls.first().args;
            expect(args[0]).toEqual(["../"]);
            expect(args[1].relativeTo).toBe(route);
        });
    });
});
