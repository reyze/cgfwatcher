import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { Language } from "angular-l10n";
import { switchMap } from "rxjs/operators";

import { PlayerService } from "../services";
import { Player } from "../models";

@Component({
    selector: "cgf-player-detail",
    templateUrl: "./player-detail.component.html",
    styleUrls: ["./player-detail.component.css"]
})
export class PlayerDetailComponent implements OnInit {
    @Language() lang: string;

    player: Player;

    constructor(
        private playerService: PlayerService,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.route.params
            .pipe(
                switchMap((params: Params) => this.playerService.getPlayer(+params["id"]))
            )
            .subscribe((player: Player) => this.player = player);
    }

    closeDetail() {
        this.router.navigate(["../"], { relativeTo: this.route });
    }
}
