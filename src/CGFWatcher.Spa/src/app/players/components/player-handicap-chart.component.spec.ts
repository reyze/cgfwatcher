import { TestBed, ComponentFixture, async } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { from, throwError } from "rxjs";
import { TranslationModule } from "angular-l10n";

import { PlayerHandicapChartComponent } from "../components";
import { HandicapChangeService, HandicapChangeFilter } from "../services";

import { SharedModule } from "../../shared";

describe("PlayerHandicapChartComponent", () => {
    let fixture: ComponentFixture<PlayerHandicapChartComponent>;
    let component: PlayerHandicapChartComponent;
    let service: HandicapChangeService;
    let data: any[];

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [SharedModule, TranslationModule.forRoot({})],
            declarations: [PlayerHandicapChartComponent],
            providers: [HandicapChangeService]
        }).compileComponents();

        fixture = TestBed.createComponent(PlayerHandicapChartComponent);
        component = fixture.componentInstance;

        service = fixture.debugElement.injector.get(HandicapChangeService);

        data = [
            { id: 1, playerId: 10, newHandicap: 10, changed: "2016-01-10" },
            { id: 2, playerId: 10, newHandicap: 9.8, changed: "2016-02-01" },
            { id: 3, playerId: 10, newHandicap: 10.5, changed: "2016-03-01" }
        ];

        component.player = {
            id: 10,
            fullName: "Donald Trump",
            handicap: 30.2,
            memberships: []
        };
    }));

    describe("Template: initial state", () => {
        it("should render just form", () => {
            fixture.detectChanges();

            fixture.whenStable().then(() => {
                component.isLoadingInProgress = false;
                fixture.detectChanges();

                const form = fixture.debugElement.query(By.css("form"));
                const loader = fixture.debugElement.query(By.css("form + p"));
                const error = fixture.debugElement.query(By.css("p.alert"));
                const content = fixture.debugElement.query(By.css("form + div"));

                expect(form).not.toBeNull();
                expect(loader).toBeNull();
                expect(error).toBeNull();
                expect(content).toBeNull();
            });
        });
    });

    describe("Template: loading in progress", () => {
        it("should render loading message", () => {
            fixture.detectChanges();

            fixture.whenStable().then(() => {
                component.isLoadingInProgress = true;
                fixture.detectChanges();

                const form = fixture.debugElement.query(By.css("form"));
                const loader = fixture.debugElement.query(By.css("form + p"));
                const error = fixture.debugElement.query(By.css("p.alert"));
                const content = fixture.debugElement.query(By.css("form + div"));

                expect(form).not.toBeNull();
                expect(loader).not.toBeNull();
                expect(error).toBeNull();
                expect(content).toBeNull();
            });
        });
    });

    describe("Template: error occurred in data request", () => {
        it("should render loading message and error message", () => {
            fixture.detectChanges();

            fixture.whenStable().then(() => {
                component.isLoadingInProgress = true;
                component.errorMessage = "Request error";
                fixture.detectChanges();

                const form = fixture.debugElement.query(By.css("form"));
                const loader = fixture.debugElement.query(By.css("form + p"));
                const error = fixture.debugElement.query(By.css("p.alert"));
                const content = fixture.debugElement.query(By.css("form + div"));

                expect(form).not.toBeNull();
                expect(loader).not.toBeNull();
                expect(error).not.toBeNull();
                expect(content).toBeNull();
            });
        });
    });

    describe("Template: handicap changes data loaded", () => {
        // Unable to test template content rendering due to missing Chart.js source code
        // Error message: "ng2-charts configuration issue: Embedding Chart.js lib is mandatory"
    });

    describe("Template: year selector", () => {
        beforeEach(async(() => {
            jasmine.clock().mockDate(new Date(2018, 1, 1));

            fixture.detectChanges();
        }));

        it("should be displayed as a select element", () => {
            const el = fixture.debugElement.query(By.css("#year"));

            expect(el.children.length).toBe(4);

            expect(el.children[1].nativeElement.value).toBe("2018");
            expect(el.children[1].nativeElement.textContent).toBe("2018");
        });

        it("should refresh data when user changes year selection", () => {
            spyOn(component, "getHandicapChanges");

            const el = fixture.debugElement.query(By.css("#year")).nativeElement;

            el.value = "2017";
            el.dispatchEvent(new Event("change"));

            expect(component.getHandicapChanges).toHaveBeenCalledWith("2017");
        });
    });

    describe("get reversedHandicapChanges()", () => {
        it("should return reversed handicap changes array without modifying base data", () => {
            component.handicapChanges = data;

            expect(component.reversedHandicapChanges).toEqual([
                { id: 3, playerId: 10, newHandicap: 10.5, changed: <any>"2016-03-01" },
                { id: 2, playerId: 10, newHandicap: 9.8, changed: <any>"2016-02-01" },
                { id: 1, playerId: 10, newHandicap: 10, changed: <any>"2016-01-10" }
            ]);

            expect(component.handicapChanges).toEqual(data);
        });
    });

    describe("ngOnInit()", () => {
        it("should call getHandicapChanges() with no parameters", () => {
            spyOn(component, "getHandicapChanges");

            component.ngOnInit();

            expect(component.getHandicapChanges).toHaveBeenCalledWith();
        });
    });

    describe("yearsForFilter()", () => {
        it("should return array of years from this year till 2016", () => {
            jasmine.clock().mockDate(new Date(2019, 1, 1));

            const result = component.yearsForFilter();

            expect(result).toEqual([2019, 2018, 2017, 2016]);
        });
    });

    describe("getHandicapChanges()", () => {
        it("should load handicap changes for given player and year", () => {
            spyOn(service, "getHandicapChanges").and.returnValue(from([data]));

            component.getHandicapChanges();

            expect(service.getHandicapChanges).toHaveBeenCalledWith({
                playerId: 10,
                year: undefined
            });

            component.getHandicapChanges(2016);

            expect(service.getHandicapChanges).toHaveBeenCalledWith({
                playerId: 10,
                year: 2016
            });

            expect(component.handicapChanges.length).toBe(3);
            expect(component.handicapChanges).toBe(data);
        });

        it("should prepare chart dataset data", () => {
            spyOn(service, "getHandicapChanges").and.returnValue(from([data]));

            component.getHandicapChanges();

            expect(component.chartData[0].data).toEqual([
                { x: new Date(Date.UTC(2016, 0, 10)), y: 10 },
                { x: new Date(Date.UTC(2016, 1, 1)), y: 9.8 },
                { x: new Date(Date.UTC(2016, 2, 1)), y: 10.5 }
            ]);
        });

        it("should set min and max for x-axis based on handicap changes data", () => {
            spyOn(service, "getHandicapChanges").and.returnValue(from([data]));

            component.getHandicapChanges();

            const scaleOptions = component.chartOptions.scales.xAxes[0].time;

            expect(scaleOptions.min).toEqual(new Date(Date.UTC(2016, 0, 1)));
            expect(scaleOptions.max).toEqual(new Date(Date.UTC(2016, 2, 1)));
        });

        it("should handle data retrieval error", () => {
            spyOn(service, "getHandicapChanges").and.returnValue(throwError(new Error("Connection error")));

            component.getHandicapChanges();

            expect(component.isLoadingInProgress).toBeTruthy();
            expect(component.errorMessage).toBe("Connection error");
        });

        it("should reset errorMessage before loading data", () => {
            spyOn(service, "getHandicapChanges").and.returnValue(from([data]));

            component.errorMessage = "Foo error";

            component.getHandicapChanges();

            expect(component.errorMessage).toBeNull();
        });

        it("should set isLoadingInProgress while loading data", () => {
            spyOn(service, "getHandicapChanges").and.callFake(function (f: HandicapChangeFilter) {
                expect(component.isLoadingInProgress).toBeTruthy();

                return from([data]);
            });

            expect(component.isLoadingInProgress).toBeFalsy();

            component.getHandicapChanges();

            expect(component.isLoadingInProgress).toBeFalsy();
        });
    });

    describe("chartOptions", () => {
        it("should format x-axis ticks as locale date", () => {
            const formatter = component.chartOptions.scales.xAxes[0].ticks.callback;

            component.lang = "cs";
            expect(formatter("2016-01-10")).toEqual("10. 1. 2016");

            component.lang = "en";
            expect(formatter("2016-01-10")).toEqual("1/10/2016");
        });

        it("should format y-axis ticks as locale decimal number", () => {
            const formatter = component.chartOptions.scales.yAxes[0].ticks.callback;

            component.lang = "cs";
            expect(formatter(11.5)).toEqual("11,5");
            expect(formatter(9)).toEqual("9,0");

            component.lang = "en";
            expect(formatter(11.5)).toEqual("11.5");
            expect(formatter(9)).toEqual("9.0");
        });

        it("should format tooltip titles as locale date", () => {
            const formatter = component.chartOptions.tooltips.callbacks.title;

            const tooltipItems = [
                { xLabel: new Date("2016-01-10") }
            ];

            component.lang = "cs";
            expect(formatter(tooltipItems, null)).toEqual("10. 1. 2016");

            component.lang = "en";
            expect(formatter(tooltipItems, null)).toEqual("1/10/2016");
        });

        it("should format tooltip labels as locale decimal number decorated with series name", () => {
            const formatter = component.chartOptions.tooltips.callbacks.label;

            const tooltipItem = {
                yLabel: 0,
                datasetIndex: 1
            };

            const datasets = {
                datasets: [
                    { label: "Foo" },
                    { label: "HCP" },
                    { label: "Bar" }
                ]
            };

            component.lang = "cs";
            tooltipItem.yLabel = 11.5;
            expect(formatter(tooltipItem, datasets)).toEqual("HCP: 11,5");
            tooltipItem.yLabel = 9;
            expect(formatter(tooltipItem, datasets)).toEqual("HCP: 9,0");

            component.lang = "en";
            tooltipItem.yLabel = 11.5;
            expect(formatter(tooltipItem, datasets)).toEqual("HCP: 11.5");
            tooltipItem.yLabel = 9;
            expect(formatter(tooltipItem, datasets)).toEqual("HCP: 9.0");
        });
    });
});
