import { DebugElement } from "@angular/core";
import { TestBed, ComponentFixture, async } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { from, throwError } from "rxjs";
import { TranslationModule, StorageStrategy, L10nConfig, L10nLoader } from "angular-l10n";

import { PlayerListingComponent } from "./player-listing.component";
import { PlayerService, PlayerFilter } from "../services";
import { Player } from "../models";

import { SharedModule, PagedList } from "../../shared";

describe("PlayerListingComponent", () => {
    const l10nConfig: L10nConfig = {
        locale: {
            languages: [{ code: "en", dir: "ltr" }],
            language: "en",
            storage: StorageStrategy.Disabled
        },
        translation: {
            translationData: [{
                languageCode: "en",
                data: {
                    "Player.HomeClub": "Home club",
                    "Player.Blocked": "Blocked club",
                    "Player.Deleted": "Deleted club"
                }
            }]
        }
    };

    let fixture: ComponentFixture<PlayerListingComponent>;
    let component: PlayerListingComponent;
    let service: PlayerService;
    let players: Player[];
    let list: PagedList<Player>;
    let filter: PlayerFilter;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [SharedModule, TranslationModule.forRoot(l10nConfig)],
            declarations: [PlayerListingComponent],
            providers: [PlayerService]
        }).compileComponents();

        fixture = TestBed.createComponent(PlayerListingComponent);
        component = fixture.componentInstance;

        service = fixture.debugElement.injector.get(PlayerService);

        TestBed.get(L10nLoader).load();

        players = [
            { id: 1, fullName: "Donald Trump", handicap: 32, memberships: [] },
            { id: 2, fullName: "James Bond", handicap: 6.8, memberships: [] }
        ];

        list = { data: players, totalCount: 2 };

        filter = {
            registrationNumberOrName: "Paul",
            handicapFrom: 10,
            handicapTo: 20,
            membershipsCountFrom: 1,
            membershipsCountTo: 5,
            golfClubPrefix: "XYZ",
            isHome: true,
            isBlocked: false,
            isDeleted: true,
            page: 4,
            perPage: 50
        };
    }));

    describe("Template: initial state", () => {
        it("should render nothing", () => {
            fixture.detectChanges();

            const hr = fixture.debugElement.query(By.css("hr"));
            const loader = fixture.debugElement.query(By.css("hr + p"));
            const error = fixture.debugElement.query(By.css("p.alert"));
            const table = fixture.debugElement.query(By.css("table"));
            const pager = fixture.debugElement.query(By.css("cgf-pager"));

            expect(hr).toBeNull();
            expect(loader).toBeNull();
            expect(error).toBeNull();
            expect(table).toBeNull();
            expect(pager).toBeNull();
        });
    });

    describe("Template: loading in progress", () => {
        it("should render horizontal separator and loading message", () => {
            component.filter = filter;
            component.isLoadingInProgress = true;

            fixture.detectChanges();

            const hr = fixture.debugElement.query(By.css("hr"));
            const loader = fixture.debugElement.query(By.css("hr + p"));
            const error = fixture.debugElement.query(By.css("p.alert"));
            const table = fixture.debugElement.query(By.css("table"));
            const pager = fixture.debugElement.query(By.css("cgf-pager"));

            expect(hr).not.toBeNull();
            expect(loader).not.toBeNull();
            expect(error).toBeNull();
            expect(table).toBeNull();
            expect(pager).toBeNull();
        });
    });

    describe("Template: error occurred in data request", () => {
        it("should render horizontal separator, loading message and error message", () => {
            component.filter = filter;
            component.isLoadingInProgress = true;
            component.errorMessage = "Request error";

            fixture.detectChanges();

            const hr = fixture.debugElement.query(By.css("hr"));
            const loader = fixture.debugElement.query(By.css("hr + p"));
            const error = fixture.debugElement.query(By.css("p.alert"));
            const table = fixture.debugElement.query(By.css("table"));
            const pager = fixture.debugElement.query(By.css("cgf-pager"));

            expect(hr).not.toBeNull();
            expect(loader).not.toBeNull();
            expect(error).not.toBeNull();
            expect(table).toBeNull();
            expect(pager).toBeNull();
        });
    });

    describe("Template: players data loaded", () => {
        it("should render horizontal separator, table and pager component", () => {
            component.filter = filter;
            component.players = list;
            component.isLoadingInProgress = false;

            fixture.detectChanges();

            const hr = fixture.debugElement.query(By.css("hr"));
            const loader = fixture.debugElement.query(By.css("hr + p"));
            const error = fixture.debugElement.query(By.css("p.alert"));
            const table = fixture.debugElement.query(By.css("table"));
            const pager = fixture.debugElement.query(By.css("cgf-pager"));

            expect(hr).not.toBeNull();
            expect(loader).toBeNull();
            expect(error).toBeNull();
            expect(table).not.toBeNull();
            expect(pager).not.toBeNull();
        });
    });

    describe("Template: rendering of players listing", () => {
        beforeEach(async(() => {
            component.filter = filter;
            component.players = list;

            component.players.data[0].memberships = [
                { registrationNumber: "111222", golfClub: "Black Bridge", isHome: false, isBlocked: false, isDeleted: false },
                { registrationNumber: "888999", golfClub: "Augusta National", isHome: false, isBlocked: false, isDeleted: false }
            ];

            component.players.data[1].memberships = [
                { registrationNumber: "123456", golfClub: "Black Bridge", isHome: false, isBlocked: false, isDeleted: false }
            ];

            fixture.detectChanges();
        }));

        it("should render players list with detailed membership data", () => {
            const rows = fixture.debugElement.queryAll(By.css("tbody > tr"));

            checkPlayerFundamentals(rows[0], "Donald Trump", "32.0");
            checkRegistrationNumbers(rows[0], ["111222", "888999"]);
            checkGolfClubs(rows[0], ["Black Bridge", "Augusta National"]);
            checkEmptyMembershipAttributes(rows[0], 2);

            checkPlayerFundamentals(rows[1], "James Bond", "6.8");
            checkRegistrationNumbers(rows[1], ["123456"]);
            checkGolfClubs(rows[1], ["Black Bridge"]);
            checkEmptyMembershipAttributes(rows[1], 1);

            function checkPlayerFundamentals(row: DebugElement, name: string, handicap: string) {
                expect(row.children[0].nativeElement.textContent).toBe(name);
                expect(row.children[1].nativeElement.textContent).toBe(handicap);
            }

            function checkRegistrationNumbers(row: DebugElement, registrationNumbers: string[]) {
                const divs = row.children[2].queryAll(By.css("div"));

                expect(divs.map(x => x.nativeElement.textContent.trim())).toEqual(registrationNumbers);
            }

            function checkGolfClubs(row: DebugElement, golfClubs: string[]) {
                const divs = row.children[3].queryAll(By.css("div"));

                expect(divs.map(x => x.nativeElement.textContent.trim())).toEqual(golfClubs);
            }

            function checkEmptyMembershipAttributes(row: DebugElement, expectedCount: number) {
                for (let i = 4; i <= 6; i++) {
                    const brs = row.children[4].queryAll(By.css("div > br"));

                    expect(brs.length).toBe(expectedCount);
                }
            }
        });

        it("should render label for home memberships", () => {
            component.players.data[0].memberships[1].isHome = true;
            component.players.data[1].memberships[0].isHome = true;

            fixture.detectChanges();

            const row = fixture.debugElement.queryAll(By.css("tbody > tr"));

            checkLabel(row[0].children[4].query(By.css("div:nth-of-type(2) > span.label")));
            checkLabel(row[1].children[4].query(By.css("div > span.label")));

            function checkLabel(element: DebugElement) {
                const label = element.nativeElement;

                expect(label.className).toContain("success");
                expect(label.textContent).toBe("Home club");
            }
        });

        it("should render label for blocked memberships", () => {
            component.players.data[0].memberships[1].isBlocked = true;
            component.players.data[1].memberships[0].isBlocked = true;

            fixture.detectChanges();

            const row = fixture.debugElement.queryAll(By.css("tbody > tr"));

            checkLabel(row[0].children[5].query(By.css("div:nth-of-type(2) > span.label")));
            checkLabel(row[1].children[5].query(By.css("div > span.label")));

            function checkLabel(element: DebugElement) {
                const label = element.nativeElement;

                expect(label.className).toContain("warning");
                expect(label.textContent).toBe("Blocked club");
            }
        });

        it("should render label for deleted memberships", () => {
            component.players.data[0].memberships[1].isDeleted = true;
            component.players.data[1].memberships[0].isDeleted = true;

            fixture.detectChanges();

            const row = fixture.debugElement.queryAll(By.css("tbody > tr"));

            checkLabel(row[0].children[6].query(By.css("div:nth-of-type(2) > span.label")));
            checkLabel(row[1].children[6].query(By.css("div > span.label")));

            function checkLabel(element: DebugElement) {
                const label = element.nativeElement;

                expect(label.className).toContain("alert");
                expect(label.textContent).toBe("Deleted club");
            }
        });
    });

    describe("Template: player selecting", () => {
        beforeEach(async(() => {
            component.filter = filter;
            component.players = list;
            fixture.detectChanges();
        }));

        it("should raise onPlayerSelected event when player name clicked", () => {
            let selectedPlayer: Player;
            const els = fixture.debugElement.queryAll(By.css("table a"));

            component.onPlayerSelected.subscribe(emittedPlayer => selectedPlayer = emittedPlayer);

            els[0].triggerEventHandler("click", null);
            expect(selectedPlayer.fullName).toBe("Donald Trump");

            els[1].triggerEventHandler("click", null);
            expect(selectedPlayer.fullName).toBe("James Bond");
        });
    });

    describe("ngOnChanges()", () => {
        beforeEach(() => {
            component.filter = filter;
        });

        it("should load players data with 'pageSize' equal to 20", () => {
            spyOn(service, "getPlayers").and.returnValue(from([list]));

            component.ngOnChanges(null);

            expect(service.getPlayers).toHaveBeenCalledWith(filter);
            expect(service.getPlayers).toHaveBeenCalledWith(jasmine.objectContaining({
                perPage: 20
            }));

            expect(component.players.totalCount).toBe(2);
            expect(component.players.data).toBe(players);
        });

        it("should handle data retrieval error", () => {
            spyOn(service, "getPlayers").and.returnValue(throwError(new Error("Connection error")));

            component.ngOnChanges(null);

            expect(component.isLoadingInProgress).toBeTruthy();
            expect(component.errorMessage).toBe("Connection error");
        });

        it("should reset errorMessage before loading data", () => {
            spyOn(service, "getPlayers").and.returnValue(from([list]));

            component.errorMessage = "Foo error";

            component.ngOnChanges(null);

            expect(component.errorMessage).toBeNull();
        });

        it("should set isLoadingInProgress while loading data", () => {
            spyOn(service, "getPlayers").and.callFake(function (f: PlayerFilter) {
                expect(component.isLoadingInProgress).toBeTruthy();

                return from([list]);
            });

            expect(component.isLoadingInProgress).toBeFalsy();

            component.ngOnChanges(null);

            expect(component.isLoadingInProgress).toBeFalsy();
        });

        it("should do nothing when filter is null", () => {
            spyOn(service, "getPlayers");

            component.filter = null;

            component.ngOnChanges(null);

            expect(service.getPlayers).not.toHaveBeenCalled();
            expect(component.players).toBeUndefined();
        });
    });

    describe("changePage()", () => {
        it("should emit onPageChanged event with given page number", () => {
            let selectedPage: number;

            component.onPageChanged.subscribe((page: number) => selectedPage = page);

            component.changePage(8);

            expect(selectedPage).toBe(8);
        });
    });

    describe("selectPlayer()", () => {
        it("should emit onPlayerSelected event with given player value", () => {
            const expectedPlayer: Player = {
                id: 100,
                fullName: "Donald Trump",
                handicap: 30.2,
                memberships: []
            };

            let selectedPlayer: Player;

            component.onPlayerSelected.subscribe((player: Player) => selectedPlayer = player);

            component.selectPlayer(expectedPlayer);

            expect(selectedPlayer).toBe(expectedPlayer);
        });
    });
});
