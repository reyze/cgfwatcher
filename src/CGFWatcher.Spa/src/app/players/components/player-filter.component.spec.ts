import { TestBed, ComponentFixture, async } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { from } from "rxjs";
import { TranslationModule, LocaleValidationModule, LocaleService } from "angular-l10n";

import { PlayerFilterComponent } from "./player-filter.component";
import { PlayerFilter } from "../services";

import { SharedModule, GolfClubService, GolfClub } from "../../shared";

describe("PlayerFilterComponent", () => {
    let fixture: ComponentFixture<PlayerFilterComponent>;
    let component: PlayerFilterComponent;
    let service: GolfClubService;
    let locale: LocaleService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [SharedModule, TranslationModule.forRoot({}), LocaleValidationModule.forRoot()],
            declarations: [PlayerFilterComponent],
            providers: [GolfClubService]
        }).compileComponents();

        fixture = TestBed.createComponent(PlayerFilterComponent);
        component = fixture.componentInstance;

        service = fixture.debugElement.injector.get(GolfClubService);
        locale = fixture.debugElement.injector.get(LocaleService);

        component.filter = Object.assign(<PlayerFilter>{}, {
            registrationNumberOrName: "Paul",
            handicapFrom: "10", // has to be initialized as a string due to l10nValidateNumber directive
            handicapTo: "20",
            membershipsCountFrom: 1,
            membershipsCountTo: 5,
            golfClubPrefix: "XYZ",
            isHome: true,
            isBlocked: false,
            isDeleted: true,
            page: 2,
            perPage: 20
        });

        const data: GolfClub[] = [
            { id: 123, prefix: "ABC", name: "Prague" },
            { id: 321, prefix: "XYZ", name: "London" }
        ];

        spyOn(service, "getGolfClubs").and.returnValue(from([data]));
        spyOn(locale, "getDefaultLocale").and.returnValue("cs-CZ");
    }));

    function performInputEvent(selector: string, value: string) {
        const el = fixture.debugElement.query(By.css(selector)).nativeElement;

        el.value = value;

        (el.tagName === "SELECT")
            ? el.dispatchEvent(new Event("change"))
            : el.dispatchEvent(new Event("input"));
    }

    describe("Template: filter rendering", () => {
        beforeEach(async(() => {
            fixture.detectChanges();
        }));

        it("should render filter inputs with correct values", () => {
            expect(fixture.debugElement.query(By.css("#registrationNumberOrName")).properties["value"]).toBe("Paul");
            expect(fixture.debugElement.query(By.css("#handicapFrom")).properties["value"]).toBe("10");
            expect(fixture.debugElement.query(By.css("#handicapTo")).properties["value"]).toBe("20");
            expect(fixture.debugElement.query(By.css("#golfClubPrefix")).properties["value"]).toBe("XYZ");
            expect(fixture.debugElement.query(By.css("#isHome")).properties["value"]).toBe("true");
            expect(fixture.debugElement.query(By.css("#isBlocked")).properties["value"]).toBe("false");
            expect(fixture.debugElement.query(By.css("#isDeleted")).properties["value"]).toBe("true");
        });

        it("should render select element for golf clubs", () => {
            const el = fixture.debugElement.query(By.css("#golfClubPrefix"));

            const firstClub = el.children[1].nativeElement;
            expect(firstClub.value).toBe("ABC");
            expect(firstClub.textContent).toBe("ABC – Prague");

            const secondClub = el.children[2].nativeElement;
            expect(secondClub.value).toBe("XYZ");
            expect(secondClub.textContent).toBe("XYZ – London");
        });
    });

    describe("Template: filter submitting", () => {
        beforeEach(async(() => {
            fixture.detectChanges();
        }));

        it("should trigger onFilterSubmit event", () => {
            let submittedFilter: PlayerFilter;
            const el = fixture.debugElement.query(By.css("form"));

            component.onFilterSubmit.subscribe((filter: PlayerFilter) => submittedFilter = filter);

            el.triggerEventHandler("submit", null);

            expect(submittedFilter).toEqual(
                Object.assign(component.filter, {
                    handicapFrom: 10, // initialized as a string but we compare numbers
                    handicapTo: 20
                })
            );
        });

        it("should incorporate user input", () => {
            performInputEvent("#registrationNumberOrName", "Peter");
            performInputEvent("#handicapFrom", "10.5");
            performInputEvent("#handicapTo", "36");
            performInputEvent("#golfClubPrefix", "ABC");
            performInputEvent("#isHome", "false");
            performInputEvent("#isBlocked", "true");
            performInputEvent("#isDeleted", "false");

            expect(component.filter.registrationNumberOrName).toBe("Peter");
            expect(component.filter.handicapFrom).toBe(<any>"10.5");
            expect(component.filter.handicapTo).toBe(<any>"36");
            expect(component.filter.golfClubPrefix).toBe("ABC");
            expect(component.filter.isHome).toBe(<any>"false");
            expect(component.filter.isBlocked).toBe(<any>"true");
            expect(component.filter.isDeleted).toBe(<any>"false");
        });
    });

    describe("Template: localization", () => {
        beforeEach(async(() => {
            component.filter = Object.assign(<PlayerFilter>{}, {
                handicapFrom: "15.5", // has to be initialized as a string due to l10nValidateNumber directive
                handicapTo: "30.8"
            });

            fixture.detectChanges();
        }));

        it("should render localized values for decimal filter inputs", async(() => {
            expect(fixture.debugElement.query(By.css("#handicapFrom")).properties["value"]).toBe("15,5");
            expect(fixture.debugElement.query(By.css("#handicapTo")).properties["value"]).toBe("30,8");
        }));

        it("should submit decimal filter values in local format", () => {
            let submittedFilter: PlayerFilter;
            const el = fixture.debugElement.query(By.css("form"));

            component.onFilterSubmit.subscribe((filter: PlayerFilter) => submittedFilter = filter);

            performInputEvent("#handicapFrom", "10,5");
            performInputEvent("#handicapTo", "12,3");

            el.triggerEventHandler("submit", null);

            expect(submittedFilter.handicapFrom).toEqual(10.5);
            expect(submittedFilter.handicapTo).toEqual(12.3);
        });
    });

    describe("ngOnInit()", () => {
        it("should load golf clubs", () => {
            component.ngOnInit();

            expect(component.golfClubs.length).toBe(2);
            expect(component.golfClubs[0]).toEqual({ id: 123, prefix: "ABC", name: "Prague" });
        });
    });

    describe("localizeFilter()", () => {
        it("should return filter value when it is a number in locale format", () => {
            expect(component.localizeFilter("8,5")).toBe("8,5");
        });

        it("should return localized filter value when it is a number in invariant format", () => {
            expect(component.localizeFilter("8.5")).toBe("8,5");
        });

        it("should limit maximum fraction digits to one", () => {
            expect(component.localizeFilter("8.58")).toBe("8,6");
        });

        it("should return empty string when filter is not specified", () => {
            expect(component.localizeFilter(null)).toBe("");
            expect(component.localizeFilter("")).toBe("");
            expect(component.localizeFilter(undefined)).toBe("");
        });

        it("should return empty string when filter value is not a number", () => {
            expect(component.localizeFilter("foobar")).toBe("");
        });
    });

    describe("submitFilter()", () => {
        it("should emit onFilterSubmit event with filter value", () => {
            let submittedFilter: PlayerFilter;

            component.onFilterSubmit.subscribe((filter: PlayerFilter) => submittedFilter = filter);
            component.submitFilter();

            expect(submittedFilter).toEqual(
                Object.assign(component.filter, {
                    handicapFrom: 10, // initialized as a string but we compare numbers
                    handicapTo: 20
                })
            );
        });
    });
});
