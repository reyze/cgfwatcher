﻿import { Player } from "./player";

export let PLAYERS: Player[] = [
    {
        id: 1,
        fullName: "Pavel",
        handicap: 10,
        memberships: [
            { registrationNumber: "123456", golfClub: "Black Bridge", isHome: true, isBlocked: false, isDeleted: false },
            { registrationNumber: "654321", golfClub: "Poděbrady", isHome: false, isBlocked: false, isDeleted: false }
        ]
    },
    {
        id: 2,
        fullName: "Petra",
        handicap: 32,
        memberships: [
            { registrationNumber: "111111", golfClub: "Black Bridge", isHome: true, isBlocked: false, isDeleted: false }
        ]
    },
    {
        id: 3,
        fullName: "Lenka",
        handicap: 23.5,
        memberships: [
            { registrationNumber: "222222", golfClub: "Black Bridge", isHome: true, isBlocked: false, isDeleted: false },
            { registrationNumber: "222223", golfClub: "Poděbrady", isHome: false, isBlocked: true, isDeleted: false }
        ]
    },
    {
        id: 4,
        fullName: "Tomáš",
        handicap: 15,
        memberships: [
            { registrationNumber: "333333", golfClub: "Black Bridge", isHome: true, isBlocked: false, isDeleted: false }
        ]
    },
    {
        id: 5,
        fullName: "Jirka",
        handicap: 19.8,
        memberships: [
            { registrationNumber: "444444", golfClub: "Black Bridge", isHome: true, isBlocked: false, isDeleted: false }
        ]
    },
    {
        id: 6,
        fullName: "Filip",
        handicap: 8.3,
        memberships: [
            { registrationNumber: "555555", golfClub: "Black Bridge", isHome: true, isBlocked: false, isDeleted: false }
        ]
    }
];
