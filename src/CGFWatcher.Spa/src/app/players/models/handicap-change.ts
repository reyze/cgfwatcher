﻿export class HandicapChange {
    id: number;
    playerId: number;
    newHandicap: number;
    changed: Date;
}
