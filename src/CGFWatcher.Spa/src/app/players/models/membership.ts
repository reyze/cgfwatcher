﻿export class Membership {
    registrationNumber: string;
    golfClub: string;
    isHome: boolean;
    isBlocked: boolean;
    isDeleted: boolean;
}
