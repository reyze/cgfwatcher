﻿import { Membership } from "./membership";

export class Player {
    id: number;
    fullName: string;
    handicap: number;
    memberships: Membership[];
}
