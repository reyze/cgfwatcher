﻿import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { PlayerComponent, PlayerDetailComponent } from "./components";

const playersRoutes: Routes = [
    {
        path: "players",
        component: PlayerComponent,
        children: [
            { path: ":id", component: PlayerDetailComponent }
        ]
    }
];

export const playersRouting: ModuleWithProviders = RouterModule.forChild(playersRoutes);
