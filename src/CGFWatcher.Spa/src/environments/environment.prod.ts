export const environment = {
  production: true,
  defaultLocale: "cs-CZ",
  apiUrl: "https://localhost:50100"
};
