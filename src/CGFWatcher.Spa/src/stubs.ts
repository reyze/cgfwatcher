/* tslint:disable */

import { Component } from "@angular/core";
import { BehaviorSubject } from "rxjs";

export class RouterStub {
    navigate(commands: any[]) { return "foo"; }
}

export class ActivatedRouteStub {
    // ActivatedRoute.params is Observable
    private subject = new BehaviorSubject(this.testParams);
    params = this.subject.asObservable();

    // Test parameters
    private _testParams: {};
    get testParams() { return this._testParams; }
    set testParams(params: {}) {
        this._testParams = params;
        this.subject.next(params);
    }
}

@Component({ selector: "router-outlet", template: "" })
export class RouterOutletStubComponent { }
