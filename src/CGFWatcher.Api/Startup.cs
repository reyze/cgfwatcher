﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CGFWatcher.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore().AddJsonFormatters(options =>
            {
                options.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.DefaultValueHandling = DefaultValueHandling.Include;
                options.NullValueHandling = NullValueHandling.Ignore;
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddHttpsRedirection(options =>
            {
                options.RedirectStatusCode = StatusCodes.Status301MovedPermanently;
            });

            services.AddCors();

            services.AddCGFWatcher(Configuration.GetConnectionString("WatcherConnection"));

            services.AddTransient<IPlayerRepository, PlayerRepository>();
            services.AddTransient<IGolfClubRepository, GolfClubRepository>();
            services.AddTransient<IHandicapChangeRepository, HandicapChangeRepository>();
            services.AddTransient<IPlayerSnapshotRepository, PlayerSnapshotRepository>();
            services.AddTransient<IMembershipSnapshotRepository, MembershipSnapshotRepository>();
            services.AddTransient<IClubComparisonRepository, ClubComparisonRepository>();
        }

        public void Configure(IApplicationBuilder app)
        {
            var corsOrigins = Configuration.GetSection("AppSettings:CorsOrigins").Value;

            app.UseHttpsRedirection();
            app.UseCors(builder => builder.WithOrigins(corsOrigins).AllowAnyHeader().AllowAnyMethod().WithExposedHeaders("X-Total-Count"));
            app.UseMvc();
        }
    }
}
