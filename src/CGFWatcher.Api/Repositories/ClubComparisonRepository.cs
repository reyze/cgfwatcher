﻿using System.Linq;
using CGFWatcher.Database;
using Microsoft.EntityFrameworkCore;

namespace CGFWatcher.Api
{
    public class ClubComparisonRepository : IClubComparisonRepository
    {
        private WatcherContext _context;

        public ClubComparisonRepository(WatcherContext context)
        {
            _context = context;
        }

        public ClubComparisonData GetClubComparison(ClubComparisonFilter filter)
        {
            var query = _context.MembershipSnapshots
                .Include(x => x.GolfClub)
                .Where(x => x.Created.Date <= filter.Date)
                .OrderByDescending(x => x.Created);

            return new ClubComparisonData
            {
                GolfClubData1 = query.FirstOrDefault(x => x.GolfClub.Prefix == filter.GolfClubPrefix1),
                GolfClubData2 = query.FirstOrDefault(x => x.GolfClub.Prefix == filter.GolfClubPrefix2)
            };
        }
    }
}
