﻿using System.Collections.Generic;
using CGFWatcher.Database;

namespace CGFWatcher.Api
{
    public interface IGolfClubRepository
    {
        IList<GolfClub> GetGolfClubs();
    }
}
