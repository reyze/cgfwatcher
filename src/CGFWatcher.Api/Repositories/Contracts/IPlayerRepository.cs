﻿namespace CGFWatcher.Api
{
    public interface IPlayerRepository
    {
        PlayerData GetPlayer(int id);

        IPagedList<PlayerData> GetPlayers(PlayerFilter filter);
    }
}
