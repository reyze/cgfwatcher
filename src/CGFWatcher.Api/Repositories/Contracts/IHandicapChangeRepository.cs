﻿using System.Collections.Generic;
using CGFWatcher.Database;

namespace CGFWatcher.Api
{
    public interface IHandicapChangeRepository
    {
        IList<HandicapChange> GetHandicapChanges(HandicapChangeFilter filter);
    }
}
