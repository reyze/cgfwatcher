﻿using System.Collections.Generic;
using CGFWatcher.Database;

namespace CGFWatcher.Api
{
    public interface IPlayerSnapshotRepository
    {
        IList<PlayerSnapshot> GetPlayerSnapshots(PlayerSnapshotFilter filter);
    }
}
