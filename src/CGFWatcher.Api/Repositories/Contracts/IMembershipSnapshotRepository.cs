﻿using System.Collections.Generic;
using CGFWatcher.Database;

namespace CGFWatcher.Api
{
    public interface IMembershipSnapshotRepository
    {
        IList<MembershipSnapshot> GetMembershipSnapshots(MembershipSnapshotFilter filter);
    }
}
