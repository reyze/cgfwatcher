﻿
namespace CGFWatcher.Api
{
    public interface IClubComparisonRepository
    {
        ClubComparisonData GetClubComparison(ClubComparisonFilter filter);
    }
}
