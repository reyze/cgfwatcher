﻿using System;
using System.Collections.Generic;
using System.Linq;
using CGFWatcher.Database;
using CGFWatcher.Services;

namespace CGFWatcher.Api
{
    public class HandicapChangeRepository : IHandicapChangeRepository
    {
        private WatcherContext _context;
        private IClockProvider _clockProvider;

        public HandicapChangeRepository(WatcherContext context, IClockProvider clockProvider)
        {
            _context = context;
            _clockProvider = clockProvider;
        }

        public IList<HandicapChange> GetHandicapChanges(HandicapChangeFilter filter)
        {
            var query = _context.HandicapChanges
                .Where(x => x.PlayerID == filter.PlayerID);

            if (filter.Year.HasValue)
            {
                query = query.Where(x => x.Changed.Year == filter.Year);
            }

            var list = query.OrderBy(x => x.Changed).ToList();

            StandardizeFirstItem(list, filter);
            StandardizeLastItem(list, filter);

            return list;
        }

        public void StandardizeFirstItem(IList<HandicapChange> list, HandicapChangeFilter filter)
        {
            if (!filter.Year.HasValue)
            {
                return;
            }

            var firstItem = list.FirstOrDefault();
            
            if ((firstItem != null) && (firstItem.Changed.Date == new DateTime(filter.Year.Value, 1, 1)))
            {
                return; // time series already has a value that represents first day of the year
            }

            var previousHandicap = _context.HandicapChanges
                .Where(x => x.PlayerID == filter.PlayerID)
                .Where(x => x.Changed < new DateTime(filter.Year.Value, 1, 1))
                .OrderByDescending(x => x.Changed)
                .FirstOrDefault();

            if (previousHandicap != null)
            {
                // add last known handicap from previous year to the beginning of the time series
                list.Insert(0, new HandicapChange
                {
                    PlayerID = filter.PlayerID,
                    Changed = new DateTime(filter.Year.Value, 1, 1),
                    NewHandicap = previousHandicap.NewHandicap
                });
            }
        }
        public void StandardizeLastItem(IList<HandicapChange> list, HandicapChangeFilter filter)
        {
            var lastItem = list.LastOrDefault();

            if (lastItem == null)
            {
                return;
            }

            if (!filter.Year.HasValue || (_clockProvider.Today.Year == filter.Year))
            {
                if (lastItem.Changed.Date == _clockProvider.Today)
                {
                    return;
                }

                // add the last handicap value to the end of the time series (today)
                list.Add(new HandicapChange
                {
                    PlayerID = filter.PlayerID,
                    Changed = _clockProvider.Today,
                    NewHandicap = lastItem.NewHandicap
                });

                return;
            }
            
            if (lastItem.Changed.Date == new DateTime(filter.Year.Value, 12, 31))
            {
                return;
            }

            // add the last handicap value to the end of the year
            list.Add(new HandicapChange
            {
                PlayerID = filter.PlayerID,
                Changed = new DateTime(filter.Year.Value, 12, 31),
                NewHandicap = lastItem.NewHandicap
            });
        }
    }
}
