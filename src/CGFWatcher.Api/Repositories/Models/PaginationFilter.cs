﻿using Microsoft.AspNetCore.Mvc;

namespace CGFWatcher.Api
{
    public class PaginationFilter
    {
        public int Page { get; set; }
        public int PerPage { get; set; }
    }
}
