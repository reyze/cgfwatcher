﻿using System;

namespace CGFWatcher.Api
{
    public class PlayerSnapshotFilter
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
