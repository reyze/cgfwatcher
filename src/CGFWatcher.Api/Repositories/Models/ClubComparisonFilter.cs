﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CGFWatcher.Api
{
    public class ClubComparisonFilter
    {
        public DateTime Date { get; set; }

        [Required]
        public string GolfClubPrefix1 { get; set; }

        [Required]
        public string GolfClubPrefix2 { get; set; }
    }
}
