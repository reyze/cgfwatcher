﻿namespace CGFWatcher.Api
{
    public class HandicapChangeFilter
    {
        public int PlayerID { get; set; }
        public int? Year { get; set; }
    }
}
