﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CGFWatcher.Api
{
    public class MembershipSnapshotFilter
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }

        [Required]
        public string GolfClubPrefix { get; set; }
    }
}
