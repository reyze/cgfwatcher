﻿using System.Collections.Generic;

namespace CGFWatcher.Api
{
    public interface IPagedList<T> : IList<T>
    {
        int TotalCount { get; }
    }
}
