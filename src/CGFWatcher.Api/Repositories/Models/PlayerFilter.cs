﻿namespace CGFWatcher.Api
{
    public class PlayerFilter : PaginationFilter
    {
        public string RegistrationNumberOrName { get; set; }
        public decimal? HandicapFrom { get; set; }
        public decimal? HandicapTo { get; set; }
        public int? MembershipsCountFrom { get; set; }
        public int? MembershipsCountTo { get; set; }
        public string GolfClubPrefix { get; set; }
        public bool? IsHome { get; set; }
        public bool? IsBlocked { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
