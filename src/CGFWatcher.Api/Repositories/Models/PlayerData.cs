﻿namespace CGFWatcher.Api
{
    public class PlayerData
    {
        public int ID { get; set; }
        public string FullName { get; set; }
        public decimal Handicap { get; set; }
        public MembershipData[] Memberships { get; set; }

        public class MembershipData
        {
            public string RegistrationNumber { get; set; }
            public string GolfClub { get; set; }
            public bool IsHome { get; set; }
            public bool IsBlocked { get; set; }
            public bool IsDeleted { get; set; }
        }
    }
}
