﻿using System.Collections.Generic;

namespace CGFWatcher.Api
{
    public class PagedList<T> : List<T>, IPagedList<T>
    {
        public int TotalCount { get; private set; }

        public PagedList(IEnumerable<T> collection, int totalCount)
            : base(collection)
        {
            TotalCount = totalCount;
        }        
    }
}
