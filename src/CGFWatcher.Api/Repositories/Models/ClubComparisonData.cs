﻿using CGFWatcher.Database;

namespace CGFWatcher.Api
{
    public class ClubComparisonData
    {
        public MembershipSnapshot GolfClubData1 { get; set; }
        public MembershipSnapshot GolfClubData2 { get; set; }
    }
}
