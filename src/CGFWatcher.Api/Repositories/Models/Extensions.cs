﻿using System.Linq;

namespace CGFWatcher.Api
{
    public static class ModelExtensions
    {
        public static IPagedList<T> ToPagedList<T>(this IQueryable<T> query, PaginationFilter filter)
        {
            var count = query.Count();
            var list = query.Skip(filter.PerPage * (filter.Page - 1)).Take(filter.PerPage).ToList();

            return new PagedList<T>(list, count);
        }
    }
}
