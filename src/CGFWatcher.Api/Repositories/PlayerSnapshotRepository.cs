﻿using System.Collections.Generic;
using System.Linq;
using CGFWatcher.Database;

namespace CGFWatcher.Api
{
    public class PlayerSnapshotRepository : IPlayerSnapshotRepository
    {
        private WatcherContext _context;

        public PlayerSnapshotRepository(WatcherContext context)
        {
            _context = context;
        }

        public IList<PlayerSnapshot> GetPlayerSnapshots(PlayerSnapshotFilter filter)
        {
            return _context.PlayerSnapshots
                .Where(x => x.Created >= filter.DateFrom)
                .Where(x => x.Created.Date <= filter.DateTo)
                .OrderByDescending(x => x.Created)
                .ToList();
        }
    }
}
