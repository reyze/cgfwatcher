﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using CGFWatcher.Database;
using Microsoft.EntityFrameworkCore;

namespace CGFWatcher.Api
{
    public class PlayerRepository : IPlayerRepository
    {
        private WatcherContext _context;

        private Func<Player, PlayerData> playerProjection = x => new PlayerData
        {
            ID = x.ID,
            FullName = x.FullName,
            Handicap = x.Handicap,
            Memberships = x.Memberships
                .Select(y => new PlayerData.MembershipData
                {
                    RegistrationNumber = y.RegistrationNumber,
                    GolfClub = y.GolfClub.Name,
                    IsHome = y.IsHome,
                    IsBlocked = y.IsBlocked,
                    IsDeleted = y.IsDeleted
                })
                .OrderByDescending(y => y.IsHome)
                .ThenBy(y => y.IsDeleted)
                .ThenBy(y => y.RegistrationNumber)
                .ToArray()
        };

        public PlayerRepository(WatcherContext context)
        {
            _context = context;
        }

        public PlayerData GetPlayer(int id)
        {
            var data = _context.Players
                .Include(x => x.Memberships)
                .ThenInclude(x => x.GolfClub)
                .FirstOrDefault(x => x.ID == id);

            if (data == null)
            {
                return null;
            }

            return playerProjection(data);
        }

        public IPagedList<PlayerData> GetPlayers(PlayerFilter filter)
        {
            var query = _context.Players
                .Include(x => x.Memberships)
                .ThenInclude(x => x.GolfClub)
                .Where(x => x.Memberships.Any());

            var data = ApplyFilter(query, filter)
                .OrderBy(x => x.FullName)
                .ToPagedList(filter);

            // in-memory evalutation to boost poor EF Core performance
            var projection = data.Select(playerProjection);
            
            return new PagedList<PlayerData>(projection, data.TotalCount);
        }

        private IQueryable<Player> ApplyFilter(IQueryable<Player> query, PlayerFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.RegistrationNumberOrName))
            {
                if (Regex.IsMatch(filter.RegistrationNumberOrName, "^[0-9]*$"))
                {
                    query = query.Where(x => x.Memberships.Any(y => y.RegistrationNumber.Contains(filter.RegistrationNumberOrName)));
                }
                else
                {
                    query = query.Where(x => x.FullName.Contains(filter.RegistrationNumberOrName));
                }
            }

            if (filter.HandicapFrom.HasValue)
            {
                query = query.Where(x => x.Handicap >= filter.HandicapFrom);
            }

            if (filter.HandicapTo.HasValue)
            {
                query = query.Where(x => x.Handicap <= filter.HandicapTo);
            }

            if (filter.MembershipsCountFrom.HasValue)
            {
                query = query.Where(x => x.Memberships.Count() >= filter.MembershipsCountFrom);
            }

            if (filter.MembershipsCountTo.HasValue)
            {
                query = query.Where(x => x.Memberships.Count() <= filter.MembershipsCountTo);
            }

            if (!string.IsNullOrEmpty(filter.GolfClubPrefix))
            {
                var golfClubID = _context.GolfClubs.Where(x => x.Prefix == filter.GolfClubPrefix).Select(x => x.ID).FirstOrDefault();

                query = query.Where(x => x.Memberships.Any(y => y.GolfClubID == golfClubID));

                if (filter.IsHome.HasValue)
                {
                    query = query.Where(x => x.Memberships.Any(y => y.IsHome == filter.IsHome && y.GolfClubID == golfClubID));
                }

                if (filter.IsBlocked.HasValue)
                {
                    query = query.Where(x => x.Memberships.Any(y => y.IsBlocked == filter.IsBlocked && y.GolfClubID == golfClubID));
                }

                if (filter.IsDeleted.HasValue)
                {
                    query = query.Where(x => x.Memberships.Any(y => y.IsDeleted == filter.IsDeleted && y.GolfClubID == golfClubID));
                }
            }
            else
            {
                if (filter.IsHome.HasValue)
                {
                    query = filter.IsHome.Value
                        ? query.Where(x => x.Memberships.All(y => y.IsHome))
                        : query.Where(x => x.Memberships.Any(y => !y.IsHome));
                }

                if (filter.IsBlocked.HasValue)
                {
                    query = filter.IsBlocked.Value
                        ? query.Where(x => x.Memberships.All(y => y.IsBlocked))
                        : query.Where(x => x.Memberships.Any(y => !y.IsBlocked));
                }

                if (filter.IsDeleted.HasValue)
                {
                    query = filter.IsDeleted.Value
                        ? query.Where(x => x.Memberships.All(y => y.IsDeleted))
                        : query.Where(x => x.Memberships.Any(y => !y.IsDeleted));
                }
            }

            return query;
        }
    }
}
