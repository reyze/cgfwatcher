﻿using System.Collections.Generic;
using System.Linq;
using CGFWatcher.Database;

namespace CGFWatcher.Api
{
    public class MembershipSnapshotRepository : IMembershipSnapshotRepository
    {
        private WatcherContext _context;

        public MembershipSnapshotRepository(WatcherContext context)
        {
            _context = context;
        }

        public IList<MembershipSnapshot> GetMembershipSnapshots(MembershipSnapshotFilter filter)
        {
            return _context.MembershipSnapshots
                .Where(x => x.Created >= filter.DateFrom)
                .Where(x => x.Created.Date <= filter.DateTo)
                .Where(x => x.GolfClub.Prefix == filter.GolfClubPrefix)
                .OrderByDescending(x => x.Created)
                .ToList();
        }
    }
}
