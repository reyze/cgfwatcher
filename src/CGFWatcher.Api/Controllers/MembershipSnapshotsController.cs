﻿using System.Collections.Generic;
using CGFWatcher.Database;
using Microsoft.AspNetCore.Mvc;

namespace CGFWatcher.Api
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class MembershipSnapshotsController : ControllerBase
    {
        private IMembershipSnapshotRepository _membershipSnapshotRepository;

        public MembershipSnapshotsController(IMembershipSnapshotRepository membershipSnapshotRepository)
        {
            _membershipSnapshotRepository = membershipSnapshotRepository;
        }

        [HttpGet]
        public IEnumerable<MembershipSnapshot> Get([FromQuery] MembershipSnapshotFilter filter)
        {
            return _membershipSnapshotRepository.GetMembershipSnapshots(filter);
        }
    }
}
