﻿using System.Collections.Generic;
using CGFWatcher.Database;
using Microsoft.AspNetCore.Mvc;

namespace CGFWatcher.Api
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class PlayerSnapshotsController : ControllerBase
    {
        private IPlayerSnapshotRepository _playerSnapshotRepository;

        public PlayerSnapshotsController(IPlayerSnapshotRepository playerSnapshotRepository)
        {
            _playerSnapshotRepository = playerSnapshotRepository;
        }

        [HttpGet]
        public IEnumerable<PlayerSnapshot> Get([FromQuery] PlayerSnapshotFilter filter)
        {
            return _playerSnapshotRepository.GetPlayerSnapshots(filter);
        }
    }
}
