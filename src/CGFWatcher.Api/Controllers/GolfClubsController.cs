﻿using System.Collections.Generic;
using CGFWatcher.Database;
using Microsoft.AspNetCore.Mvc;

namespace CGFWatcher.Api
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class GolfClubsController : ControllerBase
    {
        private IGolfClubRepository _golfClubRepository;

        public GolfClubsController(IGolfClubRepository golfClubRepository)
        {
            _golfClubRepository = golfClubRepository;
        }

        [HttpGet]
        public IEnumerable<GolfClub> Get()
        {
            return _golfClubRepository.GetGolfClubs();
        }
    }
}
