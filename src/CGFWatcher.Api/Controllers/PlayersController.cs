﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace CGFWatcher.Api
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class PlayersController : ControllerBase
    {
        private IPlayerRepository _playerRepository;

        public PlayersController(IPlayerRepository playerRepository)
        {
            _playerRepository = playerRepository;
        }

        [HttpGet]
        public IEnumerable<PlayerData> Get([FromQuery] PlayerFilter filter)
        {
            var data = _playerRepository.GetPlayers(filter);

            // TODO: can be moved to MVC filter for later reuse
            Response.AddPaginationHeaders(data);

            return data;
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var player = _playerRepository.GetPlayer(id);

            if (player == null)
            {
                return NotFound();
            }

            return new ObjectResult(player);
        }
    }
}
