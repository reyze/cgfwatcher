﻿using Microsoft.AspNetCore.Http;

namespace CGFWatcher.Api
{
    public static class ControllerExtensions
    {
        public static void AddPaginationHeaders<T>(this HttpResponse response, IPagedList<T> data)
        {
            response.Headers.Add("X-Total-Count", data.TotalCount.ToString());
        }
    }
}
