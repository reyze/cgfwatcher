﻿using Microsoft.AspNetCore.Mvc;

namespace CGFWatcher.Api
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ClubComparisonController : ControllerBase
    {
        private IClubComparisonRepository _clubComparisonRepository;

        public ClubComparisonController(IClubComparisonRepository clubComparisonRepository)
        {
            _clubComparisonRepository = clubComparisonRepository;
        }

        [HttpGet]
        public ClubComparisonData Get([FromQuery] ClubComparisonFilter filter)
        {
            return _clubComparisonRepository.GetClubComparison(filter);
        }
    }
}
