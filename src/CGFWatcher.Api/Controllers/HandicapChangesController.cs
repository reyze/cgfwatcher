﻿using System.Collections.Generic;
using CGFWatcher.Database;
using Microsoft.AspNetCore.Mvc;

namespace CGFWatcher.Api
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class HandicapChangesController : ControllerBase
    {
        private IHandicapChangeRepository _handicapChangeRepository;

        public HandicapChangesController(IHandicapChangeRepository handicapChangeRepository)
        {
            _handicapChangeRepository = handicapChangeRepository;
        }

        [HttpGet]
        public IEnumerable<HandicapChange> Get([FromQuery] HandicapChangeFilter filter)
        {
            return _handicapChangeRepository.GetHandicapChanges(filter);
        }
    }
}
