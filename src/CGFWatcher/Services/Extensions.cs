﻿namespace CGFWatcher.Services
{
    public static class Extensions
    {
        public static decimal ParseHandicap(this string value)
        {
            var hcp = decimal.Parse(value);

            if (value.StartsWith("+"))
            {
                return -1 * hcp;
            }

            return hcp;
        }

        public static string FormatHandicap(this decimal value)
        {
            if (value >= 0)
            {
                return value.ToString("n1");
            }

            return string.Concat("+", (-1 * value).ToString("n1"));
        }
    }
}
