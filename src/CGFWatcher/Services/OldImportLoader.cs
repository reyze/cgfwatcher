﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace CGFWatcher.Services
{
    public class OldImportLoader : IImportLoader
    {
        private const string _url = "http://cgf.cz/Golfers.aspx";

        private string _viewState;
        private string _viewStateGenerator;
        private string _playersHtml;

        public async Task<string> LoadPlayersHtml(ImportParameters parameters)
        {
            var initialHtml = await ProcessWebRequest("GET");

            ParseViewStateData(initialHtml);

            while (!await TryPostRequest(parameters))
            {
            }

            return _playersHtml;
        }

        private async Task<bool> TryPostRequest(ImportParameters parameters)
        {
            var postData = PreparePostData(parameters);

            try
            {
                _playersHtml = await ProcessWebRequest("POST", postData);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private async Task<string> ProcessWebRequest(string method, IDictionary<string, string> postData = null)
        {
            var cookies = new CookieContainer();
            var webRequest = WebRequest.Create(_url) as HttpWebRequest;

            webRequest.Method = method;
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.CookieContainer = cookies;
            webRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            webRequest.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36";

            if (postData != null)
            {
                var postString = string.Join("&", postData.Select(x => string.Concat(WebUtility.UrlEncode(x.Key), "=", WebUtility.UrlEncode(x.Value))));

                webRequest.Headers[HttpRequestHeader.Referer] = _url;
                webRequest.Headers[HttpRequestHeader.ContentLength] = postString.Length.ToString();

                using (var writer = new StreamWriter(await webRequest.GetRequestStreamAsync()))
                {
                    writer.Write(postString);
                }
            }

            string result;

            using (var response = await webRequest.GetResponseAsync())
            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                result = await reader.ReadToEndAsync();
            }

            return result;
        }

        private void ParseViewStateData(string html)
        {
            var doc = new HtmlDocument();

            doc.LoadHtml(html);
            
            _viewState = doc.DocumentNode
                .SelectNodes("//input[@id='__VIEWSTATE']")
                .FirstOrDefault()
                .GetAttributeValue("value", string.Empty);
            
            _viewStateGenerator = doc.DocumentNode
                .SelectNodes("//input[@id='__VIEWSTATEGENERATOR']")
                .FirstOrDefault()
                .GetAttributeValue("value", string.Empty);
        }

        private IDictionary<string, string> PreparePostData(ImportParameters parameters)
        {
            return new Dictionary<string, string>
            {
                { "__EVENTTARGET", "" },
                { "__EVENTARGUMENT", "" },
                { "__LASTFOCUS", "" },
                { "__VIEWSTATE", _viewState },
                { "__VIEWSTATEGENERATOR", _viewStateGenerator },
                { "ctl00$tbHcp", "" },
                { "ctl00$usrCourseSearch$seCountry", "CZE" },
                { "ctl00$usrCourseSearch$seRegion", "21157493" },
                { "ctl00$usrCourseSearch$seType", "OUT" },
                { "ctl00$usrTopMenu1$tbSearchString", "" },
                { "ctl00$MainPlaceHolder$tbSurname", "" },
                { "ctl00$MainPlaceHolder$tbName", "" },
                { "ctl00$MainPlaceHolder$tbMemNum", "" },
                { "ctl00$MainPlaceHolder$seSex", "-1" },
                { "ctl00$MainPlaceHolder$tbHcpFrom$tbHcp", parameters.HandicapFrom },
                { "ctl00$MainPlaceHolder$tbHcpTo$tbHcp", parameters.HandicapTo },
                { "ctl00$MainPlaceHolder$tbAgeCatFrom", "" },
                { "ctl00$MainPlaceHolder$tbAgeCatTo", "" },
                { "ctl00$MainPlaceHolder$msGolfRegion", "" },
                { "ctl00$MainPlaceHolder$msRegion", "" },
                { "ctl00$MainPlaceHolder$chbOnlyHomeMemberYes", "" },
                { "ctl00$MainPlaceHolder$chbFullText", "" },
                { "ctl00$MainPlaceHolder$btFilter", "Vyhledat" },
            };
        }
    }
}
