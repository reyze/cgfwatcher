﻿using System;

namespace CGFWatcher.Services
{
    public class ClockProvider : IClockProvider
    {
        public DateTime Now
        {
            get
            {
                return DateTime.Now;
            }
        }

        public DateTime Today
        {
            get
            {
                return DateTime.Today;
            }
        }
    }
}
