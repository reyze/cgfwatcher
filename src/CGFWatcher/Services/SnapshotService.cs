﻿using System.Linq;
using CGFWatcher.Database;

namespace CGFWatcher.Services
{
    public class SnapshotService : ISnapshotService
    {
        private WatcherContext _context;
        private IClockProvider _clockProvider;

        public SnapshotService(WatcherContext context, IClockProvider clockProvider)
        {
            _context = context;
            _clockProvider = clockProvider;
        }

        public void CreateSnapshots()
        {
            CreatePlayerSnapshot();
            CreateMembershipSnapshot();

            _context.SaveChanges();
        }

        private void CreatePlayerSnapshot()
        {
            var handicaps = _context.Players.Where(x => x.Memberships.Any(y => !y.IsDeleted))
                .Select(x => x.Handicap)
                .ToList();

            _context.PlayerSnapshots.Add(new PlayerSnapshot
            {
                Created = _clockProvider.Now,
                Handicap_lt_0 = handicaps.Count(x => x >= -8 && x < 0),
                Handicap_0_10 = handicaps.Count(x => x >= 0 && x < 10),
                Handicap_10_20 = handicaps.Count(x => x >= 10 && x < 20),
                Handicap_20_30 = handicaps.Count(x => x >= 20 && x < 30),
                Handicap_30_36 = handicaps.Count(x => x >= 30 && x <= 36),
                Handicap_37_53 = handicaps.Count(x => x >= 37 && x <= 53),
                Handicap_54 = handicaps.Count(x => x == 54)
            });
        }

        private void CreateMembershipSnapshot()
        {
            var memberships = _context.Memberships.Where(x => !x.IsDeleted)
                .Select(x => new
                {
                    x.GolfClubID,
                    x.IsHome,
                    x.IsBlocked,
                    x.Player.Handicap
                })
                .ToList();

            var snapshots = memberships.GroupBy(x => x.GolfClubID)
                .Select(x => new MembershipSnapshot
                {
                    GolfClubID = x.Key,
                    Created = _clockProvider.Now,
                    Total = x.Count(),
                    TotalHome = x.Count(y => y.IsHome),
                    TotalBlocked = x.Count(y => y.IsBlocked),
                    Handicap_lt_0 = x.Count(y => y.Handicap >= -8 && y.Handicap < 0),
                    Handicap_0_10 = x.Count(y => y.Handicap >= 0 && y.Handicap < 10),
                    Handicap_10_20 = x.Count(y => y.Handicap >= 10 && y.Handicap < 20),
                    Handicap_20_30 = x.Count(y => y.Handicap >= 20 && y.Handicap < 30),
                    Handicap_30_36 = x.Count(y => y.Handicap >= 30 && y.Handicap <= 36),
                    Handicap_37_53 = x.Count(y => y.Handicap >= 37 && y.Handicap <= 53),
                    Handicap_54 = x.Count(y => y.Handicap == 54)
                });

            _context.MembershipSnapshots.AddRange(snapshots);
        }
    }
}
