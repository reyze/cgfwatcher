﻿using System.Collections.Generic;

namespace CGFWatcher.Services
{
    public interface IImportParser
    {
        IList<ImportData> ParsePlayersHtml(string html);
    }
}
