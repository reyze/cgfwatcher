﻿using System;

namespace CGFWatcher.Services
{
    public interface IClockProvider
    {
        DateTime Now { get; }

        DateTime Today { get; }
    }
}
