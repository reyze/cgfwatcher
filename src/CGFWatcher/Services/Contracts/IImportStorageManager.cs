﻿using System.Collections.Generic;
using CGFWatcher.Database;

namespace CGFWatcher.Services
{
    public interface IImportStorageManager
    {
        ImportLog StoreImportData(IList<ImportData> data, ImportParameters parameters, WatcherContext context);
    }
}
