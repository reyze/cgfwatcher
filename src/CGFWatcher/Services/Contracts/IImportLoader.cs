﻿using System.Threading.Tasks;

namespace CGFWatcher.Services
{
    public interface IImportLoader
    {
        Task<string> LoadPlayersHtml(ImportParameters parameters);
    }
}
