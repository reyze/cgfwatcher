﻿using System.Threading.Tasks;
using CGFWatcher.Database;

namespace CGFWatcher.Services
{
    public interface IImportService
    {
        Task<ImportLog> ImportPlayers(ImportParameters parameters);
    }
}
