﻿using CGFWatcher.Database;

namespace CGFWatcher.Services
{
    public interface IDataRefreshService
    {
        DataRefreshLog RefreshPlayersData(DataRefreshParameters parameters);
    }
}
