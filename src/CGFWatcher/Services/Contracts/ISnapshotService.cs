﻿namespace CGFWatcher.Services
{
    public interface ISnapshotService
    {
        void CreateSnapshots();
    }
}
