﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace CGFWatcher.Services
{
    public class ImportLoader : IImportLoader
    {
        private const string _url = "http://www.cgf.cz/cz/cgf/subjekty-cgf/hraci";

        private string _playersHtml;

        public async Task<string> LoadPlayersHtml(ImportParameters parameters)
        {
            while (!await TryPostRequest(parameters))
            {
            }

            return _playersHtml;
        }

        private async Task<bool> TryPostRequest(ImportParameters parameters)
        {
            var postData = PreparePostData(parameters);

            try
            {
                var json = await ProcessWebRequest("POST", postData);
                var parsed = JObject.Parse(json);

                _playersHtml = parsed["html"].ToString();

                return true;
            }
            catch
            {
                return false;
            }
        }

        private async Task<string> ProcessWebRequest(string method, IDictionary<string, string> postData)
        {
            var cookies = new CookieContainer();
            var webRequest = WebRequest.Create(_url) as HttpWebRequest;

            webRequest.Method = method;
            webRequest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            webRequest.CookieContainer = cookies;
            webRequest.Accept = "*/*";
            webRequest.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36";
            
            var postString = string.Join("&", postData.Select(x => string.Concat(WebUtility.UrlEncode(x.Key), "=", WebUtility.UrlEncode(x.Value))));

            webRequest.Headers[HttpRequestHeader.Referer] = _url;
            webRequest.Headers[HttpRequestHeader.ContentLength] = postString.Length.ToString();

            using (var writer = new StreamWriter(await webRequest.GetRequestStreamAsync()))
            {
                writer.Write(postString);
            }

            string result;

            using (var response = await webRequest.GetResponseAsync())
            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                result = await reader.ReadToEndAsync();
            }

            return result;
        }

        private IDictionary<string, string> PreparePostData(ImportParameters parameters)
        {
            return new Dictionary<string, string>
            {
                { "page", "cgf/subjekty-cgf/hraci" },
                { "golferListFilter", "1" },
                { "firstName", "" },
                { "lastName", "" },
                { "memberNumber", "" },
                { "fulltext", "1" },
                { "sex", "" },
                { "hcpFrom", parameters.HandicapFrom },
                { "hcpTo", parameters.HandicapTo },
                { "ageCatFrom", "" },
                { "ageCatTo", "" },
                { "regions", "" },
            };
        }
    }
}
