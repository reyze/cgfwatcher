﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CGFWatcher.Database;
using  Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace CGFWatcher.Services
{
    public class DataRefreshService : IDataRefreshService
    {
        private WatcherContext _context;
        private IClockProvider _clockProvider;

        private IList<GolfClub> _golfClubs;
        private int _rowsCount;
        private int _newPlayersCount;
        private int _newMembershipsCount;
        private int _handicapChangesCount;

        private string _currentFullName;
        private decimal _currentHandicap;

        private IList<Membership> _detachedMemberships;

        public DataRefreshService(WatcherContext context, IClockProvider clockProvider)
        {
            _context = context;
            _clockProvider = clockProvider;
        }

        public DataRefreshLog RefreshPlayersData(DataRefreshParameters parameters)
        {
            InitializeService();

            var stopwatch = Stopwatch.StartNew();

            ProcessImportRows(parameters);

            stopwatch.Stop();

            var log = new DataRefreshLog
            {
                Refreshed = _clockProvider.Now,
                DurationInSeconds = (int)stopwatch.Elapsed.TotalSeconds,
                RowsCount = _rowsCount,
                NewPlayersCount = _newPlayersCount,
                NewMembershipsCount = _newMembershipsCount,
                HandicapChangesCount = _handicapChangesCount,
                Parameters = JsonConvert.SerializeObject(parameters)
            };

            _context.DataRefreshLogs.Add(log);
            _context.SaveChanges();

            return log;
        }

        private void InitializeService()
        {
            _golfClubs = _context.GolfClubs.ToList();
            _rowsCount = 0;
            _newPlayersCount = 0;
            _newMembershipsCount = 0;
            _handicapChangesCount = 0;
            _detachedMemberships = new List<Membership>();
        }

        private void ProcessImportRows(DataRefreshParameters parameters)
        {
            var rows = LoadRelevantImportRows(parameters);

            AddOrUpdatePlayers(rows, parameters);

            _context.SaveChanges();

            CleanupDetachedMemberships();
            CleanupDeletedMemberships(rows);

            _context.SaveChanges();
        }

        private void AddOrUpdatePlayers(IList<ImportRow> rows, DataRefreshParameters parameters)
        {
            var playersCache = LoadPlayersCache(parameters);
            var groupedPlayers = rows.Where(x => !x.IsDeleted).GroupBy(x => new { x.FullName, x.Handicap });

            groupedPlayers = ApplyParametersOnGroupedPlayers(groupedPlayers, parameters);

            foreach (var importRows in groupedPlayers)
            {
                _currentFullName = importRows.Key.FullName;
                _currentHandicap = importRows.Key.Handicap;

                IncreaseRowsCount(importRows);

                var registrationNumbers = importRows.Select(x => x.RegistrationNumber);

                if (HandleDetachedMemberships(importRows, registrationNumbers))
                {
                    continue;
                }

                var player = GetExistingPlayer(playersCache, registrationNumbers);

                if (player != null)
                {
                    ProcessExistingPlayer(player, importRows);
                }
                else
                {
                    ProcessNewPlayer(importRows);
                }
            }
        }

        private void CleanupDetachedMemberships()
        {
            _context.Memberships.RemoveRange(_detachedMemberships);
        }

        private void CleanupDeletedMemberships(IList<ImportRow> rows)
        {
            var deletedRegistrationNumbers = rows.Where(x => x.IsDeleted).Select(x => x.RegistrationNumber);

            var deletedMemberships = _context.Memberships
                .Where(x => deletedRegistrationNumbers.Contains(x.RegistrationNumber))
                .Where(x => x.IsDeleted == false)
                .ToList();

            foreach (var membership in deletedMemberships)
            {
                membership.IsDeleted = true;
                membership.Changed = _clockProvider.Now;
            }

            IncreaseRowsCount(deletedRegistrationNumbers);
        }

        private IList<ImportRow> LoadRelevantImportRows(DataRefreshParameters parameters)
        {
            return _context.ImportRows
                .Where(x => x.Handicap >= parameters.HandicapFrom.ParseHandicap())
                .Where(x => x.Handicap <= parameters.HandicapTo.ParseHandicap())
                .OrderBy(x => x.RegistrationNumber)
                .ToList();
        }

        private IList<Player> LoadPlayersCache(DataRefreshParameters parameters)
        {
            return _context.Players
                .Include(x => x.Memberships)
                .Where(x => x.Handicap >= (parameters.HandicapFrom.ParseHandicap() - 1))
                .Where(x => x.Handicap <= (parameters.HandicapTo.ParseHandicap() + 1))
                .ToList();
        }

        private IEnumerable<T> ApplyParametersOnGroupedPlayers<T>(IEnumerable<T> data, DataRefreshParameters parameters)
        {
            if (parameters.ItemsFrom.HasValue)
            {
                data = data.Skip(parameters.ItemsFrom.Value - 1);
            }

            if (parameters.ItemsTo.HasValue)
            {
                var start = 0;

                if (parameters.ItemsFrom.HasValue && parameters.ItemsFrom.Value > 0)
                {
                    start = parameters.ItemsFrom.Value - 1;
                }

                data = data.Take(parameters.ItemsTo.Value - start);
            }

            return data;
        }

        private void ProcessNewPlayer(IEnumerable<ImportRow> importRows)
        {
            var player = AddPlayer();

            foreach (var importRow in importRows)
            {
                var detachedMembership = _detachedMemberships.FirstOrDefault(x => x.RegistrationNumber == importRow.RegistrationNumber);

                if (detachedMembership == null)
                {
                    AddMembership(player, importRow);
                }
                else
                {
                    UpdateMembership(player, detachedMembership, importRow);

                    detachedMembership.Player = player;

                    _detachedMemberships.Remove(detachedMembership);
                }
            }
        }

        private void ProcessExistingPlayer(Player player, IEnumerable<ImportRow> importRows)
        {
            UpdatePlayer(player);

            HandleMissingMemberships(player, importRows);

            foreach (var importRow in importRows)
            {
                var existingMembership = GetExistingMembership(player, importRow);

                if (existingMembership == null)
                {
                    AddMembership(player, importRow);
                }
                else
                {
                    UpdateMembership(player, existingMembership, importRow);

                    existingMembership.PlayerID = player.ID;
                }
            }
        }

        private void HandleMissingMemberships(Player player, IEnumerable<ImportRow> importRows)
        {
            var presentMemberships = importRows.Select(x => x.RegistrationNumber);
            var missingMemberships = player.Memberships.Where(x => !presentMemberships.Contains(x.RegistrationNumber));

            foreach (var membership in missingMemberships)
            {
                if (_context.ImportRows.Any(x => x.RegistrationNumber == membership.RegistrationNumber && !x.IsDeleted))
                {
                    _detachedMemberships.Add(membership);
                }
                else
                {
                    membership.IsDeleted = true;
                    membership.Changed = _clockProvider.Now;
                }
            }
        }

        private bool HandleDetachedMemberships(IEnumerable<ImportRow> importRows, IEnumerable<string> registrationNumbers)
        {
            if (!_detachedMemberships.Any(x => registrationNumbers.Contains(x.RegistrationNumber)))
            {
                return false;
            }

            ProcessNewPlayer(importRows);

            return true;
        }

        private Player GetExistingPlayer(IList<Player> playersCache, IEnumerable<string> registrationNumbers)
        {
            var player = playersCache.FirstOrDefault(x => x.Memberships.Any(y => registrationNumbers.Contains(y.RegistrationNumber)));

            if (player == null)
            {
                player = _context.Players.Include(x => x.Memberships)
                    .FirstOrDefault(x => x.Memberships.Any(y => registrationNumbers.Contains(y.RegistrationNumber)));
            }

            if (player == null)
            {
                player = playersCache.FirstOrDefault(x => x.FullName == _currentFullName && x.Handicap == _currentHandicap);
            }

            if (player == null)
            {
                player = _context.Players.Include(x => x.Memberships)
                    .FirstOrDefault(x => x.FullName == _currentFullName && x.Handicap == _currentHandicap);
            }

            return player;
        }

        private Player AddPlayer()
        {
            _newPlayersCount++;

            var player = new Player
            {
                FullName = _currentFullName,
                Handicap = _currentHandicap
            };

            _context.Players.Add(player);

            _context.HandicapChanges.Add(new HandicapChange
            {
                Player = player,
                NewHandicap = _currentHandicap,
                Changed = _clockProvider.Now
            });

            return player;
        }

        private void UpdatePlayer(Player player)
        {
            player.FullName = _currentFullName;

            if (player.Handicap == _currentHandicap)
            {
                return;
            }

            _handicapChangesCount++;

            player.Handicap = _currentHandicap;

            _context.HandicapChanges.Add(new HandicapChange
            {
                Player = player,
                NewHandicap = _currentHandicap,
                Changed = _clockProvider.Now
            });
        }


        private Membership GetExistingMembership(Player player, ImportRow importRow)
        {
            var existingMembership = player.Memberships.FirstOrDefault(x => x.RegistrationNumber == importRow.RegistrationNumber);

            if (existingMembership == null)
            {
                existingMembership = _context.Memberships.FirstOrDefault(x => x.RegistrationNumber == importRow.RegistrationNumber);
            }

            return existingMembership;
        }

        private void AddMembership(Player player, ImportRow importRow)
        {
            _newMembershipsCount++;

            _context.Memberships.Add(new Membership
            {
                Player = player,
                GolfClub = GetGolfClub(importRow.Club, importRow.RegistrationNumber),
                RegistrationNumber = importRow.RegistrationNumber,
                IsHome = importRow.IsHome,
                IsBlocked = importRow.IsBlocked,
                Created = _clockProvider.Now
            });
        }

        private void UpdateMembership(Player player, Membership membership, ImportRow importRow)
        {
            var changed = false;

            if (membership.IsHome != importRow.IsHome)
            {
                membership.IsHome = importRow.IsHome;
                changed = true;
            }

            if (membership.IsBlocked != importRow.IsBlocked)
            {
                membership.IsBlocked = importRow.IsBlocked;
                changed = true;
            }

            if (membership.IsDeleted)
            {
                membership.IsDeleted = false;
                changed = true;
            }

            if (changed)
            {
                membership.Changed = _clockProvider.Now;
            }
        }

        private GolfClub GetGolfClub(string name, string registrationNumber)
        {
            var club = _golfClubs.FirstOrDefault(x => x.Prefix == ParseGolfClubPrefix(registrationNumber));

            if (club != null)
            {
                if (club.Name != name)
                {
                    club.Name = name;
                }

                return club;
            }

            club = new GolfClub
            {
                Name = name,
                Prefix = ParseGolfClubPrefix(registrationNumber)
            };

            _context.GolfClubs.Add(club);
            _golfClubs.Add(club);

            return club;
        }

        private string ParseGolfClubPrefix(string registrationNumber)
        {
            return registrationNumber.Substring(0, 3);
        }

        private void IncreaseRowsCount<T>(IEnumerable<T> data)
        {
            _rowsCount += data.Count();
        }
    }
}
