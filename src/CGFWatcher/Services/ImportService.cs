﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using CGFWatcher.Database;
using Newtonsoft.Json;

namespace CGFWatcher.Services
{
    public class ImportService : IImportService
    {
        private WatcherContext _context;
        private IImportLoader _importLoader;
        private IImportParser _importParser;
        private IImportStorageManager _importStorageManager;
        private IClockProvider _clockProvider;

        private ImportLog _currentImportLog;

        public ImportService(WatcherContext context, IImportLoader importLoader, IImportParser importParser, IImportStorageManager importStorageManager, IClockProvider clockProvider)
        {
            _context = context;
            _importLoader = importLoader;
            _importParser = importParser;
            _importStorageManager = importStorageManager;
            _clockProvider = clockProvider;
        }

        public async Task<ImportLog> ImportPlayers(ImportParameters parameters)
        {
            _currentImportLog = new ImportLog();

            var rows = await LoadAndParsePlayersHtml(parameters);

            StoreImportedData(rows, parameters);

            SaveImportLog(parameters);

            return _currentImportLog;
        }

        private async Task<IList<ImportData>> LoadAndParsePlayersHtml(ImportParameters parameters)
        {
            var stopwatch = Stopwatch.StartNew();

            var html = await _importLoader.LoadPlayersHtml(parameters);
            var rows = _importParser.ParsePlayersHtml(html);

            stopwatch.Stop();

            _currentImportLog.LoadingDurationInSeconds = (int)stopwatch.Elapsed.TotalSeconds;

            return rows;
        }

        private void StoreImportedData(IList<ImportData> rows, ImportParameters parameters)
        {
            var stopwatch = Stopwatch.StartNew();

            var log = _importStorageManager.StoreImportData(rows, parameters, _context);

            stopwatch.Stop();

            log.StorageDurationInSeconds = (int)stopwatch.Elapsed.TotalSeconds;
            log.LoadingDurationInSeconds = _currentImportLog.LoadingDurationInSeconds;

            _currentImportLog = log;
        }

        private void SaveImportLog(ImportParameters parameters)
        {
            _currentImportLog.Parameters = JsonConvert.SerializeObject(parameters);
            _currentImportLog.Imported = _clockProvider.Now;

            _context.ImportLogs.Add(_currentImportLog);
            _context.SaveChanges();
        }
    }
}
