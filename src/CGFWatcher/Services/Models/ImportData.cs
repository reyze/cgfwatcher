﻿namespace CGFWatcher.Services
{
    public class ImportData
    {
        public string FullName { get; set; }
        public string RegistrationNumber { get; set; }
        public string Club { get; set; }
        public decimal Handicap { get; set; }
        public bool IsHome { get; set; }
        public bool IsBlocked { get; set; }
    }
}
