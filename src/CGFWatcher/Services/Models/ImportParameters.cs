﻿using System.ComponentModel.DataAnnotations;

namespace CGFWatcher.Services
{
    public class ImportParameters
    {
        [RegularExpression("^\\+?[0-9]*(,[0-9]+)?$")]
        public string HandicapFrom { get; set; }

        [RegularExpression("^\\+?[0-9]*(,[0-9]+)?$")]
        public string HandicapTo { get; set; }
    }
}
