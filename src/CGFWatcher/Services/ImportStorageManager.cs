﻿using System.Collections.Generic;
using System.Linq;
using CGFWatcher.Database;

namespace CGFWatcher.Services
{
    public class ImportStorageManager : IImportStorageManager
    {
        private IClockProvider _clockProvider;

        private WatcherContext _context;
        private IList<string> _updatedRows;
        private IList<ImportRow> _relevantRows;

        public ImportStorageManager(IClockProvider clockProvider)
        {
            _clockProvider = clockProvider;
        }

        public ImportLog StoreImportData(IList<ImportData> data, ImportParameters parameters, WatcherContext context)
        {
            InitializeStorage(context, parameters);

            ProcessData(data);

            return new ImportLog
            {
                RowsCount = data.Count
            };
        }

        private void InitializeStorage(WatcherContext context, ImportParameters parameters)
        {
            _context = context;
            _updatedRows = new List<string>();

            _relevantRows = _context.ImportRows
                .Where(x => x.Handicap >= parameters.HandicapFrom.ParseHandicap())
                .Where(x => x.Handicap <= parameters.HandicapTo.ParseHandicap())
                .ToList();
        }

        private void ProcessData(IList<ImportData> data)
        {
            foreach (var row in data)
            {
                ProcessRow(row);
            }

            DeleteUnusedRows();

            _context.SaveChanges();
        }

        private void ProcessRow(ImportData row)
        {
            var existingRow = _relevantRows.FirstOrDefault(x => x.RegistrationNumber == row.RegistrationNumber);

            if (existingRow == null)
            {
                existingRow = _context.ImportRows.FirstOrDefault(x => x.RegistrationNumber == row.RegistrationNumber);
            }

            if (existingRow != null)
            {
                existingRow.FullName = row.FullName;
                existingRow.Club = row.Club;
                existingRow.Handicap = row.Handicap;
                existingRow.IsHome = row.IsHome;
                existingRow.IsBlocked = row.IsBlocked;
                existingRow.IsDeleted = false;
                existingRow.Changed = _clockProvider.Now;

                _updatedRows.Add(row.RegistrationNumber);
            }
            else
            {
                _context.Add(new ImportRow
                {
                    RegistrationNumber = row.RegistrationNumber,
                    FullName = row.FullName,
                    Club = row.Club,
                    Handicap = row.Handicap,
                    IsHome = row.IsHome,
                    IsBlocked = row.IsBlocked,
                    Changed = _clockProvider.Now
                });
            }
        }

        private void DeleteUnusedRows()
        {
            var rowsForDeletion = _relevantRows.Where(x => !_updatedRows.Contains(x.RegistrationNumber));

            foreach (var row in rowsForDeletion)
            {
                row.IsDeleted = true;
                row.Changed = _clockProvider.Now;
            }
        }
    }
}
