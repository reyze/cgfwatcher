﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using HtmlAgilityPack;

namespace CGFWatcher.Services
{
    public class ImportParser : IImportParser
    {
        public IList<ImportData> ParsePlayersHtml(string html)
        {
            var list = new List<ImportData>();
            var doc = new HtmlDocument();

            doc.LoadHtml(html);
            
            var table = doc.DocumentNode.SelectNodes("//table");

            if (table == null)
            {
                return list;
            }

            var rows = table.FirstOrDefault().SelectNodes(".//tr");

            if (rows == null)
            {
                return list;
            }

            var processedNumbers = new List<string>();

            foreach (var row in rows.Skip(1))
            {
                var cells = row.SelectNodes(".//td").ToArray();
                var registrationNumber = GetCellContent(cells[1]);

                if (processedNumbers.Contains(registrationNumber))
                {
                    continue;
                }

                list.Add(new ImportData
                {
                    FullName = GetCellContent(cells[0]),
                    RegistrationNumber = registrationNumber,
                    Club = GetCellContent(cells[2]),
                    Handicap = ParseHandicap(cells[4]),
                    IsHome = (cells[3].InnerText == "Ano"),
                    IsBlocked = (cells[6].InnerText == "Ano")
                });

                processedNumbers.Add(registrationNumber);
            }

            return list;
        }

        private string GetCellContent(HtmlNode node)
        {
            return WebUtility.HtmlDecode(node.InnerText.Trim());
        }

        private decimal ParseHandicap(HtmlNode node)
        {
            var token = node.InnerText;
            var hcp = decimal.Parse(token);

            if (token.StartsWith("+"))
            {
                return -1 * hcp;
            }

            return hcp;
        }
    }
}
