﻿using CGFWatcher.Database;
using CGFWatcher.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace CGFWatcher
{
    public static class ServiceCollectionExtensions
    {
        public static void AddCGFWatcher(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<WatcherContext>(options => options.UseSqlServer(connectionString));

            services.AddTransient<IClockProvider, ClockProvider>();
            services.AddTransient<IImportService, ImportService>();
            services.AddTransient<IImportLoader, ImportLoader>();
            services.AddTransient<IImportParser, ImportParser>();
            services.AddTransient<IImportStorageManager, ImportStorageManager>();
            services.AddTransient<IDataRefreshService, DataRefreshService>();
            services.AddTransient<ISnapshotService, SnapshotService>();
        }
    }
}
