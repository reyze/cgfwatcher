using System;
using System.Collections.Generic;
using  Microsoft.EntityFrameworkCore.Migrations;
using  Microsoft.EntityFrameworkCore.Metadata;

namespace CGFWatcher.Migrations
{
    public partial class Snapshots : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_HandicapChange_Player_PlayerID", schema: "CGF", table: "HandicapChange");
            migrationBuilder.DropForeignKey(name: "FK_Membership_GolfClub_GolfClubID", schema: "CGF", table: "Membership");
            migrationBuilder.DropForeignKey(name: "FK_Membership_Player_PlayerID", schema: "CGF", table: "Membership");
            migrationBuilder.CreateTable(
                name: "MembershipSnapshot",
                schema: "CGF",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    GolfClubID = table.Column<int>(nullable: false),
                    Handicap_0_10 = table.Column<int>(nullable: false),
                    Handicap_10_20 = table.Column<int>(nullable: false),
                    Handicap_20_30 = table.Column<int>(nullable: false),
                    Handicap_30_36 = table.Column<int>(nullable: false),
                    Handicap_37_53 = table.Column<int>(nullable: false),
                    Handicap_54 = table.Column<int>(nullable: false),
                    Handicap_lt_0 = table.Column<int>(nullable: false),
                    Total = table.Column<int>(nullable: false),
                    TotalBlocked = table.Column<int>(nullable: false),
                    TotalHome = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MembershipSnapshot", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MembershipSnapshot_GolfClub_GolfClubID",
                        column: x => x.GolfClubID,
                        principalSchema: "CGF",
                        principalTable: "GolfClub",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "PlayerSnapshot",
                schema: "CGF",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Handicap_0_10 = table.Column<int>(nullable: false),
                    Handicap_10_20 = table.Column<int>(nullable: false),
                    Handicap_20_30 = table.Column<int>(nullable: false),
                    Handicap_30_36 = table.Column<int>(nullable: false),
                    Handicap_37_53 = table.Column<int>(nullable: false),
                    Handicap_54 = table.Column<int>(nullable: false),
                    Handicap_lt_0 = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerSnapshot", x => x.ID);
                });
            migrationBuilder.AddForeignKey(
                name: "FK_HandicapChange_Player_PlayerID",
                schema: "CGF",
                table: "HandicapChange",
                column: "PlayerID",
                principalSchema: "CGF",
                principalTable: "Player",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Membership_GolfClub_GolfClubID",
                schema: "CGF",
                table: "Membership",
                column: "GolfClubID",
                principalSchema: "CGF",
                principalTable: "GolfClub",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Membership_Player_PlayerID",
                schema: "CGF",
                table: "Membership",
                column: "PlayerID",
                principalSchema: "CGF",
                principalTable: "Player",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_HandicapChange_Player_PlayerID", schema: "CGF", table: "HandicapChange");
            migrationBuilder.DropForeignKey(name: "FK_Membership_GolfClub_GolfClubID", schema: "CGF", table: "Membership");
            migrationBuilder.DropForeignKey(name: "FK_Membership_Player_PlayerID", schema: "CGF", table: "Membership");
            migrationBuilder.DropTable(name: "MembershipSnapshot", schema: "CGF");
            migrationBuilder.DropTable(name: "PlayerSnapshot", schema: "CGF");
            migrationBuilder.AddForeignKey(
                name: "FK_HandicapChange_Player_PlayerID",
                schema: "CGF",
                table: "HandicapChange",
                column: "PlayerID",
                principalSchema: "CGF",
                principalTable: "Player",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Membership_GolfClub_GolfClubID",
                schema: "CGF",
                table: "Membership",
                column: "GolfClubID",
                principalSchema: "CGF",
                principalTable: "GolfClub",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Membership_Player_PlayerID",
                schema: "CGF",
                table: "Membership",
                column: "PlayerID",
                principalSchema: "CGF",
                principalTable: "Player",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
