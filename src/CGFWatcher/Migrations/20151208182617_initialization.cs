using System;
using System.Collections.Generic;
using  Microsoft.EntityFrameworkCore.Migrations;
using  Microsoft.EntityFrameworkCore.Metadata;

namespace CGFWatcher.Migrations
{
    public partial class Initialization : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema("CGF");
            migrationBuilder.CreateTable(
                name: "DataRefreshLog",
                schema: "CGF",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DurationInSeconds = table.Column<int>(nullable: false),
                    HandicapChangesCount = table.Column<int>(nullable: false),
                    NewMembershipsCount = table.Column<int>(nullable: false),
                    NewPlayersCount = table.Column<int>(nullable: false),
                    Parameters = table.Column<string>(nullable: true),
                    Refreshed = table.Column<DateTime>(nullable: false),
                    RowsCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataRefreshLog", x => x.ID);
                });
            migrationBuilder.CreateTable(
                name: "GolfClub",
                schema: "CGF",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Prefix = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GolfClub", x => x.ID);
                });
            migrationBuilder.CreateTable(
                name: "ImportLog",
                schema: "CGF",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Imported = table.Column<DateTime>(nullable: false),
                    LoadingDurationInSeconds = table.Column<int>(nullable: false),
                    Parameters = table.Column<string>(nullable: true),
                    RowsCount = table.Column<int>(nullable: false),
                    StorageDurationInSeconds = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImportLog", x => x.ID);
                });
            migrationBuilder.CreateTable(
                name: "ImportRow",
                schema: "CGF",
                columns: table => new
                {
                    RegistrationNumber = table.Column<string>(nullable: false),
                    Changed = table.Column<DateTime>(nullable: false),
                    Club = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    Handicap = table.Column<decimal>(nullable: false),
                    IsBlocked = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsHome = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImportRow", x => x.RegistrationNumber);
                });
            migrationBuilder.CreateTable(
                name: "Player",
                schema: "CGF",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<DateTime>(nullable: true),
                    FullName = table.Column<string>(nullable: false),
                    Handicap = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Player", x => x.ID);
                });
            migrationBuilder.CreateTable(
                name: "HandicapChange",
                schema: "CGF",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Changed = table.Column<DateTime>(nullable: false),
                    NewHandicap = table.Column<decimal>(nullable: false),
                    PlayerID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HandicapChange", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HandicapChange_Player_PlayerID",
                        column: x => x.PlayerID,
                        principalSchema: "CGF",
                        principalTable: "Player",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "Membership",
                schema: "CGF",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Changed = table.Column<DateTime>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    GolfClubID = table.Column<int>(nullable: false),
                    IsBlocked = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsHome = table.Column<bool>(nullable: false),
                    PlayerID = table.Column<int>(nullable: false),
                    RegistrationNumber = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Membership", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Membership_GolfClub_GolfClubID",
                        column: x => x.GolfClubID,
                        principalSchema: "CGF",
                        principalTable: "GolfClub",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Membership_Player_PlayerID",
                        column: x => x.PlayerID,
                        principalSchema: "CGF",
                        principalTable: "Player",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateIndex(
                name: "IX_Membership_RegistrationNumber",
                schema: "CGF",
                table: "Membership",
                column: "RegistrationNumber",
                unique: true);
            migrationBuilder.CreateIndex(
                name: "IX_Player_FullName",
                schema: "CGF",
                table: "Player",
                column: "FullName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(name: "DataRefreshLog", schema: "CGF");
            migrationBuilder.DropTable(name: "HandicapChange", schema: "CGF");
            migrationBuilder.DropTable(name: "ImportLog", schema: "CGF");
            migrationBuilder.DropTable(name: "ImportRow", schema: "CGF");
            migrationBuilder.DropTable(name: "Membership", schema: "CGF");
            migrationBuilder.DropTable(name: "GolfClub", schema: "CGF");
            migrationBuilder.DropTable(name: "Player", schema: "CGF");
        }
    }
}
