using System;
using System.Collections.Generic;
using  Microsoft.EntityFrameworkCore.Migrations;

namespace CGFWatcher.Migrations
{
    public partial class PlayerCouldNotBeDeleted : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_HandicapChange_Player_PlayerID", schema: "CGF", table: "HandicapChange");
            migrationBuilder.DropForeignKey(name: "FK_Membership_GolfClub_GolfClubID", schema: "CGF", table: "Membership");
            migrationBuilder.DropForeignKey(name: "FK_Membership_Player_PlayerID", schema: "CGF", table: "Membership");
            migrationBuilder.DropColumn(name: "Deleted", schema: "CGF", table: "Player");
            migrationBuilder.AddForeignKey(
                name: "FK_HandicapChange_Player_PlayerID",
                schema: "CGF",
                table: "HandicapChange",
                column: "PlayerID",
                principalSchema: "CGF",
                principalTable: "Player",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Membership_GolfClub_GolfClubID",
                schema: "CGF",
                table: "Membership",
                column: "GolfClubID",
                principalSchema: "CGF",
                principalTable: "GolfClub",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Membership_Player_PlayerID",
                schema: "CGF",
                table: "Membership",
                column: "PlayerID",
                principalSchema: "CGF",
                principalTable: "Player",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_HandicapChange_Player_PlayerID", schema: "CGF", table: "HandicapChange");
            migrationBuilder.DropForeignKey(name: "FK_Membership_GolfClub_GolfClubID", schema: "CGF", table: "Membership");
            migrationBuilder.DropForeignKey(name: "FK_Membership_Player_PlayerID", schema: "CGF", table: "Membership");
            migrationBuilder.AddColumn<DateTime>(
                name: "Deleted",
                schema: "CGF",
                table: "Player",
                nullable: true);
            migrationBuilder.AddForeignKey(
                name: "FK_HandicapChange_Player_PlayerID",
                schema: "CGF",
                table: "HandicapChange",
                column: "PlayerID",
                principalSchema: "CGF",
                principalTable: "Player",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Membership_GolfClub_GolfClubID",
                schema: "CGF",
                table: "Membership",
                column: "GolfClubID",
                principalSchema: "CGF",
                principalTable: "GolfClub",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Membership_Player_PlayerID",
                schema: "CGF",
                table: "Membership",
                column: "PlayerID",
                principalSchema: "CGF",
                principalTable: "Player",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
