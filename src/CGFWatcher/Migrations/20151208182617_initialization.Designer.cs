using System;
using  Microsoft.EntityFrameworkCore;
using  Microsoft.EntityFrameworkCore.Infrastructure;
using  Microsoft.EntityFrameworkCore.Metadata;
using  Microsoft.EntityFrameworkCore.Migrations;
using CGFWatcher.Database;

namespace CGFWatcher.Migrations
{
    [DbContext(typeof(WatcherContext))]
    [Migration("20151208182617_Initialization")]
    partial class Initialization
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("Relational:DefaultSchema", "CGF")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CGFWatcher.Database.DataRefreshLog", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("DurationInSeconds");

                    b.Property<int>("HandicapChangesCount");

                    b.Property<int>("NewMembershipsCount");

                    b.Property<int>("NewPlayersCount");

                    b.Property<string>("Parameters")
                        .HasAnnotation("MaxLength", 500);

                    b.Property<DateTime>("Refreshed");

                    b.Property<int>("RowsCount");

                    b.HasKey("ID");
                });

            modelBuilder.Entity("CGFWatcher.Database.GolfClub", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("Prefix")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 3);

                    b.HasKey("ID");
                });

            modelBuilder.Entity("CGFWatcher.Database.HandicapChange", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Changed");

                    b.Property<decimal>("NewHandicap");

                    b.Property<int>("PlayerID");

                    b.HasKey("ID");
                });

            modelBuilder.Entity("CGFWatcher.Database.ImportLog", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Imported");

                    b.Property<int>("LoadingDurationInSeconds");

                    b.Property<string>("Parameters")
                        .HasAnnotation("MaxLength", 500);

                    b.Property<int>("RowsCount");

                    b.Property<int>("StorageDurationInSeconds");

                    b.HasKey("ID");
                });

            modelBuilder.Entity("CGFWatcher.Database.ImportRow", b =>
                {
                    b.Property<string>("RegistrationNumber")
                        .HasAnnotation("MaxLength", 7);

                    b.Property<DateTime>("Changed");

                    b.Property<string>("Club")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("FullName")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<decimal>("Handicap");

                    b.Property<bool>("IsBlocked");

                    b.Property<bool>("IsDeleted");

                    b.Property<bool>("IsHome");

                    b.HasKey("RegistrationNumber");
                });

            modelBuilder.Entity("CGFWatcher.Database.Membership", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("Changed");

                    b.Property<DateTime>("Created");

                    b.Property<int>("GolfClubID");

                    b.Property<bool>("IsBlocked");

                    b.Property<bool>("IsDeleted");

                    b.Property<bool>("IsHome");

                    b.Property<int>("PlayerID");

                    b.Property<string>("RegistrationNumber")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 7);

                    b.HasKey("ID");

                    b.HasIndex("RegistrationNumber")
                        .IsUnique();
                });

            modelBuilder.Entity("CGFWatcher.Database.Player", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("Deleted");

                    b.Property<string>("FullName")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<decimal>("Handicap");

                    b.HasKey("ID");

                    b.HasIndex("FullName");
                });

            modelBuilder.Entity("CGFWatcher.Database.HandicapChange", b =>
                {
                    b.HasOne("CGFWatcher.Database.Player")
                        .WithMany()
                        .HasForeignKey("PlayerID");
                });

            modelBuilder.Entity("CGFWatcher.Database.Membership", b =>
                {
                    b.HasOne("CGFWatcher.Database.GolfClub")
                        .WithMany()
                        .HasForeignKey("GolfClubID");

                    b.HasOne("CGFWatcher.Database.Player")
                        .WithMany()
                        .HasForeignKey("PlayerID");
                });
        }
    }
}
