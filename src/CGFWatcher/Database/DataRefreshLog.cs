﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CGFWatcher.Database
{
    public class DataRefreshLog
    {
        public int ID { get; set; }
        public DateTime Refreshed { get; set; }

        public int DurationInSeconds { get; set; }

        public int RowsCount { get; set; }
        public int NewPlayersCount { get; set; }
        public int NewMembershipsCount { get; set; }
        public int HandicapChangesCount { get; set; }

        [MaxLength(500)]
        public string Parameters { get; set; }
    }
}
