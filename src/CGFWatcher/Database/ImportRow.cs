﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CGFWatcher.Database
{
    public class ImportRow
    {
        [MaxLength(7)]
        public string RegistrationNumber { get; set; }

        [MaxLength(100)]
        public string FullName { get; set; }

        [MaxLength(100)]
        public string Club { get; set; }

        public decimal Handicap { get; set; }
        public bool IsHome { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime Changed { get; set; }
    }
}
