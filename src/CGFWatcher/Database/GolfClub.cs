﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CGFWatcher.Database
{
    public class GolfClub
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(3)]
        public string Prefix { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public List<Membership> Memberships { get; set; }
    }
}
