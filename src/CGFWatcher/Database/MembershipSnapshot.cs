﻿using System;

namespace CGFWatcher.Database
{
    public class MembershipSnapshot
    {
        public int ID { get; set; }
        public int GolfClubID { get; set; }
        public DateTime Created { get; set; }

        public int Total { get; set; }
        public int TotalHome { get; set; }
        public int TotalBlocked { get; set; }

        public int Handicap_lt_0 { get; set; }
        public int Handicap_0_10 { get; set; }
        public int Handicap_10_20 { get; set; }
        public int Handicap_20_30 { get; set; }
        public int Handicap_30_36 { get; set; }
        public int Handicap_37_53 { get; set; }
        public int Handicap_54 { get; set; }

        public GolfClub GolfClub { get; set; }
    }
}
