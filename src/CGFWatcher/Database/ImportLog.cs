﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CGFWatcher.Database
{
    public class ImportLog
    {
        public int ID { get; set; }
        public DateTime Imported { get; set; }

        public int LoadingDurationInSeconds { get; set; }
        public int StorageDurationInSeconds { get; set; }

        public int RowsCount { get; set; }

        [MaxLength(500)]
        public string Parameters { get; set; }
    }
}
