﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace CGFWatcher.Database
{
    public class WatcherContext : DbContext
    {
        public WatcherContext()
        {
        }

        public WatcherContext(DbContextOptions options)
            : base(options)
        {
        }
        
        public DbSet<GolfClub> GolfClubs { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Membership> Memberships { get; set; }
        public DbSet<HandicapChange> HandicapChanges { get; set; }
        public DbSet<ImportRow> ImportRows { get; set; }
        public DbSet<ImportLog> ImportLogs { get; set; }
        public DbSet<DataRefreshLog> DataRefreshLogs { get; set; }
        public DbSet<PlayerSnapshot> PlayerSnapshots { get; set; }
        public DbSet<MembershipSnapshot> MembershipSnapshots { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("CGF");
            modelBuilder.Entity<Player>().HasIndex(x => x.FullName);
            modelBuilder.Entity<Membership>().HasIndex(x => x.RegistrationNumber).IsUnique();
            modelBuilder.Entity<ImportRow>().HasKey(x => x.RegistrationNumber);
            modelBuilder.Entity<PlayerSnapshot>().Ignore(x => x.Total);

            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                entity.Relational().TableName = entity.DisplayName();
            }
        }
    }
}
