﻿using System;
using System.Linq;

namespace CGFWatcher.Database
{
    public class PlayerSnapshot
    {
        public int ID { get; set; }
        public DateTime Created { get; set; }

        public int Handicap_lt_0 { get; set; }
        public int Handicap_0_10 { get; set; }
        public int Handicap_10_20 { get; set; }
        public int Handicap_20_30 { get; set; }
        public int Handicap_30_36 { get; set; }
        public int Handicap_37_53 { get; set; }
        public int Handicap_54 { get; set; }

        public int Total
        {
            get
            {
                var data = new[]
                {
                    Handicap_lt_0,
                    Handicap_0_10,
                    Handicap_10_20,
                    Handicap_20_30,
                    Handicap_30_36,
                    Handicap_37_53,
                    Handicap_54
                };

                return data.Sum();
            }
        }
    }
}
