﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CGFWatcher.Database
{
    public class Membership
    {
        public int ID { get; set; }
        public int PlayerID { get; set; }
        public int GolfClubID { get; set; }

        [Required]
        [MaxLength(7)]
        public string RegistrationNumber { get; set; }

        public bool IsHome { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsDeleted { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Changed { get; set; }

        public Player Player { get; set; }
        public GolfClub GolfClub { get; set; }
    }
}
