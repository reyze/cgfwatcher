﻿using System;

namespace CGFWatcher.Database
{
    public class HandicapChange
    {
        public int ID { get; set; }
        public int PlayerID { get; set; }

        public decimal NewHandicap { get; set; }
        public DateTime Changed { get; set; }

        public Player Player { get; set; }
    }
}
