﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CGFWatcher.Database
{
    public class Player
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(100)]
        public string FullName { get; set; }

        public decimal Handicap { get; set; }

        public List<Membership> Memberships { get; set; }
        public List<HandicapChange> HandicapChanges { get; set; }
    }
}
