﻿using System.Linq;
using System.Threading.Tasks;
using CGFWatcher.Database;
using CGFWatcher.Web.Models;
using CGFWatcher.Services;
using Microsoft.AspNetCore.Mvc;

namespace CGFWatcher.Web.Controllers
{
    public class ImportController : Controller
    {
        private WatcherContext _context;
        private IImportService _importService;

        public ImportController(WatcherContext context, IImportService importService)
        {
            _context = context;
            _importService = importService;
        }

        public IActionResult Index()
        {
            return View(new ImportModel
            {
                ImportRowsCount = _context.ImportRows.Count()
            });
        }

        [HttpPost]
        public async Task<IActionResult> Run(ImportModel model)
        {
            if (ModelState.IsValid)
            {
                await _importService.ImportPlayers(model.ImportParameters);
            }

            return RedirectToAction("Index");
        }
    }
}
