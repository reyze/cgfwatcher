﻿using System.Linq;
using System.Threading.Tasks;
using CGFWatcher.Database;
using CGFWatcher.Web.Models;
using CGFWatcher.Services;
using Microsoft.AspNetCore.Mvc;

namespace CGFWatcher.Web.Controllers
{
    public class SnapshotController : Controller
    {
        private WatcherContext _context;
        private ISnapshotService _snapshotService;

        public SnapshotController(WatcherContext context, ISnapshotService snapshotService)
        {
            _context = context;
            _snapshotService = snapshotService;
        }

        public IActionResult Index()
        {
            return View(new SnapshotModel
            {
                PlayerSnapshotsCount = _context.PlayerSnapshots.Count(),
                MembershipSnapshotsCount = _context.MembershipSnapshots.Count()
            });
        }

        [HttpPost]
        public IActionResult Create()
        {
            _snapshotService.CreateSnapshots();

            return RedirectToAction("Index");
        }
    }
}
