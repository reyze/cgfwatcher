﻿using System.Collections.Generic;
using System.Linq;
using CGFWatcher.Database;
using CGFWatcher.Web.Models;
using CGFWatcher.Web.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CGFWatcher.Web.Controllers
{
    public class PlayerController : Controller
    {
        private IPlayerRepository _playerRepository;
        private IGolfClubRepository _golfClubRepository;

        public PlayerController(IPlayerRepository playerRepository, IGolfClubRepository golfClubRepository)
        {
            _playerRepository = playerRepository;
            _golfClubRepository = golfClubRepository;
        }

        public IActionResult Index()
        {
            var golfClubs = _golfClubRepository.GetGolfClubs();

            return View(new PlayerModel
            {
                GolfClubs = CreateGolfClubsSelectList(golfClubs)
            });
        }

        [HttpPost]
        public IActionResult Listing(PlayerModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            return View(new PlayerListingModel
            {
                Players = _playerRepository.GetPlayers(model.Filter)
            });
        }

        public IActionResult Detail(int id)
        {
            return View(new PlayerDetailModel
            {
                PlayerData = _playerRepository.GetPlayerData(id)
            });
        }

        private IEnumerable<SelectListItem> CreateGolfClubsSelectList(IList<GolfClub> golfClubs)
        {
            var emptyList = new[]
            {
                new SelectListItem { Value = null, Text = null }
            };

            var clubList = golfClubs.Select(x => new SelectListItem
            {
                Value = x.ID.ToString(),
                Text = string.Format("{0} ({1})", x.Name, x.Prefix)
            });
            
            return emptyList.Union(clubList);
        }
    }
}
