﻿using System.Linq;
using CGFWatcher.Database;
using CGFWatcher.Web.Models;
using CGFWatcher.Services;
using Microsoft.AspNetCore.Mvc;

namespace CGFWatcher.Web.Controllers
{
    public class DataRefreshController : Controller
    {
        private WatcherContext _context;
        private IDataRefreshService _dataRefreshService;

        public DataRefreshController(WatcherContext context, IDataRefreshService dataRefreshService)
        {
            _context = context;
            _dataRefreshService = dataRefreshService;
        }

        public IActionResult Index()
        {
            return View(new DataRefreshModel
            {
                PlayersCount = _context.Players.Count(),
                MembershipsCount = _context.Memberships.Count()
            });
        }

        [HttpPost]
        public IActionResult Run(DataRefreshModel model)
        {
            if (ModelState.IsValid)
            {
                _dataRefreshService.RefreshPlayersData(model.DataRefreshParameters);
            }

            return RedirectToAction("Index");
        }
    }
}
