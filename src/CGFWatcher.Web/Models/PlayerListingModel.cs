﻿using System.Collections.Generic;
using CGFWatcher.Database;

namespace CGFWatcher.Web.Models
{
    public class PlayerListingModel
    {
        public IList<Player> Players { get; set; }
    }
}
