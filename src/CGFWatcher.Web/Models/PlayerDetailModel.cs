﻿using CGFWatcher.Web.Repositories;

namespace CGFWatcher.Web.Models
{
    public class PlayerDetailModel
    {
        public PlayerData PlayerData { get; set; }
    }
}
