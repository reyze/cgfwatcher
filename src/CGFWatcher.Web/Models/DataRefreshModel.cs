﻿using CGFWatcher.Services;

namespace CGFWatcher.Web.Models
{
    public class DataRefreshModel
    {
        public int PlayersCount { get; set; }
        public int MembershipsCount { get; set; }
        public DataRefreshParameters DataRefreshParameters { get; set; }
    }
}
