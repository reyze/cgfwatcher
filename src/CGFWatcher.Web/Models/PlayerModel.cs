﻿using System.Collections.Generic;
using CGFWatcher.Web.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CGFWatcher.Web.Models
{
    public class PlayerModel
    {
        public PlayerFilter Filter { get; set; }
        public IEnumerable<SelectListItem> GolfClubs { get; set; }
    }
}
