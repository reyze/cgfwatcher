﻿using CGFWatcher.Services;

namespace CGFWatcher.Web.Models
{
    public class ImportModel
    {
        public int ImportRowsCount { get; set; }
        public ImportParameters ImportParameters { get; set; }
    }
}
