﻿namespace CGFWatcher.Web.Models
{
    public class SnapshotModel
    {
        public int PlayerSnapshotsCount { get; set; }
        public int MembershipSnapshotsCount { get; set; }
    }
}
