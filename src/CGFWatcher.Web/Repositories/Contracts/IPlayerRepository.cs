﻿using System.Collections.Generic;
using CGFWatcher.Database;

namespace CGFWatcher.Web.Repositories
{
    public interface IPlayerRepository
    {
        IList<Player> GetPlayers(PlayerFilter filter);

        PlayerData GetPlayerData(int id);
    }
}
