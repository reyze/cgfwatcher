﻿using System.Collections.Generic;
using CGFWatcher.Database;

namespace CGFWatcher.Web.Repositories
{
    public interface IGolfClubRepository
    {
        IList<GolfClub> GetGolfClubs();
    }
}
