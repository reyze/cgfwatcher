﻿namespace CGFWatcher.Web.Repositories
{
    public class PlayerFilter
    {
        public string RegistrationNumberOrName { get; set; }
        public int? GolfClubID { get; set; }
        public decimal? HandicapFrom { get; set; }
        public decimal? HandicapTo { get; set; }
    }
}
