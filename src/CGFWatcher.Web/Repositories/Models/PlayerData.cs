﻿using CGFWatcher.Database;

namespace CGFWatcher.Web.Repositories
{
    public class PlayerData
    {
        public Player Player { get; set; }
    }
}
