﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CGFWatcher.Database;
using Microsoft.EntityFrameworkCore;

namespace CGFWatcher.Web.Repositories
{
    public class PlayerRepository : IPlayerRepository
    {
        private WatcherContext _context;

        public PlayerRepository(WatcherContext context)
        {
            _context = context;
        }

        public IList<Player> GetPlayers(PlayerFilter filter)
        {
            var query = _context.Players
                .Include(x => x.Memberships)
                .ThenInclude(x => x.GolfClub)
                .Where(x => x.Memberships.Any());

            if (!string.IsNullOrEmpty(filter.RegistrationNumberOrName))
            {
                if (Regex.IsMatch(filter.RegistrationNumberOrName, "^[0-9]*$"))
                {
                    query = query.Where(x => x.Memberships.Any(y => y.RegistrationNumber.Contains(filter.RegistrationNumberOrName)));
                }
                else
                {
                    query = query.Where(x => x.FullName.Contains(filter.RegistrationNumberOrName));
                }
            }

            if (filter.GolfClubID.HasValue)
            {
                query = query.Where(x => x.Memberships.Any(y => y.GolfClubID == filter.GolfClubID));
            }

            if (filter.HandicapFrom.HasValue)
            {
                query = query.Where(x => x.Handicap >= filter.HandicapFrom);
            }

            if (filter.HandicapTo.HasValue)
            {
                query = query.Where(x => x.Handicap <= filter.HandicapTo);
            }

            return query.OrderBy(x => x.FullName).ToList();
        }

        public PlayerData GetPlayerData(int id)
        {
            return new PlayerData
            {
                Player = _context.Players
                    .Include(x => x.Memberships)
                    .ThenInclude(x => x.GolfClub)
                    .FirstOrDefault(x => x.ID == id)
            };
        }
    }
}
