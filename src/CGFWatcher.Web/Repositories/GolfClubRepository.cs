﻿using System.Collections.Generic;
using System.Linq;
using CGFWatcher.Database;

namespace CGFWatcher.Web.Repositories
{
    public class GolfClubRepository : IGolfClubRepository
    {
        private WatcherContext _context;

        public GolfClubRepository(WatcherContext context)
        {
            _context = context;
        }

        public IList<GolfClub> GetGolfClubs()
        {
            return _context.GolfClubs
                .OrderBy(x => x.Name)
                .ToList();
        }
    }
}
